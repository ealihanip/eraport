-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2018 at 09:24 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eraport`
--

-- --------------------------------------------------------

--
-- Table structure for table `absen`
--

CREATE TABLE `absen` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `izin` int(11) NOT NULL,
  `sakit` int(11) NOT NULL,
  `tanpa_keterangan` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `absen`
--

INSERT INTO `absen` (`id`, `id_ajaran`, `id_semester`, `id_kelas`, `id_siswa`, `izin`, `sakit`, `tanpa_keterangan`, `tanggal`) VALUES
(8, 1, 1, 27, 6, 1, 1, 1, '2018-10-13 11:49:00'),
(9, 1, 1, 27, 5, 1, 1, 1, '2018-10-13 11:49:00'),
(10, 1, 1, 27, 4, 1, 1, 1, '2018-10-13 11:49:00');

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `username`, `password`, `foto`, `tanggal`) VALUES
(2, 'Slamet', 'heruheroex@gmail.com', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', NULL, '2018-09-22 03:27:49'),
(6, 'user', 'slamet@dunia.akhirat', 'user', '7c222fb2927d828af22f592134e8932480637c0d', 'IMG00088-20120929-1351.jpg', '2018-08-03 01:49:27'),
(7, 'kepalasekolah', 'kepalasekolah@sekolah.sekolah', 'kepalasekolah', 'b662544d3fc7d4933a6d7bc935a6149275cf55f6', NULL, '2018-10-11 13:21:03');

-- --------------------------------------------------------

--
-- Table structure for table `anggota_kelas`
--

CREATE TABLE `anggota_kelas` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `anggota_kelas`
--

INSERT INTO `anggota_kelas` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`) VALUES
(1, 1, 27, 6),
(2, 1, 27, 5),
(3, 1, 27, 4),
(5, 1, 26, 2),
(6, 1, 26, 3),
(7, 1, 27, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aspek`
--

CREATE TABLE `aspek` (
  `id_aspek` int(11) NOT NULL,
  `nama_aspek` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aspek`
--

INSERT INTO `aspek` (`id_aspek`, `nama_aspek`) VALUES
(1, 'Pengetahuan'),
(2, 'Keterampilan');

-- --------------------------------------------------------

--
-- Table structure for table `b_keahlian`
--

CREATE TABLE `b_keahlian` (
  `id_bidang` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `b_keahlian`
--

INSERT INTO `b_keahlian` (`id_bidang`, `nama`) VALUES
(1, 'Teknik Informasi dan Komunikasi'),
(2, 'Bisnis dan Manajemen');

-- --------------------------------------------------------

--
-- Table structure for table `catatan_wk`
--

CREATE TABLE `catatan_wk` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `catatan_wk`
--

INSERT INTO `catatan_wk` (`id`, `id_ajaran`, `id_semester`, `id_kelas`, `id_siswa`, `catatan`, `tanggal`) VALUES
(5, 1, 1, 27, 6, 'test', '2018-10-13 09:57:06'),
(6, 1, 1, 27, 5, 'baik', '2018-10-12 14:46:29'),
(7, 1, 1, 27, 4, 'baik', '2018-10-12 14:46:29');

-- --------------------------------------------------------

--
-- Table structure for table `data_sekolah`
--

CREATE TABLE `data_sekolah` (
  `id` int(11) NOT NULL,
  `npsn` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `desa_kelurahan` varchar(255) NOT NULL,
  `kecamatan` varchar(255) NOT NULL,
  `kabupaten` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kode_pos` varchar(255) NOT NULL,
  `lintang` varchar(255) NOT NULL,
  `bujur` varchar(255) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo_sekolah` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_sekolah`
--

INSERT INTO `data_sekolah` (`id`, `npsn`, `nama`, `alamat`, `desa_kelurahan`, `kecamatan`, `kabupaten`, `provinsi`, `kode_pos`, `lintang`, `bujur`, `telepon`, `fax`, `email`, `website`, `logo_sekolah`, `tanggal`) VALUES
(1, 'test', 'test', 'test', 'test', 'test', 'Semarang', 'Jawa Tengah', 'test', '10', '11', 'test', 'test', 'test', 'test', 'logo.jpg', '2018-10-15 05:04:29');

-- --------------------------------------------------------

--
-- Table structure for table `deskripsi_mapel`
--

CREATE TABLE `deskripsi_mapel` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `pengetahuan` text NOT NULL,
  `keterampilan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deskripsi_mapel`
--

INSERT INTO `deskripsi_mapel` (`id`, `id_ajaran`, `id_siswa`, `id_semester`, `id_kelas`, `id_mapel`, `pengetahuan`, `keterampilan`, `tanggal`) VALUES
(28, 1, 6, 1, 27, 2, 'baik banget', 'baik coy', '2018-10-13 13:26:39'),
(29, 1, 5, 1, 27, 2, 'baik', 'baik', '2018-10-13 13:21:49'),
(30, 1, 4, 1, 27, 2, 'baik', 'baik', '2018-10-13 13:21:49');

-- --------------------------------------------------------

--
-- Table structure for table `ekskul`
--

CREATE TABLE `ekskul` (
  `id` int(11) NOT NULL,
  `id_ajaran` varchar(10) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `nama_ekskul` varchar(255) NOT NULL,
  `nama_ketua` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ekskul`
--

INSERT INTO `ekskul` (`id`, `id_ajaran`, `id_guru`, `nama_ekskul`, `nama_ketua`, `no_hp`, `alamat`, `tanggal`) VALUES
(1, '2018/2019', 5, 'PRAMUKA', 'SIGIT', '081326188804', 'SMK', '2018-09-22 03:40:22'),
(2, '2018/2019', 22, 'Bola Volly', 'Raden Mas Bro', '081326188804', 'jl. sesama', '2018-09-22 03:40:08');

-- --------------------------------------------------------

--
-- Table structure for table `golongan_mapel`
--

CREATE TABLE `golongan_mapel` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan_mapel`
--

INSERT INTO `golongan_mapel` (`id`, `nama`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'C'),
(4, 'D');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `jk` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(15) DEFAULT NULL,
  `alamat` text,
  `rt` int(10) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `desa_kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL,
  `kode_pos` varchar(20) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `akses_level` enum('1','2','3','4','5') DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `nip`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `rt`, `rw`, `desa_kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `no_hp`, `email`, `foto`, `username`, `password`, `akses_level`, `tanggal`) VALUES
(3, 'Fitri Mudawamah S.I., S.Pd', '', 'Perempuan', 'Semarang', '1971-07-19', 'islam', 'jl. Tompomas Selatan II No 12', 6, '02', '', '', '', '', '081901144830', '', NULL, 'fitri', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-08-06 09:12:33'),
(4, 'Toriq Hasan, M.SI', '04.07.035', 'Laki-laki', 'Pemalang', '1976-02-04', 'islam', 'jl. Taman Parkit I Blok C7 Perum Asri Semarang', 0, '', '', '', 'Semarang', '', '085226271976', '', NULL, 'toriq', '7c222fb2927d828af22f592134e8932480637c0d', '3', '2018-09-24 00:44:52'),
(5, 'Dibyo Sukarjo Aljawi, S.Pd', '', 'Laki-laki', 'Semarang', '1957-11-12', 'islam', 'jl. Tusam Raya No. 19 Semarang', 0, '', '', '', '', '', '08122524619', '', '100_99381.JPG', 'dibyo', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-08-06 09:18:36'),
(6, 'Arief Dwi Arsyanti, S.Pd', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Kanfer Utara I/83 ', 0, '', '', 'Banyumanik', 'Semarang', '', '085866827180', '', NULL, 'arif', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-08 19:46:52'),
(7, 'Iwan Setiawan Wibisono, S.T', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Ngemplak Tandang', 3, '09', '', '', '', '', '081326188804', '', NULL, 'iwan', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-15 00:34:53'),
(8, 'Ika Prasetya Yunianti, S.Pd', '', 'Laki-laki', 'Tegal', '0000-00-00', 'islam', 'jl. Sawunggaling Timur No 19', 1, 'XV', '', '', '', '', '', '', NULL, 'ika', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-15 00:34:38'),
(9, 'Ratna Indriani, S.Pd', '', 'Prempuan', 'Salatiga', '0000-00-00', 'islam', ' jl. Dliko Indah No. 1H Salatiga', 0, '', '', '', '', '', '', '', NULL, 'ratna', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:33:33'),
(10, 'Neneng Nurhasanah, S.Kom', '', 'Prempuan', 'Bandung', '0000-00-00', 'islam', 'jl. Gedawang Permai Blok 1 No. 11', 0, '', '', '', 'Semarang', '', '', '', NULL, 'neneng', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:36:19'),
(11, 'Dwi Hastuti, SE', '', 'Prempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Gaharu Barat Dalam V/130', 0, '', '', 'Banyumanik', '', '', '', '', NULL, 'dwi', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:38:06'),
(12, 'Daisy Samsuniar Rufiqoh, S.Pd', '', 'Prempuan', 'Kebumen', '0000-00-00', 'islam', 'Perum Tembalang Pesona Asri Blok L/17', 0, '', '', '', 'Semarang', '', '', '', NULL, 'daisy', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:40:07'),
(13, 'Ubaedillah Umar, S.Pd', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Banaran Sekaran ', 5, 'V', '', 'Gunungpati', '', '', '', '', NULL, 'ubaed', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:42:06'),
(14, 'Endah Purwatinigsih', '', 'Prempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Gatot Subroto No.66', 0, '', '', 'Ungaran', '', '', '', '', NULL, 'endah', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:53:41'),
(15, 'Yunianto Agung Nugroho, S.Pd', '06.07.048', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Jati Barat No. 229', 0, '', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'yunianto', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:54:35'),
(16, 'Suci Rahayu,s.Pd', '07.01.052', 'Perempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Sendang Elo', 9, 'II', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'suci', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:48:51'),
(17, 'Nur yasin Ritonga, S.Kom', '', 'Laki-laki', 'Dumai', '0000-00-00', 'islam', 'jl. Abimanyu VIII/15', 0, '', '', '', 'Semarang', '', '', '', NULL, 'yasin', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:21:17'),
(18, 'Wirdati Bahrurl, S.Pd', '', 'Laki-laki', 'Simpang Sugiran', '0000-00-00', 'islam', 'jl. Dinar Mas XI/26 Perum Dinar Mas', 0, '', '', '', '', '', '', 'bunda@gmail.com', NULL, 'bunda', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-09-17 16:40:50'),
(19, 'Suprihatiningsih, S.Pd', '', 'Prempuan', 'Cilacap', '1971-11-10', 'islam', 'Perum Puri Bukit Gedawang Kav-5', 10, 'IV', '', '', 'Semarang', '', '', '', NULL, 'ning', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:28:23'),
(20, 'Fadzhilah Ikhsan Sidiq', '', 'Prempuan', 'Klaten', '1983-03-12', 'islam', 'Asrama Yonif 400R', 0, '', 'Srondol', 'Banyumanik', 'Semarang', '', '', '', NULL, 'nana', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:31:20'),
(21, 'Wara Nasa Martina, S.S', '10.02.075', 'Perempuan', 'Semarang', '1983-03-20', 'islam', 'jl. Meranti Barat Dalam III/90', 0, '', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'nasa', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:55:25'),
(22, 'Hasan As\'ari, S.Pd', '', 'Laki-laki', 'Pati', '1987-11-22', 'islam', 'Tlogorejo Tlogowungu', 4, 'II', '', '', 'Pati', '', '', '', NULL, 'hasan', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:35:48'),
(23, 'Ma\'rufatin Nurus Sa\'ady', '', 'Laki-laki', 'Kudus', '1983-03-16', 'islam', 'Ds. Margorejo', 1, '07', 'Dawe', '', 'Kudus', '', '', '', NULL, 'adi', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:38:38'),
(24, 'Laely Maftuchah, S.Pd', '', 'Perempuan', 'Ungaran', '1985-11-17', 'islam', 'Karangmalang ', 3, '02', '', 'Mijen', 'Semarang', '', '', '', NULL, 'laely', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:40:09'),
(25, 'Dwi Setiawati Manik Hapsari, S.Psi', '', 'Perempuan', 'Semarang', '1981-02-13', 'islam', 'Perum Graha Merdeka Regensi No.15', 0, '', '', 'Pedurungan', 'Semarang', '', '', '', NULL, 'manik', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:41:48');

-- --------------------------------------------------------

--
-- Table structure for table `guru_mapel`
--

CREATE TABLE `guru_mapel` (
  `id_gm` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru_mapel`
--

INSERT INTO `guru_mapel` (`id_gm`, `id_ajaran`, `id_guru`, `id_kelas`, `id_mapel`, `bobot`) VALUES
(10, 1, 18, 27, 2, 75),
(11, 1, 4, 27, 1, 75),
(12, 1, 3, 27, 6, 75),
(13, 1, 7, 27, 13, 75),
(14, 1, 22, 27, 4, 75);

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `tanggal`) VALUES
(1, 'Pendidikan', '2018-09-23 01:38:32'),
(2, 'Penelitian', '2018-09-23 02:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `kd`
--

CREATE TABLE `kd` (
  `id_kd` int(11) NOT NULL,
  `kode_kd` varchar(255) NOT NULL,
  `id_aspek` int(11) DEFAULT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `isi_kd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kd`
--

INSERT INTO `kd` (`id_kd`, `kode_kd`, `id_aspek`, `id_mapel`, `id_kelas`, `isi_kd`) VALUES
(7, '1.1', 1, 1, 26, 'nanananankankanka'),
(8, '4.1', 2, 1, 26, 'nanana'),
(10, '1.1', 1, 2, 27, 'woiwoiwo'),
(11, '4.1', 2, 2, 27, 'asasa'),
(12, '3.1', NULL, 1, 7, 'asa'),
(13, '5.1', NULL, 1, 7, 'asasa'),
(14, '5.2', NULL, 1, 7, 'cv'),
(15, '3.1', 1, 6, 27, ''),
(16, '1.1', 1, 13, 27, ''),
(17, '1.2', 1, 13, 27, 'test'),
(18, '3.2', 1, 6, 27, ''),
(19, '3.3', 1, 6, 27, ''),
(20, '3.4', 1, 6, 27, ''),
(22, '4.1', 2, 6, 27, 'test'),
(23, '4.2', 2, 6, 27, 'test'),
(24, '4.3', 2, 6, 27, ''),
(25, '4.4', 2, 6, 27, ''),
(26, '1.2', 1, 2, 27, ''),
(27, '1.3', 1, 2, 27, ''),
(28, '1.4', 1, 2, 27, ''),
(29, '3.1', 1, 4, 27, ''),
(30, '3.2', 1, 4, 27, ''),
(31, '3.3', 1, 4, 27, ''),
(32, '3.4', 1, 4, 27, ''),
(33, '4.1', 2, 4, 27, '');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `id_ajaran` varchar(255) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kompetensi` int(2) NOT NULL,
  `nama_kelas` int(2) DEFAULT NULL,
  `nama_jurusan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_ajaran`, `id_guru`, `id_kompetensi`, `nama_kelas`, `nama_jurusan`) VALUES
(26, '2018/2019', 3, 2, 10, 'X TKJ'),
(27, '2018/2019', 7, 1, 10, 'X RPL'),
(28, '2018/2019', 6, 3, 10, 'X OTKP'),
(29, '2018/2019', 8, 4, 10, 'X AK'),
(30, '2018/2019', 5, 4, 11, 'XI AK');

-- --------------------------------------------------------

--
-- Table structure for table `kompetensi_dasar`
--

CREATE TABLE `kompetensi_dasar` (
  `id_kd` int(11) NOT NULL,
  `kode_kd` varchar(10) NOT NULL,
  `aspek` varchar(20) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kompetensi_dasar`
--

INSERT INTO `kompetensi_dasar` (`id_kd`, `kode_kd`, `aspek`, `id_mapel`, `id_kelas`, `isi`) VALUES
(1, '3.1', 'Pengetahuan', 6, 26, 'bacbcbthr');

-- --------------------------------------------------------

--
-- Table structure for table `k_keahlian`
--

CREATE TABLE `k_keahlian` (
  `id_kompetensi` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `k_keahlian`
--

INSERT INTO `k_keahlian` (`id_kompetensi`, `id_bidang`, `id_program`, `nama`, `nama_lengkap`) VALUES
(1, 1, 1, 'RPL', 'Rekayasa Perangkat Lunak'),
(2, 1, 1, 'TKJ', 'Teknik Komputer dan Jaringan'),
(3, 2, 2, 'OTKP', 'Otomatisasi dan Tata Kelola Perkantoran '),
(4, 2, 3, 'AKL', 'Akuntansi dan Keuangan Lembaga');

-- --------------------------------------------------------

--
-- Table structure for table `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(3) NOT NULL,
  `kode_mapel` varchar(10) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL,
  `id_golongan` int(11) NOT NULL DEFAULT '1',
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `kode_mapel`, `nama_mapel`, `id_golongan`, `tanggal`) VALUES
(1, '', 'Pendidikan Agama', 1, '2018-10-11 14:32:15'),
(2, '1213', 'Bahasa Indonesia', 2, '2018-10-14 05:23:53'),
(3, '', 'Pendidikan Kewarganegaraan dan Sejarah', 1, '2018-08-03 03:40:54'),
(4, '', 'Pendidikan Jasmani dan Kesehatan', 1, '2018-08-03 03:43:18'),
(5, '', 'Seni Budaya', 1, '2018-08-03 03:44:51'),
(6, '', 'Matematika', 1, '2018-08-03 03:47:11'),
(7, '', 'Bahasa Inggris', 1, '2018-08-03 03:47:24'),
(8, '', 'Ilmu Pengetahuan Alam', 1, '2018-08-03 03:47:40'),
(9, '', 'Ilmu Pengetahuan Sosial', 1, '2018-08-03 03:47:57'),
(10, '', 'KKPI', 1, '2018-08-03 03:48:09'),
(11, '', 'Kewirahusahaan', 1, '2018-08-03 03:48:20'),
(12, '', 'Dasar Kompetensi Kejurusan', 1, '2018-08-03 03:48:55'),
(13, '', 'Kompetensi Kejuruan', 1, '2018-08-03 03:49:26'),
(14, '', 'BTAQ', 1, '2018-08-03 03:49:33'),
(15, '', 'Fisika', 1, '2018-08-03 03:50:39'),
(16, '', 'Kimia', 1, '2018-08-03 03:50:45'),
(17, '', 'Bahasa Jawa', 1, '2018-08-03 03:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_sn` int(12) NOT NULL,
  `id_siswa` int(12) NOT NULL,
  `nilai` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id_nilai`, `id_semester`, `id_sn`, `id_siswa`, `nilai`, `tanggal`) VALUES
(1, 1, 33, 6, '90', '2018-10-13 09:36:36'),
(2, 1, 33, 5, '90', '2018-10-11 03:28:59'),
(3, 1, 33, 4, '90', '2018-10-11 03:28:59'),
(4, 1, 36, 6, '80', '2018-10-11 03:29:13'),
(5, 1, 36, 5, '80', '2018-10-11 03:29:13'),
(6, 1, 36, 4, '80', '2018-10-11 03:29:14'),
(7, 1, 37, 6, '100', '2018-10-11 03:29:25'),
(8, 1, 37, 5, '100', '2018-10-11 03:29:25'),
(9, 1, 37, 4, '100', '2018-10-11 03:29:25'),
(10, 1, 38, 6, '100', '2018-10-11 03:29:44'),
(11, 1, 38, 5, '100', '2018-10-11 03:29:44'),
(12, 1, 38, 4, '100', '2018-10-11 03:29:44'),
(13, 1, 39, 6, '80', '2018-10-11 03:29:55'),
(14, 1, 39, 5, '80', '2018-10-11 03:29:55'),
(15, 1, 39, 4, '80', '2018-10-11 03:29:55'),
(16, 1, 40, 6, '100', '2018-10-11 03:30:13'),
(17, 1, 40, 5, '100', '2018-10-11 03:30:13'),
(18, 1, 40, 4, '100', '2018-10-11 03:30:13'),
(19, 1, 42, 6, '100', '2018-10-11 03:30:38'),
(20, 1, 42, 5, '100', '2018-10-11 03:30:38'),
(21, 1, 42, 4, '100', '2018-10-11 03:30:38'),
(22, 1, 12, 6, '83', '2018-10-11 03:32:57'),
(23, 1, 12, 5, '83', '2018-10-11 03:32:57'),
(24, 1, 12, 4, '83', '2018-10-11 03:32:57'),
(25, 1, 43, 6, '83', '2018-10-11 03:38:04'),
(26, 1, 43, 5, '83', '2018-10-11 03:38:04'),
(27, 1, 43, 4, '0', '2018-10-11 03:38:05'),
(28, 1, 44, 6, '90', '2018-10-11 05:00:54'),
(29, 1, 44, 5, '90', '2018-10-11 05:00:54'),
(30, 1, 44, 4, '90', '2018-10-11 05:00:54'),
(31, 1, 45, 6, '80', '2018-10-11 05:01:12'),
(32, 1, 45, 5, '80', '2018-10-11 05:01:12'),
(33, 1, 45, 4, '80', '2018-10-11 05:01:12'),
(34, 1, 46, 6, '100', '2018-10-11 05:01:33'),
(35, 1, 46, 5, '100', '2018-10-11 05:01:33'),
(36, 1, 46, 4, '100', '2018-10-11 05:01:33'),
(37, 1, 47, 6, '100', '2018-10-11 05:01:46'),
(38, 1, 47, 5, '100', '2018-10-11 05:01:46'),
(39, 1, 47, 4, '100', '2018-10-11 05:01:47'),
(40, 1, 48, 6, '80', '2018-10-11 05:02:05'),
(41, 1, 48, 5, '80', '2018-10-11 05:02:05'),
(42, 1, 48, 4, '80', '2018-10-11 05:02:05'),
(43, 1, 49, 6, '83', '2018-10-11 05:03:21'),
(44, 1, 49, 5, '83', '2018-10-11 05:03:21'),
(45, 1, 49, 4, '83', '2018-10-11 05:03:21'),
(46, 1, 50, 6, '100', '2018-10-11 05:06:51'),
(47, 1, 50, 5, '100', '2018-10-11 05:06:51'),
(48, 1, 50, 4, '100', '2018-10-11 05:06:51'),
(49, 1, 51, 6, '100', '2018-10-11 05:07:12'),
(50, 1, 51, 5, '100', '2018-10-11 05:07:13'),
(51, 1, 51, 4, '100', '2018-10-11 05:07:13'),
(52, 1, 52, 6, '83', '2018-10-11 05:07:26'),
(53, 1, 52, 5, '83', '2018-10-11 05:07:26'),
(54, 1, 52, 4, '83', '2018-10-11 05:07:26'),
(55, 2, 33, 6, '10', '2018-10-12 14:43:32'),
(56, 2, 33, 5, '10', '2018-10-12 14:43:32'),
(57, 2, 33, 4, '10', '2018-10-12 14:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ekskul`
--

CREATE TABLE `nilai_ekskul` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_ekskul` int(11) NOT NULL,
  `predikat` varchar(10) NOT NULL,
  `deskripsi` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_ekskul`
--

INSERT INTO `nilai_ekskul` (`id`, `id_ajaran`, `id_siswa`, `id_semester`, `id_kelas`, `id_ekskul`, `predikat`, `deskripsi`, `tanggal`) VALUES
(10, 1, 6, 1, 27, 1, 'C', 'test', '2018-10-13 14:12:14'),
(11, 1, 5, 1, 27, 1, 'B', 'test', '2018-10-13 14:12:15'),
(12, 1, 4, 1, 27, 1, 'B', 'test', '2018-10-13 14:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_ujian`
--

CREATE TABLE `nilai_ujian` (
  `id_nilai` int(12) NOT NULL,
  `id_aspek` int(11) NOT NULL,
  `id_ajaran` int(12) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_mapel` int(12) NOT NULL,
  `id_kelas` int(12) NOT NULL,
  `id_siswa` int(12) NOT NULL,
  `nilai` int(12) NOT NULL,
  `tipe_ujian` tinyint(4) NOT NULL,
  `tanggal` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_ujian`
--

INSERT INTO `nilai_ujian` (`id_nilai`, `id_aspek`, `id_ajaran`, `id_semester`, `id_mapel`, `id_kelas`, `id_siswa`, `nilai`, `tipe_ujian`, `tanggal`) VALUES
(1, 1, 1, 1, 2, 27, 6, 85, 1, '2018-10-11 10:32:13'),
(2, 1, 1, 1, 2, 27, 5, 85, 1, '2018-10-11 10:32:13'),
(3, 1, 1, 1, 2, 27, 4, 85, 1, '2018-10-11 10:32:13'),
(4, 1, 1, 1, 2, 27, 6, 80, 2, '2018-10-11 10:32:28'),
(5, 1, 1, 1, 2, 27, 5, 80, 2, '2018-10-11 10:32:28'),
(6, 1, 1, 1, 2, 27, 4, 80, 2, '2018-10-11 10:32:28'),
(7, 2, 1, 1, 2, 27, 6, 90, 1, '2018-10-11 10:35:32'),
(8, 2, 1, 1, 2, 27, 5, 90, 1, '2018-10-11 10:35:32'),
(9, 2, 1, 1, 2, 27, 4, 90, 1, '2018-10-11 10:35:32'),
(10, 2, 1, 1, 4, 27, 6, 90, 1, '2018-10-11 12:03:54'),
(11, 2, 1, 1, 4, 27, 5, 90, 1, '2018-10-11 12:03:54'),
(12, 2, 1, 1, 4, 27, 4, 90, 1, '2018-10-11 12:03:54'),
(13, 1, 1, 1, 4, 27, 6, 85, 1, '2018-10-11 12:08:09'),
(14, 1, 1, 1, 4, 27, 5, 85, 1, '2018-10-11 12:08:09'),
(15, 1, 1, 1, 4, 27, 4, 85, 1, '2018-10-11 12:08:09'),
(16, 1, 1, 1, 4, 27, 6, 80, 2, '2018-10-11 12:08:29'),
(17, 1, 1, 1, 4, 27, 5, 80, 2, '2018-10-11 12:08:29'),
(18, 1, 1, 1, 4, 27, 4, 80, 2, '2018-10-11 12:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `prakerin`
--

CREATE TABLE `prakerin` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `lama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prakerin`
--

INSERT INTO `prakerin` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`, `nama_instansi`, `alamat`, `lama`, `keterangan`, `tanggal`) VALUES
(1, 1, 27, 6, 'Desnet', 'jl. Kono kae no 212', '3(tiga) Bulan', 'nanannoa', '2018-09-21 23:42:20');

-- --------------------------------------------------------

--
-- Table structure for table `predikat`
--

CREATE TABLE `predikat` (
  `id` int(11) NOT NULL,
  `batas_bawah` int(11) NOT NULL,
  `batas_atas` int(11) NOT NULL,
  `predikat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `predikat`
--

INSERT INTO `predikat` (`id`, `batas_bawah`, `batas_atas`, `predikat`) VALUES
(1, 91, 100, 'A'),
(2, 84, 90, 'B'),
(3, 75, 83, 'C'),
(4, 65, 74, 'D'),
(5, 0, 64, 'E');

-- --------------------------------------------------------

--
-- Table structure for table `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nama_prestasi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prestasi`
--

INSERT INTO `prestasi` (`id`, `id_ajaran`, `id_semester`, `id_kelas`, `id_siswa`, `nama_prestasi`, `keterangan`, `tanggal`) VALUES
(1, 1, 1, 27, 6, 'Juara 2 Lomba LKS sekota Semarang', 'hoya', '2018-09-21 23:42:09');

-- --------------------------------------------------------

--
-- Table structure for table `p_keahlian`
--

CREATE TABLE `p_keahlian` (
  `id_program` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_keahlian`
--

INSERT INTO `p_keahlian` (`id_program`, `id_bidang`, `nama`) VALUES
(1, 1, 'Teknik Komputer dan Informatika'),
(2, 2, 'Manajemen Perkantoran'),
(3, 2, 'Akuntansi & Keuangan');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `nama_semester` int(11) NOT NULL,
  `aktif` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id_semester`, `nama_semester`, `aktif`) VALUES
(1, 0, 0),
(2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `set_nilai`
--

CREATE TABLE `set_nilai` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nama_penilaian` varchar(255) NOT NULL,
  `bobot_nilai` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `id_aspek` int(11) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `set_nilai`
--

INSERT INTO `set_nilai` (`id`, `id_ajaran`, `id_semester`, `id_mapel`, `id_kelas`, `nama_penilaian`, `bobot_nilai`, `id_kd`, `id_aspek`, `keterangan`) VALUES
(12, 1, 1, 2, 27, 'portofolio', 1, 11, 2, 'sas'),
(13, 1, 1, 6, 27, 'Tugas 1', 1, 15, 1, ''),
(14, 1, 1, 6, 27, 'Tugas 2', 1, 15, 1, 'test'),
(15, 1, 1, 6, 27, 'Tugas 3', 1, 15, 1, ''),
(16, 1, 1, 6, 27, 'Tugas 4', 1, 15, 1, 'test'),
(18, 1, 1, 6, 27, 'Praktek ', 1, 22, 2, ''),
(19, 1, 1, 6, 27, 'produk', 1, 22, 2, ''),
(20, 1, 1, 6, 27, 'proyek', 1, 22, 2, ''),
(21, 1, 1, 6, 27, 'portofolio', 1, 22, 2, ''),
(22, 1, 1, 6, 27, 'Praktek', 1, 23, 2, ''),
(23, 1, 1, 6, 27, 'produk', 1, 23, 2, ''),
(24, 1, 1, 6, 27, 'proyek', 1, 23, 2, ''),
(25, 1, 1, 6, 27, 'portofolio', 1, 23, 2, ''),
(26, 1, 1, 6, 27, 'Praktek', 1, 24, 2, ''),
(27, 1, 1, 6, 27, 'produk', 1, 24, 2, ''),
(28, 1, 1, 6, 27, 'proyek', 1, 24, 2, ''),
(30, 1, 1, 6, 27, 'portofolio', 1, 24, 2, ''),
(31, 1, 1, 6, 27, 'Praktek', 1, 25, 2, ''),
(32, 1, 1, 6, 27, 'produk', 1, 25, 2, ''),
(33, 1, 1, 2, 27, 'Tugas1', 1, 10, 1, ''),
(34, 1, 1, 6, 27, 'proyek', 1, 25, 2, ''),
(35, 1, 1, 6, 27, 'portofolio', 1, 25, 2, ''),
(36, 1, 1, 2, 27, 'Tugas1', 1, 26, 1, ''),
(37, 1, 1, 2, 27, 'Tugas1', 1, 27, 1, ''),
(38, 1, 1, 2, 27, 'Tugas1', 1, 28, 1, ''),
(39, 1, 1, 2, 27, 'Tugas3', 1, 10, 1, ''),
(40, 1, 1, 2, 27, 'Penilaian Harian 1', 2, 10, 1, 'nilai harian'),
(42, 1, 1, 2, 27, 'Penilaian Harian 1', 2, 26, 1, 'nilai harian'),
(43, 1, 1, 2, 27, 'Penilaian Harian 1', 2, 27, 1, 'nilai harian'),
(44, 1, 1, 4, 27, 'Tugas1', 1, 29, 1, ''),
(45, 1, 1, 4, 27, 'Tugas1', 1, 30, 1, ''),
(46, 1, 1, 4, 27, 'Tugas1', 1, 31, 1, ''),
(47, 1, 1, 4, 27, 'Tugas1', 1, 32, 1, ''),
(48, 1, 1, 4, 27, 'Tugas3', 1, 29, 1, ''),
(49, 1, 1, 4, 27, 'portofolio', 1, 33, 2, ''),
(50, 1, 1, 4, 27, 'UH1', 2, 29, 1, ''),
(51, 1, 1, 4, 27, 'UH1', 2, 30, 1, ''),
(52, 1, 1, 4, 27, 'UH1', 2, 31, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `sikap`
--

CREATE TABLE `sikap` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL DEFAULT '1',
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `sikap` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sikap`
--

INSERT INTO `sikap` (`id`, `id_ajaran`, `id_semester`, `id_kelas`, `id_siswa`, `sikap`, `tanggal`) VALUES
(4, 1, 1, 27, 6, 'baik \r\n', '2018-10-13 10:01:31'),
(5, 1, 1, 27, 5, 'baik', '2018-10-13 01:42:46'),
(6, 1, 1, 27, 4, 'baik', '2018-10-13 01:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `no_induk` varchar(255) DEFAULT NULL,
  `nisn` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `anak_ke` varchar(255) DEFAULT NULL,
  `alamat` text,
  `rt` varchar(255) DEFAULT NULL,
  `rw` varchar(255) DEFAULT NULL,
  `desa_kelurahan` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `sekolah_asal` varchar(255) DEFAULT NULL,
  `diterima_kelas` varchar(255) DEFAULT NULL,
  `diterima` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `kerja_ayah` varchar(255) DEFAULT NULL,
  `telp_ayah` varchar(255) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `kerja_ibu` varchar(255) DEFAULT NULL,
  `telp_ibu` varchar(255) DEFAULT NULL,
  `nama_wali` varchar(255) DEFAULT NULL,
  `kerja_wali` varchar(255) DEFAULT NULL,
  `telp_wali` varchar(255) DEFAULT NULL,
  `alamat_wali` text,
  `poto` varchar(255) DEFAULT NULL,
  `status_siswa` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kelas`, `id_semester`, `nama_siswa`, `no_induk`, `nisn`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `status`, `anak_ke`, `alamat`, `rt`, `rw`, `desa_kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `no_hp`, `sekolah_asal`, `diterima_kelas`, `diterima`, `email`, `nama_ayah`, `kerja_ayah`, `telp_ayah`, `nama_ibu`, `kerja_ibu`, `telp_ibu`, `nama_wali`, `kerja_wali`, `telp_wali`, `alamat_wali`, `poto`, `status_siswa`, `username`, `password`, `tanggal`) VALUES
(1, 26, 1, 'Angga Dwiki Setyawan', '14.1517', '', 'Laki-laki', 'Semarang', '1999-06-04', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'keluar', 'angga', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(2, 26, 1, 'Ahmad', '14.1516', '', 'Laki-laki', 'Semarang', '2000-05-13', 'islam', 'kandung', '0', 'jl. Sesama No.212 banyumanik', '03', '07', '', 'Banyumanik', '', '', '', 'SMP N 2 Pringsurat', '', '2018-08-02', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'ahmad', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(3, 26, 1, 'Adam Jodhi Argianto', '14.1515', '', 'Laki-laki', 'Semarang', '1994-04-04', 'islam', 'kandung', '', 'jl. Sendang Elo No.9', '06', '02', '', 'Banyumanik', 'Semarang', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'adam', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(4, 27, 1, 'Abdul Wahab Pratama', '14.1544', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'kandung', '', 'Jurang Blimbing ', '', '', '', 'Tembalang', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'abdul', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:07:29'),
(5, 27, 1, 'eko', '14.1431', '', 'Laki-laki', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'eko', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:11:13'),
(6, 27, 1, 'Widiastuti', '14.1530', '', 'Perempuan', '', '0000-00-00', 'hindu', 'angkat', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'tuti', '2f0a8cb899a0bad307240e06c1f7036e9851672f', '2018-09-08 18:11:00'),
(7, 28, 1, 'Paijo', '14.1520', '', 'Laki-laki', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'paijo', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-17 12:33:36'),
(8, 29, 1, 'Painem', '14.1521', '', 'Perempuan', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'painem', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-23 19:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_ajaran` int(11) NOT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `aktif` tinyint(4) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_ajaran`, `tahun`, `aktif`, `tanggal`) VALUES
(1, '2018/2019', 1, '2018-10-15 05:05:05'),
(2, '2019/2020', 0, '2018-10-13 09:24:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absen`
--
ALTER TABLE `absen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `anggota_kelas`
--
ALTER TABLE `anggota_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aspek`
--
ALTER TABLE `aspek`
  ADD PRIMARY KEY (`id_aspek`);

--
-- Indexes for table `b_keahlian`
--
ALTER TABLE `b_keahlian`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `catatan_wk`
--
ALTER TABLE `catatan_wk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sekolah`
--
ALTER TABLE `data_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deskripsi_mapel`
--
ALTER TABLE `deskripsi_mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekskul`
--
ALTER TABLE `ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan_mapel`
--
ALTER TABLE `golongan_mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  ADD PRIMARY KEY (`id_gm`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kd`
--
ALTER TABLE `kd`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kompetensi_dasar`
--
ALTER TABLE `kompetensi_dasar`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `k_keahlian`
--
ALTER TABLE `k_keahlian`
  ADD PRIMARY KEY (`id_kompetensi`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`),
  ADD KEY `nilai` (`id_sn`);

--
-- Indexes for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai_ujian`
--
ALTER TABLE `nilai_ujian`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `prakerin`
--
ALTER TABLE `prakerin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `predikat`
--
ALTER TABLE `predikat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_keahlian`
--
ALTER TABLE `p_keahlian`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `set_nilai`
--
ALTER TABLE `set_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sikap`
--
ALTER TABLE `sikap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_ajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absen`
--
ALTER TABLE `absen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `anggota_kelas`
--
ALTER TABLE `anggota_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `aspek`
--
ALTER TABLE `aspek`
  MODIFY `id_aspek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `b_keahlian`
--
ALTER TABLE `b_keahlian`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `catatan_wk`
--
ALTER TABLE `catatan_wk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `data_sekolah`
--
ALTER TABLE `data_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `deskripsi_mapel`
--
ALTER TABLE `deskripsi_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `ekskul`
--
ALTER TABLE `ekskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `golongan_mapel`
--
ALTER TABLE `golongan_mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  MODIFY `id_gm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kd`
--
ALTER TABLE `kd`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `kompetensi_dasar`
--
ALTER TABLE `kompetensi_dasar`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `k_keahlian`
--
ALTER TABLE `k_keahlian`
  MODIFY `id_kompetensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `nilai_ujian`
--
ALTER TABLE `nilai_ujian`
  MODIFY `id_nilai` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `prakerin`
--
ALTER TABLE `prakerin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `predikat`
--
ALTER TABLE `predikat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `p_keahlian`
--
ALTER TABLE `p_keahlian`
  MODIFY `id_program` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `set_nilai`
--
ALTER TABLE `set_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `sikap`
--
ALTER TABLE `sikap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id_ajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai` FOREIGN KEY (`id_sn`) REFERENCES `set_nilai` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
