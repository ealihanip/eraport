-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 01 Okt 2018 pada 08.34
-- Versi Server: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `skripsi`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `absensi`
--

CREATE TABLE `absensi` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `sakit` int(11) NOT NULL,
  `izin` int(11) NOT NULL,
  `alpa` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `absensi`
--

INSERT INTO `absensi` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`, `sakit`, `izin`, `alpa`, `tanggal`) VALUES
(1, 2, 27, 6, 1, 1, 0, '2018-09-20 02:13:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `email`, `username`, `password`, `foto`, `tanggal`) VALUES
(2, 'Slamet', 'heruheroex@gmail.com', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', NULL, '2018-09-22 03:27:49'),
(6, 'user', 'slamet@dunia.akhirat', 'user', '7c222fb2927d828af22f592134e8932480637c0d', 'IMG00088-20120929-1351.jpg', '2018-08-03 01:49:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota_kelas`
--

CREATE TABLE `anggota_kelas` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota_kelas`
--

INSERT INTO `anggota_kelas` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`) VALUES
(1, 1, 27, 6),
(2, 1, 27, 5),
(3, 1, 27, 4),
(5, 1, 26, 2),
(6, 1, 26, 3),
(7, 1, 27, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `aspek`
--

CREATE TABLE `aspek` (
  `id_aspek` int(11) NOT NULL,
  `nama_aspek` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `aspek`
--

INSERT INTO `aspek` (`id_aspek`, `nama_aspek`) VALUES
(1, 'Pengetahuan'),
(2, 'Keterampilan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `b_keahlian`
--

CREATE TABLE `b_keahlian` (
  `id_bidang` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `b_keahlian`
--

INSERT INTO `b_keahlian` (`id_bidang`, `nama`) VALUES
(1, 'Teknik Informasi dan Komunikasi'),
(2, 'Bisnis dan Manajemen');

-- --------------------------------------------------------

--
-- Struktur dari tabel `catatan_wk`
--

CREATE TABLE `catatan_wk` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `catatan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `catatan_wk`
--

INSERT INTO `catatan_wk` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`, `catatan`, `tanggal`) VALUES
(1, 1, 27, 6, 'apik', '2018-09-21 23:41:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_sekolah`
--

CREATE TABLE `data_sekolah` (
  `id` int(11) NOT NULL,
  `npsn` varchar(255) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `desa_kelurahan` varchar(255) NOT NULL,
  `kecamatan` varchar(255) NOT NULL,
  `kabupaten` varchar(255) NOT NULL,
  `provinsi` varchar(255) NOT NULL,
  `kode_pos` varchar(255) NOT NULL,
  `lintang` varchar(255) NOT NULL,
  `bujur` varchar(255) NOT NULL,
  `telepon` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `logo_sekolah` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ekskul`
--

CREATE TABLE `ekskul` (
  `id` int(11) NOT NULL,
  `id_ajaran` varchar(10) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `nama_ekskul` varchar(255) NOT NULL,
  `nama_ketua` varchar(255) NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ekskul`
--

INSERT INTO `ekskul` (`id`, `id_ajaran`, `id_guru`, `nama_ekskul`, `nama_ketua`, `no_hp`, `alamat`, `tanggal`) VALUES
(1, '2018/2019', 5, 'PRAMUKA', 'SIGIT', '081326188804', 'SMK', '2018-09-22 03:40:22'),
(2, '2018/2019', 22, 'Bola Volly', 'Raden Mas Bro', '081326188804', 'jl. sesama', '2018-09-22 03:40:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id_guru` int(11) NOT NULL,
  `nama_guru` varchar(100) NOT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `jk` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(15) DEFAULT NULL,
  `alamat` text,
  `rt` int(10) DEFAULT NULL,
  `rw` varchar(10) DEFAULT NULL,
  `desa_kelurahan` varchar(100) DEFAULT NULL,
  `kecamatan` varchar(100) DEFAULT NULL,
  `kabupaten` varchar(100) DEFAULT NULL,
  `kode_pos` varchar(20) DEFAULT NULL,
  `no_hp` varchar(15) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(64) NOT NULL,
  `akses_level` enum('1','2','3','4','5') DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id_guru`, `nama_guru`, `nip`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `alamat`, `rt`, `rw`, `desa_kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `no_hp`, `email`, `foto`, `username`, `password`, `akses_level`, `tanggal`) VALUES
(3, 'Fitri Mudawamah S.I., S.Pd', '', 'Perempuan', 'Semarang', '1971-07-19', 'islam', 'jl. Tompomas Selatan II No 12', 6, '02', '', '', '', '', '081901144830', '', NULL, 'fitri', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-08-06 09:12:33'),
(4, 'Toriq Hasan, M.SI', '04.07.035', 'Laki-laki', 'Pemalang', '1976-02-04', 'islam', 'jl. Taman Parkit I Blok C7 Perum Asri Semarang', 0, '', '', '', 'Semarang', '', '085226271976', '', NULL, 'toriq', '7c222fb2927d828af22f592134e8932480637c0d', '3', '2018-09-24 00:44:52'),
(5, 'Dibyo Sukarjo Aljawi, S.Pd', '', 'Laki-laki', 'Semarang', '1957-11-12', 'islam', 'jl. Tusam Raya No. 19 Semarang', 0, '', '', '', '', '', '08122524619', '', '100_99381.JPG', 'dibyo', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-08-06 09:18:36'),
(6, 'Arief Dwi Arsyanti, S.Pd', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Kanfer Utara I/83 ', 0, '', '', 'Banyumanik', 'Semarang', '', '085866827180', '', NULL, 'arif', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-08 19:46:52'),
(7, 'Iwan Setiawan Wibisono, S.T', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Ngemplak Tandang', 3, '09', '', '', '', '', '081326188804', '', NULL, 'iwan', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-15 00:34:53'),
(8, 'Ika Prasetya Yunianti, S.Pd', '', 'Laki-laki', 'Tegal', '0000-00-00', 'islam', 'jl. Sawunggaling Timur No 19', 1, 'XV', '', '', '', '', '', '', NULL, 'ika', '7c222fb2927d828af22f592134e8932480637c0d', '2', '2018-09-15 00:34:38'),
(9, 'Ratna Indriani, S.Pd', '', 'Prempuan', 'Salatiga', '0000-00-00', 'islam', ' jl. Dliko Indah No. 1H Salatiga', 0, '', '', '', '', '', '', '', NULL, 'ratna', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:33:33'),
(10, 'Neneng Nurhasanah, S.Kom', '', 'Prempuan', 'Bandung', '0000-00-00', 'islam', 'jl. Gedawang Permai Blok 1 No. 11', 0, '', '', '', 'Semarang', '', '', '', NULL, 'neneng', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:36:19'),
(11, 'Dwi Hastuti, SE', '', 'Prempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Gaharu Barat Dalam V/130', 0, '', '', 'Banyumanik', '', '', '', '', NULL, 'dwi', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:38:06'),
(12, 'Daisy Samsuniar Rufiqoh, S.Pd', '', 'Prempuan', 'Kebumen', '0000-00-00', 'islam', 'Perum Tembalang Pesona Asri Blok L/17', 0, '', '', '', 'Semarang', '', '', '', NULL, 'daisy', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:40:07'),
(13, 'Ubaedillah Umar, S.Pd', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Banaran Sekaran ', 5, 'V', '', 'Gunungpati', '', '', '', '', NULL, 'ubaed', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:42:06'),
(14, 'Endah Purwatinigsih', '', 'Prempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Gatot Subroto No.66', 0, '', '', 'Ungaran', '', '', '', '', NULL, 'endah', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 09:53:41'),
(15, 'Yunianto Agung Nugroho, S.Pd', '06.07.048', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'jl. Jati Barat No. 229', 0, '', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'yunianto', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:54:35'),
(16, 'Suci Rahayu,s.Pd', '07.01.052', 'Perempuan', 'Semarang', '0000-00-00', 'islam', 'jl. Sendang Elo', 9, 'II', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'suci', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:48:51'),
(17, 'Nur yasin Ritonga, S.Kom', '', 'Laki-laki', 'Dumai', '0000-00-00', 'islam', 'jl. Abimanyu VIII/15', 0, '', '', '', 'Semarang', '', '', '', NULL, 'yasin', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:21:17'),
(18, 'Wirdati Bahrurl, S.Pd', '', 'Laki-laki', 'Simpang Sugiran', '0000-00-00', 'islam', 'jl. Dinar Mas XI/26 Perum Dinar Mas', 0, '', '', '', '', '', '', 'bunda@gmail.com', NULL, 'bunda', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-09-17 16:40:50'),
(19, 'Suprihatiningsih, S.Pd', '', 'Prempuan', 'Cilacap', '1971-11-10', 'islam', 'Perum Puri Bukit Gedawang Kav-5', 10, 'IV', '', '', 'Semarang', '', '', '', NULL, 'ning', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:28:23'),
(20, 'Fadzhilah Ikhsan Sidiq', '', 'Prempuan', 'Klaten', '1983-03-12', 'islam', 'Asrama Yonif 400R', 0, '', 'Srondol', 'Banyumanik', 'Semarang', '', '', '', NULL, 'nana', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:31:20'),
(21, 'Wara Nasa Martina, S.S', '10.02.075', 'Perempuan', 'Semarang', '1983-03-20', 'islam', 'jl. Meranti Barat Dalam III/90', 0, '', '', 'Banyumanik', 'Semarang', '', '', '', NULL, 'nasa', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:55:25'),
(22, 'Hasan As\'ari, S.Pd', '', 'Laki-laki', 'Pati', '1987-11-22', 'islam', 'Tlogorejo Tlogowungu', 4, 'II', '', '', 'Pati', '', '', '', NULL, 'hasan', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:35:48'),
(23, 'Ma\'rufatin Nurus Sa\'ady', '', 'Laki-laki', 'Kudus', '1983-03-16', 'islam', 'Ds. Margorejo', 1, '07', 'Dawe', '', 'Kudus', '', '', '', NULL, 'adi', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:38:38'),
(24, 'Laely Maftuchah, S.Pd', '', 'Perempuan', 'Ungaran', '1985-11-17', 'islam', 'Karangmalang ', 3, '02', '', 'Mijen', 'Semarang', '', '', '', NULL, 'laely', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:40:09'),
(25, 'Dwi Setiawati Manik Hapsari, S.Psi', '', 'Perempuan', 'Semarang', '1981-02-13', 'islam', 'Perum Graha Merdeka Regensi No.15', 0, '', '', 'Pedurungan', 'Semarang', '', '', '', NULL, 'manik', '7c222fb2927d828af22f592134e8932480637c0d', '1', '2018-08-06 10:41:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru_mapel`
--

CREATE TABLE `guru_mapel` (
  `id_gm` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `bobot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `guru_mapel`
--

INSERT INTO `guru_mapel` (`id_gm`, `id_ajaran`, `id_guru`, `id_kelas`, `id_mapel`, `bobot`) VALUES
(10, 1, 18, 27, 2, 75),
(11, 1, 4, 27, 1, 75),
(12, 1, 3, 27, 6, 75),
(13, 1, 7, 27, 13, 75);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `tanggal`) VALUES
(1, 'Pendidikan', '2018-09-23 01:38:32'),
(2, 'Penelitian', '2018-09-23 02:22:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kd`
--

CREATE TABLE `kd` (
  `id_kd` int(11) NOT NULL,
  `kode_kd` varchar(255) NOT NULL,
  `id_aspek` int(11) DEFAULT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `isi_kd` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kd`
--

INSERT INTO `kd` (`id_kd`, `kode_kd`, `id_aspek`, `id_mapel`, `id_kelas`, `isi_kd`) VALUES
(7, '1.1', 1, 1, 26, 'nanananankankanka'),
(8, '4.1', 2, 1, 26, 'nanana'),
(10, '1.1', 1, 2, 27, 'woiwoiwo'),
(11, '4.1', 2, 2, 27, 'asasa'),
(12, '3.1', NULL, 1, 7, 'asa'),
(13, '5.1', NULL, 1, 7, 'asasa'),
(14, '5.2', NULL, 1, 7, 'cv');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` int(11) NOT NULL,
  `id_ajaran` varchar(255) NOT NULL,
  `id_guru` int(11) NOT NULL,
  `id_kompetensi` int(2) NOT NULL,
  `nama_kelas` int(2) DEFAULT NULL,
  `nama_jurusan` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `id_ajaran`, `id_guru`, `id_kompetensi`, `nama_kelas`, `nama_jurusan`) VALUES
(26, '2018/2019', 3, 2, 10, 'X TKJ'),
(27, '2018/2019', 7, 1, 10, 'X RPL'),
(28, '2018/2019', 6, 3, 10, 'X OTKP'),
(29, '2018/2019', 8, 4, 10, 'X AK'),
(30, '2018/2019', 5, 4, 11, 'XI AK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kompetensi_dasar`
--

CREATE TABLE `kompetensi_dasar` (
  `id_kd` int(11) NOT NULL,
  `kode_kd` varchar(10) NOT NULL,
  `aspek` varchar(20) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kompetensi_dasar`
--

INSERT INTO `kompetensi_dasar` (`id_kd`, `kode_kd`, `aspek`, `id_mapel`, `id_kelas`, `isi`) VALUES
(1, '3.1', 'Pengetahuan', 6, 26, 'bacbcbthr');

-- --------------------------------------------------------

--
-- Struktur dari tabel `k_keahlian`
--

CREATE TABLE `k_keahlian` (
  `id_kompetensi` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `id_program` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `nama_lengkap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `k_keahlian`
--

INSERT INTO `k_keahlian` (`id_kompetensi`, `id_bidang`, `id_program`, `nama`, `nama_lengkap`) VALUES
(1, 1, 1, 'RPL', 'Rekayasa Perangkat Lunak'),
(2, 1, 1, 'TKJ', 'Teknik Komputer dan Jaringan'),
(3, 2, 2, 'OTKP', 'Otomatisasi dan Tata Kelola Perkantoran '),
(4, 2, 3, 'AKL', 'Akuntansi dan Keuangan Lembaga');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id_mapel` int(3) NOT NULL,
  `kode_mapel` varchar(10) NOT NULL,
  `nama_mapel` varchar(100) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id_mapel`, `kode_mapel`, `nama_mapel`, `tanggal`) VALUES
(1, '', 'Pendidikan Agama', '2018-08-03 03:39:52'),
(2, '1213', 'Bahasa Indonesia', '2018-08-03 03:28:08'),
(3, '', 'Pendidikan Kewarganegaraan dan Sejarah', '2018-08-03 03:40:54'),
(4, '', 'Pendidikan Jasmani dan Kesehatan', '2018-08-03 03:43:18'),
(5, '', 'Seni Budaya', '2018-08-03 03:44:51'),
(6, '', 'Matematika', '2018-08-03 03:47:11'),
(7, '', 'Bahasa Inggris', '2018-08-03 03:47:24'),
(8, '', 'Ilmu Pengetahuan Alam', '2018-08-03 03:47:40'),
(9, '', 'Ilmu Pengetahuan Sosial', '2018-08-03 03:47:57'),
(10, '', 'KKPI', '2018-08-03 03:48:09'),
(11, '', 'Kewirahusahaan', '2018-08-03 03:48:20'),
(12, '', 'Dasar Kompetensi Kejurusan', '2018-08-03 03:48:55'),
(13, '', 'Kompetensi Kejuruan', '2018-08-03 03:49:26'),
(14, '', 'BTAQ', '2018-08-03 03:49:33'),
(15, '', 'Fisika', '2018-08-03 03:50:39'),
(16, '', 'Kimia', '2018-08-03 03:50:45'),
(17, '', 'Bahasa Jawa', '2018-08-03 03:52:54');

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai`
--

CREATE TABLE `nilai` (
  `id_nilai` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nilai` varchar(255) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `nilai_ekskul`
--

CREATE TABLE `nilai_ekskul` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_ekskul` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nilai` enum('A','B','C','D') NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `nilai_ekskul`
--

INSERT INTO `nilai_ekskul` (`id`, `id_ajaran`, `id_ekskul`, `id_kelas`, `id_siswa`, `nilai`, `keterangan`, `tanggal`) VALUES
(1, 1, 1, 27, 6, 'A', 'Melaksanakan kegiatan Pramuka dengan Sangat Baik', '2018-09-21 23:42:03'),
(2, 1, 2, 27, 6, 'B', 'Melaksanakan kegiatan Bola Volly dengan Baik', '2018-09-21 23:42:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prakerin`
--

CREATE TABLE `prakerin` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nama_instansi` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `lama` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `tanggal` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prakerin`
--

INSERT INTO `prakerin` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`, `nama_instansi`, `alamat`, `lama`, `keterangan`, `tanggal`) VALUES
(1, 1, 27, 6, 'Desnet', 'jl. Kono kae no 212', '3(tiga) Bulan', 'nanannoa', '2018-09-21 23:42:20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `prestasi`
--

CREATE TABLE `prestasi` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_siswa` int(11) NOT NULL,
  `nama_prestasi` varchar(255) DEFAULT NULL,
  `keterangan` text,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `prestasi`
--

INSERT INTO `prestasi` (`id`, `id_ajaran`, `id_kelas`, `id_siswa`, `nama_prestasi`, `keterangan`, `tanggal`) VALUES
(1, 1, 27, 6, 'Juara 2 Lomba LKS sekota Semarang', 'hoya', '2018-09-21 23:42:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `p_keahlian`
--

CREATE TABLE `p_keahlian` (
  `id_program` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `p_keahlian`
--

INSERT INTO `p_keahlian` (`id_program`, `id_bidang`, `nama`) VALUES
(1, 1, 'Teknik Komputer dan Informatika'),
(2, 2, 'Manajemen Perkantoran'),
(3, 2, 'Akuntansi & Keuangan');

-- --------------------------------------------------------

--
-- Struktur dari tabel `semester`
--

CREATE TABLE `semester` (
  `id_semester` int(11) NOT NULL,
  `nama_semester` int(11) NOT NULL,
  `aktif` enum('Y','N','','') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `semester`
--

INSERT INTO `semester` (`id_semester`, `nama_semester`, `aktif`) VALUES
(1, 1, 'Y'),
(2, 2, 'N');

-- --------------------------------------------------------

--
-- Struktur dari tabel `set_nilai`
--

CREATE TABLE `set_nilai` (
  `id` int(11) NOT NULL,
  `id_ajaran` int(11) NOT NULL,
  `id_mapel` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `nama_penilaian` varchar(255) NOT NULL,
  `bobot_nilai` int(11) NOT NULL,
  `id_kd` int(11) NOT NULL,
  `id_aspek` int(11) NOT NULL,
  `keterangan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `set_nilai`
--

INSERT INTO `set_nilai` (`id`, `id_ajaran`, `id_mapel`, `id_kelas`, `nama_penilaian`, `bobot_nilai`, `id_kd`, `id_aspek`, `keterangan`) VALUES
(9, 1, 1, 26, 'UH1', 75, 7, 1, 'vbv'),
(10, 1, 1, 26, 'UK1', 75, 8, 2, 'cdcd'),
(11, 1, 2, 27, 'Tugas1', 75, 10, 1, 'nbn'),
(12, 1, 2, 27, 'portofolio', 75, 11, 2, 'sas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sikap`
--

CREATE TABLE `sikap` (
  `id` int(11) NOT NULL,
  `id_ajaran` varchar(255) NOT NULL,
  `butiran_sikap` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sikap`
--

INSERT INTO `sikap` (`id`, `id_ajaran`, `butiran_sikap`) VALUES
(1, '2018/2019', 'Positif');

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id_siswa` int(11) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `id_semester` int(11) NOT NULL,
  `nama_siswa` varchar(255) NOT NULL,
  `no_induk` varchar(255) DEFAULT NULL,
  `nisn` varchar(255) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `agama` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `anak_ke` varchar(255) DEFAULT NULL,
  `alamat` text,
  `rt` varchar(255) DEFAULT NULL,
  `rw` varchar(255) DEFAULT NULL,
  `desa_kelurahan` varchar(255) DEFAULT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `kabupaten` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `sekolah_asal` varchar(255) DEFAULT NULL,
  `diterima_kelas` varchar(255) DEFAULT NULL,
  `diterima` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `nama_ayah` varchar(255) DEFAULT NULL,
  `kerja_ayah` varchar(255) DEFAULT NULL,
  `telp_ayah` varchar(255) DEFAULT NULL,
  `nama_ibu` varchar(255) DEFAULT NULL,
  `kerja_ibu` varchar(255) DEFAULT NULL,
  `telp_ibu` varchar(255) DEFAULT NULL,
  `nama_wali` varchar(255) DEFAULT NULL,
  `kerja_wali` varchar(255) DEFAULT NULL,
  `telp_wali` varchar(255) DEFAULT NULL,
  `alamat_wali` text,
  `poto` varchar(255) DEFAULT NULL,
  `status_siswa` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id_siswa`, `id_kelas`, `id_semester`, `nama_siswa`, `no_induk`, `nisn`, `jk`, `tempat_lahir`, `tgl_lahir`, `agama`, `status`, `anak_ke`, `alamat`, `rt`, `rw`, `desa_kelurahan`, `kecamatan`, `kabupaten`, `kode_pos`, `no_hp`, `sekolah_asal`, `diterima_kelas`, `diterima`, `email`, `nama_ayah`, `kerja_ayah`, `telp_ayah`, `nama_ibu`, `kerja_ibu`, `telp_ibu`, `nama_wali`, `kerja_wali`, `telp_wali`, `alamat_wali`, `poto`, `status_siswa`, `username`, `password`, `tanggal`) VALUES
(1, 26, 1, 'Angga Dwiki Setyawan', '14.1517', '', 'Laki-laki', 'Semarang', '1999-06-04', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'keluar', 'angga', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(2, 26, 1, 'Ahmad', '14.1516', '', 'Laki-laki', 'Semarang', '2000-05-13', 'islam', 'kandung', '0', 'jl. Sesama No.212 banyumanik', '03', '07', '', 'Banyumanik', '', '', '', 'SMP N 2 Pringsurat', '', '2018-08-02', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'ahmad', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(3, 26, 1, 'Adam Jodhi Argianto', '14.1515', '', 'Laki-laki', 'Semarang', '1994-04-04', 'islam', 'kandung', '', 'jl. Sendang Elo No.9', '06', '02', '', 'Banyumanik', 'Semarang', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'adam', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:06:53'),
(4, 27, 1, 'Abdul Wahab Pratama', '14.1544', '', 'Laki-laki', 'Semarang', '0000-00-00', 'islam', 'kandung', '', 'Jurang Blimbing ', '', '', '', 'Tembalang', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'abdul', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:07:29'),
(5, 27, 1, 'eko', '14.1431', '', 'Laki-laki', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'eko', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-08 18:11:13'),
(6, 27, 1, 'Widiastuti', '14.1530', '', 'Perempuan', '', '0000-00-00', 'hindu', 'angkat', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'tuti', '2f0a8cb899a0bad307240e06c1f7036e9851672f', '2018-09-08 18:11:00'),
(7, 28, 1, 'Paijo', '14.1520', '', 'Laki-laki', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'paijo', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-17 12:33:36'),
(8, 29, 1, 'Painem', '14.1521', '', 'Perempuan', '', '0000-00-00', 'islam', 'kandung', '', '', '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', '', '', '', '', '', '', '', '', '', NULL, 'aktif', 'painem', '7c222fb2927d828af22f592134e8932480637c0d', '2018-09-23 19:40:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tahun_ajaran`
--

CREATE TABLE `tahun_ajaran` (
  `id_ajaran` int(11) NOT NULL,
  `tahun` varchar(255) DEFAULT NULL,
  `aktif` enum('Y','N','','') NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tahun_ajaran`
--

INSERT INTO `tahun_ajaran` (`id_ajaran`, `tahun`, `aktif`, `tanggal`) VALUES
(1, '2018/2019', 'Y', '2018-08-02 08:40:33'),
(2, '2019/2020', 'N', '2018-09-08 03:43:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `absensi`
--
ALTER TABLE `absensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `anggota_kelas`
--
ALTER TABLE `anggota_kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aspek`
--
ALTER TABLE `aspek`
  ADD PRIMARY KEY (`id_aspek`);

--
-- Indexes for table `b_keahlian`
--
ALTER TABLE `b_keahlian`
  ADD PRIMARY KEY (`id_bidang`);

--
-- Indexes for table `catatan_wk`
--
ALTER TABLE `catatan_wk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_sekolah`
--
ALTER TABLE `data_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ekskul`
--
ALTER TABLE `ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id_guru`);

--
-- Indexes for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  ADD PRIMARY KEY (`id_gm`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kd`
--
ALTER TABLE `kd`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `kompetensi_dasar`
--
ALTER TABLE `kompetensi_dasar`
  ADD PRIMARY KEY (`id_kd`);

--
-- Indexes for table `k_keahlian`
--
ALTER TABLE `k_keahlian`
  ADD PRIMARY KEY (`id_kompetensi`);

--
-- Indexes for table `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id_mapel`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id_nilai`);

--
-- Indexes for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prakerin`
--
ALTER TABLE `prakerin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prestasi`
--
ALTER TABLE `prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `p_keahlian`
--
ALTER TABLE `p_keahlian`
  ADD PRIMARY KEY (`id_program`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id_semester`);

--
-- Indexes for table `set_nilai`
--
ALTER TABLE `set_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sikap`
--
ALTER TABLE `sikap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  ADD PRIMARY KEY (`id_ajaran`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `absensi`
--
ALTER TABLE `absensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `anggota_kelas`
--
ALTER TABLE `anggota_kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `aspek`
--
ALTER TABLE `aspek`
  MODIFY `id_aspek` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `b_keahlian`
--
ALTER TABLE `b_keahlian`
  MODIFY `id_bidang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `catatan_wk`
--
ALTER TABLE `catatan_wk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `data_sekolah`
--
ALTER TABLE `data_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ekskul`
--
ALTER TABLE `ekskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `guru`
--
ALTER TABLE `guru`
  MODIFY `id_guru` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `guru_mapel`
--
ALTER TABLE `guru_mapel`
  MODIFY `id_gm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kd`
--
ALTER TABLE `kd`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id_kelas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `kompetensi_dasar`
--
ALTER TABLE `kompetensi_dasar`
  MODIFY `id_kd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `k_keahlian`
--
ALTER TABLE `k_keahlian`
  MODIFY `id_kompetensi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id_mapel` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id_nilai` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `nilai_ekskul`
--
ALTER TABLE `nilai_ekskul`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prakerin`
--
ALTER TABLE `prakerin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prestasi`
--
ALTER TABLE `prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `p_keahlian`
--
ALTER TABLE `p_keahlian`
  MODIFY `id_program` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `set_nilai`
--
ALTER TABLE `set_nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `sikap`
--
ALTER TABLE `sikap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tahun_ajaran`
--
ALTER TABLE `tahun_ajaran`
  MODIFY `id_ajaran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
