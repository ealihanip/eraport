<?php
function tahun_ajaran_aktif($field)
{
	$CI =& get_instance();
	$CI->db->where('aktif','Y');
	$tahun = $CI->db->get('tahun_ajaran')->row_array();
	return $tahun[$field];
}

function semester_aktif($field)
{
	$CI =& get_instance();
	$CI->db->where('aktif','Y');
	$semester = $CI->db->get('semester')->row_array();
	return $semester[$field];
}

function guru_kelas($field)
{
	$CI =& get_instance();
	$CI->db->where('kelas.id_guru', $CI->session->userdata('id_guru'));
	$kelas =$CI->db->get('kelas')->row_array();
	return $kelas[$field];

}

function aspek($aspek)
{
	$CI =& get_instance();
	$CI->db->where('aspek.id_aspek', $CI->session->userdata('id_aspek'));
	$a= $CI->db->get('aspek')->row_array();
	return $a[$aspek];
}