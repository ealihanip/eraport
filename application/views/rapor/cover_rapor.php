<html>

<head>
    <title>Laporan</title>

    <style>
        @page {
            margin-top: 2cm;
            margin-bottom: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:12pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            
        }
        .ttd{
            float:right;
            width:50%; 
            
        }

        .deskripsi{
            
            margin:0 auto;
            border-style: solid;
            border-width: 2px;
            height:60px;
            width:60%; 
            padding:3px 3px 3px 3px;
            
        }
    </style>
</head>


<body>
    <p class='font-12 center bold'>RAPOR SISWA</p>
    <p class='font-12 center bold'>SEKOLAH MENENGAH KEJURUAN (SMK)</p>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <p class='center'><img src='<?php echo base_url('assets/upload/logo/'.$data_sekolah->logo_sekolah)?>' width='300px' height='300px'></p>
    <br>
    <br>
    <br>
    <p class='font-12 center'>Nama Siswa</p>
    <div class='deskripsi'>
        <p class='font-12 center'><?php echo $siswa->nama_siswa?></p>
    </div>
    <br>
    <br>
    <br>
    <p class='font-12 center'>NISN</p>
    <div class='deskripsi'>
        <p class='font-12 center'><?php echo $siswa->nisn?></p>
    </div>
    



</body>

</html>