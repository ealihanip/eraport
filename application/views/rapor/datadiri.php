


<html>

<head>
    <title>Laporan</title>

    <style>
        
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:12pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            
        }
        .ttd{
            float:right;
            width:40%; 
            
        }

        .foto{
            float:left;
            width:3cm;
            height:4cm; 
            border-style: solid;
            border-width: 1px;
            text-align:center;
        }
    </style>
</head>


<body>

    <p class='font-12 center bold'>KETERANGAN TENTANG DIRI SISWA </p>
    
    <table>
    <tr>
        <td>
            1.
        </td>
        <td>
            Nama Siswa (Lengkap)
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->nama_siswa?>
        </td>
    </tr>
    <tr>
        <td>
            2.
        </td>
        <td>
            Nomor Induk/NISN
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->no_induk?>
        </td>
    </tr>
    <tr>
        <td>
            3.
        </td>
        <td>
            Tempat, Tanggal Lahir
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->tempat_lahir?>, <?php echo date("d-m-Y", strtotime($siswa->tgl_lahir))?>
        </td>
    </tr>
    <tr>
        <td>
            4.
        </td>
        <td>
            Jenis Kelamin
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->jk?>
        </td>
    </tr>
    <tr>
        <td>
            5.
        </td>
        <td>
            Agama
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->agama?>
        </td>
    </tr>
    <tr>
        <td>
            6.
        </td>
        <td>
            Status dalam Keluarga
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->status?>
        </td>
    </tr>
    <tr>
        <td>
            7.
        </td>
        <td>
            Anak Ke
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->anak_ke?>
        </td>
    </tr>
    <tr>
        <td>
            8.
        </td>
        <td>
            Alamat Siswa
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->alamat?>
        </td>
    </tr>
    <tr>
        <td>
            9.
        </td>
        <td>
            Nomor Telepon Rumah
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->telp_ayah?>
        </td>
    </tr>
    <tr>
        <td>
            10.
        </td>
        <td>
            Sekolah Asal
        </td>
        <td>
            :
        </td>

            <td>
            <?php echo $siswa->sekolah_asal?>
        </td>
    </tr>
    <tr>
        <td>
            11.
        </td>
        <td>
            Di terima Di sekolah Ini
        </td>
        <td>
            :
        </td>
        <td>
            
        </td>
        
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            Di kelas
        </td>
        <td>
            :
        </td>
        <td>
            X
        </td>
        
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            Pada Tanggal
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo  date("d-m-Y", strtotime($siswa->no_induk))?>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            Nama Orang Tua
        </td>
        <td>
            :
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            a. Ayah
        </td>
        <td>
            : <?php echo $siswa->nama_ayah?>
        </td>
    </tr>




    <tr>
        <td>
            
        </td>
        <td>
            b. Ibu
        </td>
        <td>
            : <?php echo $siswa->nama_ibu?>
        </td>
    </tr>
    <tr>
        <td>
            12.
        </td>
        <td>
            Alamat Orang Tua
        </td>
        <td>
            : <?php echo $siswa->alamat_wali?>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            Nomor Telepon Rumah
        </td>
        <td>
            : <?php echo $siswa->telp_ayah?>
        </td>
    </tr>
    <tr>
        <td>
            13.
        </td>
        <td>
            Pekerjaan Orang Tua
        </td>
        <td>
            :
        </td>
        
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            a. Ayah
        </td>
        <td>
            :
        </td>
        <td>
            
        </td>
        
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            b. Ibu
        </td>
        <td>
            :
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            14.
        </td>
        <td>
            Nama Wali Siswa
        </td>
        <td>
            : <?php echo $siswa->nama_wali?>
        </td>
    </tr>
    <tr>
        <td>
            15.
        </td>
        <td>
            Alamat Wali Siswa
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->alamat_wali?>
        </td>
    </tr>
    <tr>
        <td>
            
        </td>
        <td>
            Nomor Telepon Wali
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->telp_wali?>
        </td>
    </tr>
    <tr>
        <td>
            16.
        </td>
        <td>
            Pekerjaan Wali Siswa
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->alamat_wali?>
        </td>
    </tr>
    </table>
    <br>
    <br>
    <br>

    <div class='foto'>
        <br>
        <br>
        <br>
        Foto 3x4
    </div>
    <div class='ttd'>
    <p class='font-12 justify'> <?php echo $data_sekolah->kabupaten?>, </p>
    <br>
    <br>
    <br>
    <p class='font-12 justify'>Kepala Sekolah</p>
    <p class='font-12 justify'>NIP.0</p>
    </div>

   
    
</body>

</html>