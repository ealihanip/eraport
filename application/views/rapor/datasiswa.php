


<html>

<head>
    <title>Laporan</title>

    <style>
        
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:12pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            
        }
        .ttd{
            float:right;
            width:50%; 
            
        }
    </style>
</head>


<body>


    <p class='font-12 center bold'>KEMENTERIAN PENDIDIKAN DAN KEBUDAYAAN</p>
    <p class='font-12 center bold'>REPUBLIK INDONESIA</p>
    <p class='font-12 center bold'>RAPOR SISWA</p>
    <p class='font-12 center bold'>SEKOLAH MENENGAH KEJURUAN (SMK) </p>
    
    <table>
    <tr>
        <td>
            Nama Sekolah
        </td>
        <td>
            :
        </td>

        <td>
            <?php echo $data_sekolah->nama?>
        </td>
    </tr>
    <tr>
        <td>
            NPSN / NSS
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->npsn?>
        </td>
    </tr>
    <tr>
        <td>
            Alamat
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->alamat?>
        </td>
    </tr>
    <tr>
        <td>
           
        </td>
        <td>
            
        </td>
        <td>
            
        </td>
    </tr>
    <tr>
        <td>
            Kelurahan
        </td>
        <td>
            :
        </td>

        <td>
            <?php echo $data_sekolah->desa_kelurahan?>
        </td>
    </tr>
    <tr>
        <td>
            Kecamatan
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->kecamatan?>
        </td>
    </tr>
    <tr>
        <td>
            Kota/Kabupaten
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->kabupaten?>
        </td>
    </tr>
    <tr>
        <td>
            Provinsi
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->provinsi?>
        </td>
    </tr>
    <tr>
        <td>
            Website
        </td>
        <td>
            :
        </td>

        <td>
            <?php echo $data_sekolah->website?>
        </td>
    </tr>
    <tr>
        <td>
            Email
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $data_sekolah->email?>
        </td>
    </tr>
    </table>
    
    
    
    

    

    
</body>

</html>