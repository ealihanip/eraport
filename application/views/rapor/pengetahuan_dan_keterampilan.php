<html>

<head>
    <title>Laporan</title>

    <style>
        @page {
            margin-top: 2cm;
            margin-bottom: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:12pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
            height:50px;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            height:50px;
            
        }

        .table thead tr {
            
            background-color:#fce7bd;
        }

        tabel {

        }
        .ttd{
            float:right;
            width:50%; 
            
        }

        .deskripsi{
            
            margin:0 auto;
            border-style: solid;
            border-width: 2px;
            height:auto;
            width:95%; 
            padding:3px 3px 3px 3px;
            
        }

        .tabelizin {
            border-collapse: collapse;
            width:40%;
        }

        .tabelizin th {
            text-align:center;
            border: 1px solid black;
            height:50px;
        }

        .tabelizin td {
            border: 1px solid black;
            font-size:12pt;
            height:50px;
            
        }

        .tabelizin thead tr {
            
            background-color:#fce7bd;
        }
    </style>
</head>


<body>
        
    <p class='font-12 bold'>B. Pengetahuan Dan Keterampilan</p>

    <table class='table'>
        <thead>
            <tr>    
                <th rowspan='2'>
                    No
                </th>

                <th rowspan='2' width='300px'>
                    Mata Pelajaran
                </th>
                <th colspan='4'>
                    Pengetahuan
                </th>
                <th colspan='4'>
                    Keterampilan
                </th>
            </tr>
            <tr>    
                <th>
                    KKM
                </th>

                <th>
                    Angka
                </th>

                <th>
                    Predikat
                </th>

                <th>
                    Deskripsi
                </th>

                <th>
                    KKM
                </th>

                <th>
                    Angka
                </th>

                <th>
                    Predikat
                </th>

                <th>
                    Deskripsi
                </th>
                
            </tr>
        </thead>
        <tbody>

            <?php $no=1; foreach($golongan_mapel as $data_golongan_mapel){ $first=true;?>
                    
                    
                
                    <?php $no=1; foreach($mapel as $datamapel){?>

                        <?php if($data_golongan_mapel->id==$datamapel['id_golongan']){?>
                            
                            <?php if($first==true){?>
                                
                                <tr>
                                    <td colspan='10'>
                                        Kelompok <?php echo $data_golongan_mapel->nama?>
                                    </td>
                                <tr>

                            <?php $first=false;}?>
                            

                            <tr>

                                <td align='center'>   
                                    <?php echo $no;$no++; ?>
                                    
                                </td>
                                <td>   
                                    <?php echo $datamapel['nama_mapel']?>
                                </td>
                                
                                
                                
                                <?php foreach($datamapel['nilai'] as $datanilai){?>

                                    <?php if($datanilai['id_aspek']==1){?>
                                        <td align='center'>   
                                            <?php echo $datanilai['kkm']?>
                                        </td>
                                        <td align='center'> 
                                            <?php echo $datanilai['nilai_akhir']?>
                                        </td>
                                        <td align='center'>    
                                            <?php echo $datanilai['predikat']?>
                                        </td>

                                        <td>   
                                            <?php echo $datanilai['deskripsi']?>
                                        </td>
                                    <?php }?>
                                <?php }?>
                                <?php foreach($datamapel['nilai'] as $datanilai){?>

                                    <?php if($datanilai['id_aspek']==2){?>

                                        <td align='center'>   
                                            <?php echo $datanilai['kkm']?>
                                        </td>
                                        <td align='center'> 
                                            <?php echo $datanilai['nilai_akhir']?>
                                        </td>
                                        <td align='center'>    
                                            <?php echo $datanilai['predikat']?>
                                        </td>

                                        <td>   
                                            <?php echo $datanilai['deskripsi']?>
                                        </td>
                                    <?php }?>
                                <?php }?>

                            </tr>
                            
                            
                        <?php }?>
                            

                    <?php }?>

            <?php }?>
            

        </tbody>
    </table>
    <br>

    <p class='font-12 bold'>C. Praktik Kerja Lapangan</p>
    
    <table class='table'>

        <thead>

            <tr>
                <th>
                    No
                </th>

                <th>
                    Mitra DU/DI
                </th>

                <th>
                    Lokasi
                </th>

                <th>
                    Lamanya (bulan)
                </th>

                <th>
                    Keterangan
                </th>
            </tr>
            
        </thead>
        
        <tbody>

            <?php if(empty($prakerin)){?>
                <tr>
                    <td align='center'>
                        -
                    </td>

                    <td align='center'>
                    -
                    </td>

                    <td align='center'>
                    -
                    </td>

                    <td align='center'>
                    -
                    </td>

                    <td align='center'>
                    -
                    </td>
                </tr>
            <?php }?>
            <?php $no=1; foreach($prakerin as $data_prakerin){?>
                <tr>
                    <td align='center'>
                        <?php echo $no?>
                    </td>

                    <td align='center'>
                        <?php echo $data_prakerin->nama_instansi?>
                    </td>

                    <td align='center'>
                        <?php echo $data_prakerin->alamat?>
                    </td>

                    <td align='center'>
                        <?php echo $data_prakerin->lama?>
                    </td>

                    <td align='center'>
                        <?php echo $data_prakerin->keterangan?>
                    </td>
                </tr>
            <?php $no++; }?>
        </tbody>




    </table>

    <p class='font-12 bold'>E. Ekstrakulikuler</p>
    
    <table class='table'>

        <thead>

            <tr>
                <th>
                    No
                </th>

                <th>
                    Kegiatan Ekstrakulikuler
                </th>

                <th>
                    Keterangan
                </th>

            </tr>
            
        </thead>
        
        <tbody>

            <?php if(empty($ekskul)){?>
                <tr>
                    <td align='center'>
                        -
                    </td>

                    <td align='center'>
                    -
                    </td>

                    <td align='center'>
                    -
                    </td>
                </tr>
            <?php }?>
            <?php $no=1; foreach($ekskul as $data_ekskul){?>
                <tr>
                    <td align='center'>
                        <?php echo $no?>
                    </td>

                    <td align='center'>
                        <?php echo $data_ekskul->nama_ekskul?>
                    </td>

                    <td align='center'>
                        <?php echo $data_ekskul->deskripsi?>
                    </td>

                </tr>
            <?php $no++; }?>
        </tbody>




    </table>

    <p class='font-12 bold'>F. Prestasi</p>
    
    <table class='table'>

        <thead>

            <tr>
                <th>
                    No
                </th>

                <th>
                    Jenis Prestasi
                </th>

                <th>
                    Keterangan
                </th>

            </tr>
            
        </thead>
        
        <tbody>

            <?php if(empty($prestasi)){?>
                <tr>
                    <td align='center'>
                        -
                    </td>

                    <td align='center'>
                    -
                    </td>

                    <td align='center'>
                    -
                    </td>
                </tr>
            <?php }?>
            <?php $no=1; foreach($prestasi as $prestasi){?>
                <tr>
                    <td align='center'>
                        <?php echo $no?>
                    </td>

                    <td align='center'>
                        <?php echo $prestasi->nama_prestasi?>
                    </td>

                    <td align='center'>
                        <?php echo $prestasi->keterangan?>
                    </td>

                </tr>
            <?php $no++; }?>
        </tbody>

    </table>

    <p class='font-12 bold'>G. Ketidakhadiran </p>
    
    <table class='tabelizin'>
        <?php if(!empty($ketidakhadiran)){?>
        <tbody>
            <tr>
                <td width='50%'>
                    Sakit
                </td>
                <td width='50%'>
                    : <?php echo $ketidakhadiran->sakit?>  
                </td>
            </tr>

            <tr>
                <td>
                    izin 
                </td>
                <td>
                    : <?php echo $ketidakhadiran->izin?> 
                </td>
            </tr>

            <tr>
                <td>
                    Tanpa Keterangan
                </td>
                <td>
                    : <?php echo $ketidakhadiran->tanpa_keterangan?> 
                </td>
            </tr>
            
        </tbody>
        <?php }?>
    </table>

</body>

</html>