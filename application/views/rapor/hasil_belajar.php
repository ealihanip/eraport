<html>

<head>
    <title>Laporan</title>

    <style>
        @page {
            margin-top: 2cm;
            margin-bottom: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:12pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            
        }
        tabel {

        }
        .ttd{
            float:right;
            width:50%; 
            
        }

        .deskripsi{
            
            margin:0 auto;
            border-style: solid;
            border-width: 2px;
            height:auto;
            width:95%; 
            padding:3px 3px 3px 3px;
            
        }
    </style>
</head>


<body>
    <table>
    <tr>
        <td width='175px'>
            Nama Sekolah
        </td>
        <td width='10px'>
            :
        </td>

        <td width='400px'>
            <?php echo $data_sekolah->nama?>
        </td>
        <td>
            Kelas
        </td>
        <td width='10px'>
            :
        </td>
        <td>
            <?php echo $siswa->nama_jurusan?>
        </td>
    </tr>
    <tr>
        <td>
            Alamat
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->alamat?>
        </td>
        <td>
            Semester
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $semester?>
        </td>
    </tr>
    <tr>
        <td>
            Nama Siswa
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->nama_siswa?>
        </td>
        <td>
            Tahun Pelajaran
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $tahun_ajaran->tahun?>
        </td>
    </tr>
    <tr>
        <td>
            Nomor Induk/NISN
        </td>
        <td>
            :
        </td>
        <td>
            <?php echo $siswa->no_induk?>
        </td>
    </tr>
    </table>

    <p class='font-12 justify bold'>CAPAIAN HASIL BELAJAR</p>
    <p class='font-12 justify'>A. Sikap</p>
    
    <div class='deskripsi'>

    <p class='font-12 justify'>Deskripsi: <?php if(!empty($sikap)){?><?php echo $sikap[0]->sikap?><?php }?></p>
    </div>


</body>

</html>