<html>

<head>
    <title>Laporan</title>

    <style>
        @page {
            margin-top: 2cm;
            margin-bottom: 2cm;
            margin-left: 2cm;
            margin-right: 2cm;
        }
        .font-16{

            font-size:16pt;
        }

        .font-12{

            font-size:10pt;
        }

        .font-14{

            font-size:14pt;
        }

        .bold{
            font-weight: bold;
        }

        .bold{
            font-weight: bold;
        }

        .center{
            text-align: center;
        }
        .right{
            text-align: right;
        }
        .left{
            text-align: left;
        }

        .underline{
            text-decoration: underline;
        }

        .justify{
            text-align: justify;
        }

        body{
            font-family: "Times New Roman", Times, serif;
            line-height: 12pt;
        }

        br {
            display: block;
            margin: 1pt;
            line-height: 0;
        }
        
        .table {
            border-collapse: collapse;
            width:100%;
            margin:0 auto;
        }

        .table th {
            text-align:center;
            border: 1px solid black;
        }

        .table td {
            border: 1px solid black;
            font-size:12pt;
            
        }
        .ttd{
            float:right;
            width:30%; 
            
        }
        .ttd1{
            float:left;
            width:50%; 
            
        }

        .deskripsi{
            
            margin:0 auto;
            border-style: solid;
            border-width: 2px;
            height:auto;
            width:95%; 
            padding:3px 3px 3px 3px;
            
        }
        .tanggapan{
            
            margin:0 auto;
            border-style: solid;
            border-width: 2px;
            height:80px;
            width:95%; 
            padding:3px 3px 3px 3px;
            
        }
    </style>
</head>


<body>
    <p class='font-12 justify'>H. Catatan Wali Kleas</p>

    
    <div class='deskripsi'>
        <?php if(!empty($catatan)){?>
            <?php echo $catatan->catatan?>
        <?php }else{?>
            Belum Di Isi
        <?php }?>
    </div>
    
    <p class='font-12 justify'>I. Tanggapan Orang Tua/Wali</p>
    <div class='tanggapan'>
    
    </div>
    <?php if($semester!=1){?>

        <p class='font-12 justify bold'>Keputusan:</p>
        
        <?php if($kelulusan['lulus']==true){?>
            <p class='font-12 justify'>Berdasarkan hasil yang dicapai pada semester 1 dan 2, peserta didik ditetapkan Naik ke kelas <?php echo $kelulusan['di_kelas']?> (<?php echo $kelulusan['nama_jurusan']?>)</p>
        
        <?php }else{?>
            <p class='font-12 justify'>Berdasarkan hasil yang dicapai pada semester 1 dan 2, peserta didik ditetapkan Tinggal di kelas <?php echo $kelulusan['di_kelas']?> (<?php echo $kelulusan['nama_jurusan']?>) </p>
        
        <?php }?>
        
        <br>
        
        
        

    <?php }else{?>
        <br>
        <br>
        <br>
        <br>
    <?php }?>
    
    <div class='ttd1'>
    <p class='font-12 justify'>Mengetahui;</p>
    <p class='font-12 justify'>Orang Tua/Wali</p>
    <br>
    <br>
    <br>
    <p class='font-12 justify'>.........................................</p>
    </div>

    <div class='ttd'>
    <p class='font-12 justify'><?php echo $data_sekolah->kabupaten?>, .................................,20.... </p>
    <p class='font-12 justify'>Wali Kelas</p>
    <br>
    <br>
    <br>
    <p class='font-12 inline justify'><?php echo $kelas[0]->nama_guru?></p>
    <p class='font-12 justify'>NIP.</p>
    </div>

   
    
    <div class='ttd'>
    <p class='font-12 justify'>Mengetahui, </p>
    <p class='font-12 justify'>Kepala Sekolah</p>
    <br>
    <br>
    <br>
    <p class='font-12 inline justify'>Slamet</p>
    <p class='font-12 justify'>NIP.</p>
    </div>


</body>

</html>