<!-- Site footer -->
<footer class="footer">
<p>&copy; <?php echo date('Y')?>. Sistem Informasi Pengolahan Data Nilai Siswa</p>
</footer>

</div> <!-- /container -->
<!-- javascript bootstrap -->
<script src="<?php echo base_url()?>assets/home/js/bootstrap.min.js"></script>


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <script src="js/ie10-viewport-bug-workaround.js"></script> -->
</body>
</html>
