<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="<?php echo $title ?>">
<meta name="author" content="<?php echo $title ?>">
<link rel="icon" href="../../favicon.ico">

<title><?php echo $title ?></title>

<!-- Bootstrap core CSS -->
<link href="<?php echo base_url()?>assets/home/css/bootstrap.min.css" rel="stylesheet">

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!-- <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet"> -->

<!-- Custom styles for this template -->
<link href="<?php echo base_url()?>assets/home/css/style.css" rel="stylesheet">

<!-- carousel -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/home/css/carousel.css">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!-- <script src="js/ie-emulation-modes-warning.js"></script> -->

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- javascript jquery -->
<script src="<?php echo base_url()?>assets/plugins/jquery-ui-1.12.1/external/jquery/jquery.js" type="text/javascript"></script>
</head>

<body>

<div class="container">
