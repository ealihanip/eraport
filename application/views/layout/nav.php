<!-- Static navbar -->
<nav class="navbar navbar-inverse">
<div class="container-fluid">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
<span class="sr-only">Toggle navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div id="navbar" class="navbar-collapse collapse">
<ul class="nav navbar-nav">
<li class="active"><a href="<?php echo base_url()?>">Home</a></li>
<li><a href="<?php echo base_url('fasilitas')?>">Fasilitas</a></li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profile<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="<?php echo base_url('sejarah')?>">sejarah</a></li>
<li><a href="<?php echo base_url('visi_misi')?>">Visi & Misi</a></li>
</ul>
</li>
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Informasi<span class="caret"></span></a>
<ul class="dropdown-menu">
<li><a href="<?php echo base_url('guru')?>">Data Guru</a></li>
<li><a href="<?php echo base_url('Jurusan')?>">Jurusan</a></li>
<li><a href="<?php echo base_url('ekstrakurikuler')?>">Ekstrakurikuler</a></li>
</ul>
</li>
<li><a href="<?php echo base_url('kontak')?>">Kontak</a></li>

</ul>
</div><!--/.nav-collapse -->
</div><!--/.container-fluid -->
</nav>
</div>
