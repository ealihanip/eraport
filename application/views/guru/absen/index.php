

<?php


//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
	echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}



?>


<div class='col-lg-12'>


    <form method='post' action='<?php echo base_url('guru/absen/store')?>'>
    
    </div>

    <table class='table table-striped'>
		<thead>
			<tr>
				<th class='text-center' width='300px'>
					Nama Siswa
				</th>
				<th class='text-center' >
					Izin
				</th>
				<th class='text-center' >
					Sakit
				</th>
				<th class='text-center' >
					Tanpa Keterangan
				</th>
			</tr>

			
			
		</thead>

		<tbody>

			<?php foreach($siswa as $data_siswa){?>
				<tr>
					<td>
						<?php echo $data_siswa->nama_siswa?>
					</td>

					

						<?php if(!empty($absen)){?>

							<?php foreach($absen as $data_absen){?>

								<?php if($data_absen->id_siswa==$data_siswa->id_siswa){?>

									<input type='hidden' name='id_absen[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_absen->id?>'>
									<div class="form-group">

									<td>
										<input type='text' name="sakit[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" value='<?php echo $data_absen->sakit?>'>
									</td>
									<td>	
										<input type='text' name="izin[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" value='<?php echo $data_absen->izin?>'>
									</td>
									<td>
										<input type='text' name="tanpa_keterangan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" value='<?php echo $data_absen->tanpa_keterangan?>'>
									</td>
									
									</div>
								<?php }?>

							<?php }?>
						<?php }else{?>
							<div class="form-group">
									<td>
										<input type='text' name="sakit[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" >
									</td>
									<td>	
										<input type='text' name="izin[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" >
									</td>
									<td>
										<input type='text' name="tanpa_keterangan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="" >
									</td>
							</div>
						<?php }?>
					
				</tr>
			<?php }?>
		</tbody>
	</table>

    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
    </div>

    
    </form>

</div>

