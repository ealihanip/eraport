

<?php


//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
	echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}



?>


<div class='col-lg-12'>


    <form method='post' action='<?php echo base_url('guru/deskripsi_ekskul/store?ekskul='.$id_ekskul)?>'>
    


	<div class="form-group">
        <label>Mata Pelajaran</label>
        <select class="form-control" name="id_ekskul" onchange='window.location="<?php echo base_url("guru/deskripsi_ekskul/index?")?>ekskul="+this.options[this.selectedIndex].value'>
            
			<option>Pilih Mata Pelajaran</option>
			<?php foreach ($ekskul as $ekskul ) { ?>
				<option value="<?php echo $ekskul->id_ekskul ?>"<?php if ($ekskul->id_ekskul==$id_ekskul) {
                    echo "selected";} ?>>
                    <?php echo $ekskul->nama_ekskul ?>
                </option>
                <?php } ?>
        </select>
    </div>

    <table class='table table-striped'>
		<thead>
			<tr>
				<th class='text-center'>
					Nama Siswa
				</th>
				<th class='text-center' >
					Deskripsi Mata Pelajaran
				</th>
			</tr>

			
			
		</thead>

		<tbody>

			<?php foreach($siswa as $data_siswa){?>
				<tr>
					<td>
						<?php echo $data_siswa->nama_siswa?>
					</td>

					<td>

						<?php if(!empty($deskripsi_ekskul)){?>

							<?php foreach($deskripsi_ekskul as $data_deskripsi_ekskul){?>

								<?php if($data_deskripsi_ekskul->id_siswa==$data_siswa->id_siswa){?>

									<input type='hidden' name='id_deskripsi_ekskul[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_deskripsi_ekskul->id?>'>
									<div class="form-group">
										<textarea name="pengetahuan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="Pengetahuan"><?php echo $data_deskripsi_ekskul->pengetahuan?></textarea>
									</div>

									<div class="form-group">
										<textarea name="keterampilan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="Keterampilan"><?php echo $data_deskripsi_ekskul->keterampilan?></textarea>
									</div>
								<?php }?>

							<?php }?>
						<?php }else{?>
							<div class="form-group">
								<label>Pengetahuan</label>
								<textarea name="pengetahuan[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Pengetahuan" rows='3' ></textarea>
								<label>Keterampilan</label>
								<textarea name="keterampilan[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Keterampilan" rows='3' ></textarea>

							</div>
						<?php }?>
					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>

    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
    </div>

    
    </form>

</div>

