<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama Siswa</th>
            <th>No Induk</th>
            <th>Jenis kelamin</th>
            <th width="10%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($kelas as $kelas) 
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $kelas->nama_siswa ?></td>
        <td><?php echo $kelas->no_induk ?></td>
        <td><?php echo $kelas->jk ?></td>
        <td><?php include('detail.php'); ?></td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>