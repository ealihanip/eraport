<?php
//notifikaksi kala ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//notifikasi
if ($this->session->flashdata('sukses')) 
{
	echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}

//open form
echo form_open(base_url('guru/dasbor/profile'));
?>

<div class="col-md-6">
	<div class="form-group">
		<label>Nama</label>
		<input type="text" name="nama_guru" class="form-control" value="<?php echo $guru->nama_guru ?>">
	</div>

	<div class="form-group">
		<label>Email</label>
		<input type="email" name="email" class="form-control" placeholder="Masukkan Email" value="<?php echo  $guru->email ?>">
	</div>

	<div class="form-group">
		<label>Username</label>
		<input type="text" name="username" class="form-control" placeholder="Masukkan Username" value="<?php echo $guru->username ?>" >
	</div>

	<div class="form-group">
		<label>Password<span class="text-danger"><small>(password minimal 5 karakter atau dibiarkan kosong)</small></span></label>
		<input type="password" name="password" class="form-control" placeholder="Masukkan Password" value="<?php echo set_value('password') ?>">
	</div>

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>
</div>


<?php
//form close
echo form_close();
?>