 <?php
//dapatkan id user saat login
$id_guru 	  = $this->session->userdata('id_guru');
$level 		  = $this->session->userdata('akses_level');
$user_aktif = $this->model_guru->detail($id_guru);

?>
<h3>Selamat Datang <strong><?php echo $user_aktif->nama_guru ?></strong></h3>   
<hr />
<h3>Mata Pelajaran yang diampu di Tahun Pelajaran <strong><?php echo tahun_ajaran_aktif('tahun')?></strong>  Semester <strong><?php echo semester_aktif('nama_semester')?></strong></h3>

<table width="100%" class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
           <th>Kelas</th>
		<th>Mapel</th>
        </tr>
    </thead>
    <tbody>
        <?php $no=1; foreach ($gm as $gm) { ?> 
   <tr>
			<td><?php echo $gm->nama_jurusan ?></td>
			<td><?php echo $gm->nama_mapel ?></td>
    </tr>
    <?php $no++; } ?>
</tbody>
</table>