<html>

    <head>
        <style>

            .table {
                border-collapse: collapse;
                width:100%;
                margin:0 auto;
            }

            .table th {
                text-align:center;
                border: 1px solid black;
            }

            .table td {
                border: 1px solid black;
                font-size:12pt;
                
            }
        </style>
    </head>


    <body>



        <table class='table'>
            <thead>
                <tr>
                    <th rowspan='2'>
                        No
                    </th>

                    <th rowspan='2'>
                        Mata Pelajaran
                    </th>

                    <th colspan='4'>
                        Pengetahuan
                    </th>

                    <th colspan='4'>
                        Keterampilan
                    </th>

                </tr>

                <tr>
                   
                    
                    <th>
                        KKM
                    </th>

                    <th>
                        Angka
                    </th>

                    <th>
                        Predikat
                    </th>

                    <th>
                        Deskripsi
                    </th>

                    <th>
                        KKM
                    </th>

                    <th>
                        Angka
                    </th>

                    <th>
                        Predikat
                    </th>

                    <th>
                        Deskripsi
                    </th>

                </tr>
            </thead>

            <tbody>

                <?php $no=1; foreach($mapel as $datamapel){?>
                <tr>

                    <td>   
                        <?php echo $no?>
                    </td>
                    <td>   
                        <?php echo $datamapel['nama_mapel']?>
                    </td>
                    
                   
                    
                    <?php foreach($datamapel['nilai'] as $datanilai){?>

                        <?php if($datanilai['id_aspek']==1){?>
                            <td>   
                                75
                            </td>
                            <td>
                                <?php echo $datanilai['nilai_akhir']?>
                            </td>
                            <td>   
                                Predikat
                            </td>

                            <td>   
                                Deskripsi
                            </td>
                        <?php }?>
                    <?php }?>
                    <?php foreach($datamapel['nilai'] as $datanilai){?>

                        <?php if($datanilai['id_aspek']==2){?>

                            <td>   
                                kkm
                            </td>

                            <td>
                                <?php echo $datanilai['nilai_akhir']?>
                            </td>
                            <td>   
                                Predikat
                            </td>

                            <td>   
                                Deskripsi
                            </td>
                        <?php }?>
                    <?php }?>

                </tr>

                <?php $no++; }?>

            </tbody>
        </table>


        

    </body>


</html>