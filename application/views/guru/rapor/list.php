<?php
    
?>






<div class='row'>

    <div class='col-lg-12'>

        <table width="100%" class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th width="40%">Nama Siswa</th>
                    <th width="20%">Cetak Rapor(PDF)</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($siswa as $siswa) { ?>
            <tr class="odd gradeX">
                <td><?php echo $siswa->nama_siswa?></td>
                <td><a href="<?php echo base_url('guru/rapot/cetak/'.$siswa->id_siswa)?>" class="btn btn-info btn-lg" ><i class="fa fa-file-pdf-o"></i></a></td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

    </div>

</div>
<!-- <script type="text/javascript">
    $('.tooltip-left').tooltip({
        placement: 'left',
        viewport:{selector: 'body', padding:2}
    })
</script> -->

