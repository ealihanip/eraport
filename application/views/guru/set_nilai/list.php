
<p><a href="<?php echo base_url('guru/set_nilai/tambah_'.$key)?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="5%">Tahun Ajaran</th>
            <th width="5%">Kelas</th>
            <th width="10%">Nama Mapel</th>
            <th width="2%">Kode KD</th>
            <th width="2%">Nama Penilaian</th>
            <th width="2%">Bobot</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
    
         <?php 
            $i=1;
            foreach ($sn as $sn)
          { 
        ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $sn->tahun?></td>
        <td><?php echo $sn->nama_jurusan?></td>
        <td><?php echo $sn->nama_mapel?></td>
        <td><?php echo $sn->kode_kd?></td>
        <td><?php echo $sn->nama_penilaian?></td>
        <td><?php echo $sn->bobot_nilai?></td>
        <td>
            <a href="<?php echo base_url('guru/set_nilai/edit_'.$key.'/'.$sn->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>