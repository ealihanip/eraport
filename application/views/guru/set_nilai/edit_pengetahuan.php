<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('guru/set_nilai/edit_pengetahuan/'.$sn->id));
?>

<div class="col-md-8">


	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/set_nilai/edit_".$key."/".$sn->id."?")?>kelas="+this.options[this.selectedIndex].value'>
			<option value="">== Pilih Kelas ==</option>
			<?php foreach ($mapel as $kelas ) { ?>
				<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
					<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Mata Pelajaran</label>
		<select class="form-control js-example-basic-single" name="id_mapel" onchange='window.location="<?php echo base_url("guru/set_nilai/edit_".$key."/".$sn->id."/".$sn->id."?"."kelas=".$id_kelas)?>&mapel="+this.options[this.selectedIndex].value'>
			<option value="">== Pilih Mata Pelajaran ==</option>

			<?php if(isset($id_kelas) && !empty($id_kelas)){?>
				<?php foreach ($mapel as $mapel ) { ?>
					<option value="<?php echo $mapel->id_mapel ?>" <?php if($kelas->id_mapel==$id_mapel){echo "selected";}?>>
						<?php echo $mapel->nama_mapel ?>
					</option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>
	
	<div class="form-group">
		<label>Nama Penilaian</label>
		<input type="text" name="nama_penilaian" class="form-control" placeholder="UH/UTS/UAS dll..." value='<?php echo $sn->nama_penilaian?>'>
	</div>

	<div class="form-group">
		<label>Bobot Nilai </label>
		<input type="text" name="bobot_nilai" class="form-control input-sm" value='<?php echo $sn->bobot_nilai?>'>
	</div>

	<div class="form-group">
		<label>Kode KD</label>
		<select class="form-control" name="id_kd">
			<option value="">==Pilih Kode Kompetensi Dasar==</option>
			<?php if(isset($id_mapel) && !empty($id_mapel)){?>
				<?php foreach ($kd as $kd) { ?>
				<option value="<?php echo $kd->id_kd?>" <?php if($kd->id_kd==$sn->id_kd){ echo'selected';}?>>
					<?php echo $kd->kode_kd ?>
				</option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>


	<div class="form-group">
		<label>Keterangan</label>
		<input type="text" name="keterangan" class="form-control" value='<?php echo $sn->keterangan?>'>
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Proses">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('guru/set_nilai')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>

<!-- <div class="form-group">
		<label>Kode KD</label>
		<br>
		<?php foreach ($kd as $kd) { ?>
		<label>
			<a href="javascript:void(0)" title="<?php echo $kd->isi_kd ?>"><?php echo $kd->kode_kd ?></a></label>
			<input type="hidden" name="id_kd">
			<input type="checkbox" name="kode_kd">
		<?php } ?>
	</div> -->