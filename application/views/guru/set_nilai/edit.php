<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('guru/set_nilai/edit/'.$sn->id));
?>

<div class="col-md-8">

	<div class="form-group">
		<label>Tahun Ajaran</label>
		<select class="form-control" name="id_ajaran">
			<?php foreach ($ajaran as $ajaran ) { ?>
				<option value="<?php echo $ajaran->id_ajaran ?>"<?php if ($sn->id_ajaran==$ajaran->id_ajaran) {
                    echo "selected";} ?>>
					<?php echo $ajaran->tahun ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="id_kelas">
			<option value="">== Pilih Kelas ==</option>
			<?php foreach ($kelas as $kelas ) { ?>
				<option value="<?php echo $kelas->id_kelas ?>"<?php if ($sn->id_kelas==$kelas->id_kelas) {
                    echo "selected";} ?>>
					<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Mata Pelajaran</label>
		<select class="form-control" name="id_mapel">
			<option value="">== Pilih Mata Pelajaran ==</option>
			<?php foreach ($mapel as $mapel ) { ?>
				<option value="<?php echo $mapel->id_mapel ?>"<?php if ($sn->id_mapel==$mapel->id_mapel) {
                    echo "selected";} ?>>
					<?php echo $mapel->nama_mapel ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Nama Penilaian</label>
		<input type="text" name="nama_penilaian" class="form-control" placeholder="UH/UTS/UAS dll..." value="<?php echo $sn->nama_penilaian?>">
	</div>

	<div class="form-group">
		<label>Bobot Nilai</label>
		<input type="text" name="bobot_nilai" class="form-control input-sm" value="<?php echo $sn->bobot_nilai?>">
	</div>

	<div class="form-group">
		<label>Kode KD</label>
		<select class="form-control" name="id_kd">
			<option value="">==Pilih Kode Kompetensi Dasar==</option>
			<?php foreach ($kd as $kd) { ?>
			<option value="<?php echo $kd->id_kd?>"<?php if ($sn->id_kd==$kd->id_kd) {
				echo "selected";} ?>>
				<?php echo $kd->kode_kd ?>
			</option>
		<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Aspek Penilaian</label>
		<select class="form-control" name="id_aspek">
			<?php foreach ($aspek as $aspek) { ?>
				<option value="<?php echo $aspek->id_aspek?>"<?php if ($sn->id_aspek==$aspek->id_aspek) {
					echo "selected"; }?>>
					<?php echo $aspek->nama_aspek?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Keterangan</label>
		<input type="text" name="keterangan" class="form-control" value="<?php echo $sn->keterangan?>">
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Proses">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('guru/set_nilai/rencana/'.$sn->id_aspek)?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>