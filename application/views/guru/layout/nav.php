<?php
//dapatkan id user saat login
$id_guru        = $this->session->userdata('id_guru');
$akses_level    = $this->session->userdata('akses_level');
$user_aktif     = $this->model_guru->detail($id_guru);
?>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <?php if($this->session->userdata('akses_level') === '1'): ?>
            <li><a href="<?php echo base_url('guru/dasbor')?>"><i class="fa fa-dashboard "></i> Dasbor</a></li>
            <!-- Menu Guru  -->
            <li><a href="<?php echo base_url('guru/data_guru')?>"><i class="fa fa-users"></i> Guru</a></li>
             <!-- Menu Siswa  -->
            <li><a href="<?php echo base_url('guru/data_siswa')?>"><i class="fa fa-graduation-cap"></i> Siswa</a></li>
            <!-- Kompetensi Dasar -->
            <li><a href="<?php echo base_url('guru/data_kd')?>"><i class="fa fa-book"></i> Kompetensi Dasar</a></li>
            <!-- set Nilai -->
           <!--   <li><a href="<?php echo base_url('guru/set_nilai')?>"><i class="fa fa-file"></i> Perencanaan <span class="fa arrow"> </span></a> -->
            <li><a href="#"><i class="fa fa-file"></i> Perencanaan<span class="fa arrow"> </span></a>
               <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/set_nilai/pengetahuan')?>">Penilaian Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/set_nilai/keterampilan')?>">Penilaian Keterampilan</a></li>
                </ul>
            </li>  
            <!-- Penilaian -->
            <li><a href="#"><i class="fa fa-file"></i> Penilaian<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/nilai/pengetahuan')?>">Penilaian Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/nilai/keterampilan')?>">Penilaian Keterampilan</a></li>
                    
                    <li><a href="<?php echo base_url('guru/deskripsi_mapel')?>">Deskripsi Mapel</a></li>
                </ul>
            </li>  

            <?php elseif($this->session->userdata('akses_level') === '2'): ?>
            <li><a href="<?php echo base_url('guru/dasbor')?>"><i class="fa fa-dashboard "></i> Dasbor</a></li>
            <!-- Menu Guru  -->
            <li><a href="<?php echo base_url('guru/data_guru')?>"><i class="fa fa-users"></i> Guru</a></li>
             <!-- Menu Siswa  -->
            <li><a href="<?php echo base_url('guru/data_siswa')?>"><i class="fa fa-graduation-cap"></i> Siswa</a></li>
              <!-- Kelas -->
            <li><a href="<?php echo base_url('guru/kelas')?>"><i class="fa fa-clipboard"></i> Kelas </a></li>
            <!-- Mapel -->
            <li><a href="<?php echo base_url('guru/data_kd')?>"><i class="fa fa-book"></i> Kompetensi Dasar</a></li>  
            <!-- set Nilai -->
            
            <!-- Perencanaan -->
            <li><a ><i class="fa fa-file"></i>Perencanaan<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/set_nilai/pengetahuan')?>">Perancanaan Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/set_nilai/keterampilan')?>">Perancanaan Keterampilan</a></li>
                </ul>
            </li>

            <!-- Penilaian -->
            <li><a href="#"><i class="fa fa-file"></i> Penilaian<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/nilai/pengetahuan')?>">Penilaian Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/nilai/keterampilan')?>">Penilaian Keterampilan</a></li>
                    <li><a href="<?php echo base_url('guru/deskripsi_mapel')?>">Deskripsi Mapel</a></li>
                </ul>
            </li>

             <!-- Laporan hasil Belajar -->
            <li><a href="#"><i class="fa fa-file"></i>Laporan Hasil Belajar<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/catatan_wali_kelas')?>">Catatan Wali Kelas</a></li>
                    <li><a href="<?php echo base_url('guru/sikap')?>">Sikap</a></li>
                    <li><a href="<?php echo base_url('guru/absen')?>">Absensi</a></li>
                    <li><a href="<?php echo base_url('guru/nilai_ekskul')?>">Ekstrakurikuler</a></li>
                    <li><a href="<?php echo base_url('guru/prakerin')?>">Praktek Kerja Lapangan</a></li>
                    <li><a href="<?php echo base_url('guru/prestasi')?>">Prestasi</a></li>
                    <li><a href="<?php echo base_url('guru/rapot')?>">Cetak Raport</a></li>
                </ul>
            </li>  

            <?php else: ?>
            <li><a href="<?php echo base_url('guru/dasbor')?>"><i class="fa fa-dashboard "></i> Dasbor</a></li>
            <!-- Menu Guru  -->
            <li><a href="<?php echo base_url('guru/data_guru')?>"><i class="fa fa-users"></i> Guru</a></li>
             <!-- Menu Siswa  -->
            <li><a href="<?php echo base_url('guru/data_siswa')?>"><i class="fa fa-graduation-cap"></i> Siswa</a></li>
            <!-- Mapel -->
            <li><a href="<?php echo base_url('guru/data_kd')?>"><i class="fa fa-book"></i> Kompetensi Dasar</a></li>  
             <!-- Kelas -->
            <li><a href="<?php echo base_url('guru/kelas')?>"><i class="fa fa-clipboard"></i> Kelas </a></li>
            <!-- set Nilai -->
           <!--  <li><a href="#"><i class="fa fa-file"></i> Perencanaan<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/set_nilai/pengetahuan')?>">Penilaian Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/set_nilai/keterampilan')?>">Penilaian Keterampilan</a></li>
                </ul>
            </li>   -->
             <!-- Penilaian -->
            <!-- <li><a href="#"><i class="fa fa-file"></i> Penilaian<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('guru/nilai/pengetahuan')?>">Penilaian Pengetahuan</a></li>
                    <li><a href="<?php echo base_url('guru/nilai/keterampilan')?>">Penilaian Keterampilan</a></li>
                    <li><a href="<?php echo base_url('guru/nilai/sikap')?>">Penilaian Sikap</a></li>
                </ul>
            </li>   -->


              <?php endif; ?>
        </ul>
    </div>   
</nav>  
<!-- /. NAV SIDE  -->
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12"><h2><?php echo $title ?></h2></div>
        </div>
         <!-- /. ROW  -->
         <hr />
       
        <div class="row">
            <div class="col-md-12">
            <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <!-- <div class="panel-heading"><?php echo $title ?></div> -->
                    <div class="panel-body">
                        <div class="table-responsive">
