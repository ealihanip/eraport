<?php
$id_guru 	= $this->session->userdata('id_guru');
$level 		= $this->session->userdata('akses_level');
$user_aktif = $this->model_guru->detail($id_guru);
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('guru/laporan/tambah'));
?>

<div class="col-md-4">
	<div class="form-group">
		<label>Tahun Ajaran</label>
		<select class="form-control" name="id_ajaran">
			<?php foreach ($ajaran as $ajaran) { ?>
				<option value="<?php echo $ajaran->id_ajaran ?>">
					<?php echo $ajaran->tahun?>
				</option>
				<?php } ?>
		</select>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/laporan/tambah?")?>kelas="+this.options[this.selectedIndex].value'>
			<option value="">== Pilih Kelas ==</option>
			<?php foreach ($kelas as $kelas ) { ?>
				<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
					<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
				</option>
				<?php } ?>
		</select>
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
		<label>siswa</label>
		<select class="form-control js-example-basic-single" name="id_siswa">
			<option value="">== Pilih Siswa ==</option>
			<?php foreach ($siswa as $siswa ) { ?>
				<option value="<?php echo $siswa->id_siswa ?>">
					<?php echo $siswa->nama_siswa ?>
				</option>
				<?php } ?>
		</select>
	</div>
</div>

	
<div class="col-md-12">
	<div class="form-group">
		<label>Deskripsi</label>
		<textarea name="catatan" class="form-control editor"></textarea>
	</div>
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('guru/laporan')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>