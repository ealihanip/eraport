<p><a href="<?php echo base_url('guru/laporan/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="10%">Nama Siswa</th>
            <th width="85%">Deskripsi</th>
            <th width="3%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($catatan as $catatan)
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i;?></td>
        <td><?php echo $catatan->nama_siswa?></td>
        <td><?php echo $catatan->catatan?></td>
        <td>
            <a href="<?php echo base_url('guru/laporan/edit/'.$catatan->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>


<!--  <form>			
 	<input type="hidden" name="ajaran_id" value="1" />
	<input type="hidden" name="rombel_id" value="20" />
	<div class="table-responsive no-padding">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="20%">Nama Siswa</th>
					<th width="80%">Deskripsi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="hidden" name="siswa_id[]" value="1"/>
					</td>
					<td>
						<textarea name="uraian_deskripsi[]" class="editor">
						</textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>  -->