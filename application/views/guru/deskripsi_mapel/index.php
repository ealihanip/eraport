

<?php


//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
	echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}



?>


<div class='col-lg-12'>


    <form method='post' action='<?php echo base_url('guru/deskripsi_mapel/store?kelas='.$id_kelas.'&mapel='.$id_mapel)?>'>
    

	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/deskripsi_mapel/index?")?>kelas="+this.options[this.selectedIndex].value'>
			<option value="">== Pilih Kelas ==</option>
			<?php foreach ($kelas as $kelas ) { ?>
				<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
					<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
        <label>Mata Pelajaran</label>
        <select class="form-control" name="id_mapel" onchange='window.location="<?php echo base_url("guru/deskripsi_mapel/index?&kelas=".$id_kelas."&")?>mapel="+this.options[this.selectedIndex].value'>
            
			<option>Pilih Mata Pelajaran</option>
			<?php foreach ($mapel as $mapel ) { ?>
				<option value="<?php echo $mapel->id_mapel ?>"<?php if ($mapel->id_mapel==$id_mapel) {
                    echo "selected";} ?>>
                    <?php echo $mapel->nama_mapel ?>
                </option>
                <?php } ?>
        </select>
    </div>

    <table class='table table-striped'>
		<thead>
			<tr>
				<th class='text-center'>
					Nama Siswa
				</th>
				<th class='text-center' >
					Deskripsi Mata Pelajaran
				</th>
			</tr>

			
			
		</thead>

		<tbody>

			<?php foreach($siswa as $data_siswa){?>
				<tr>
					<td>
						<?php echo $data_siswa->nama_siswa?>
					</td>

					<td>

						<?php if(!empty($deskripsi_mapel)){?>

							<?php foreach($deskripsi_mapel as $data_deskripsi_mapel){?>

								<?php if($data_deskripsi_mapel->id_siswa==$data_siswa->id_siswa){?>
									
									<input type='hidden' name='id_deskripsi_mapel[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_deskripsi_mapel->id?>'>
									<div class="form-group">
										<label>Pengetahuan</label>
										<textarea name="pengetahuan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="Pengetahuan"><?php echo $data_deskripsi_mapel->pengetahuan?></textarea>
									</div>

									<div class="form-group">
										<label>Keterampilan</label>
										<textarea name="keterampilan[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="Keterampilan"><?php echo $data_deskripsi_mapel->keterampilan?></textarea>
									</div>
								<?php }?>

							<?php }?>
						<?php }else{?>
							<div class="form-group">
								<label>Pengetahuan</label>
								<textarea name="pengetahuan[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Pengetahuan" rows='3' ></textarea>
								<label>Keterampilan</label>
								<textarea name="keterampilan[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Keterampilan" rows='3' ></textarea>

							</div>
						<?php }?>
					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>

    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
    </div>

    
    </form>

</div>

