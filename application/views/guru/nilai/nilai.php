


<?php

//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
if($id_sn!='uas' && $id_sn!='uts'){

	if(!empty($nilai)){

		echo form_open(base_url('guru/nilai/update?kelas='.$id_kelas.'&mapel='.$id_mapel.'&sn='.$id_sn));

	}else{

		echo form_open(base_url('guru/nilai/store?kelas='.$id_kelas.'&mapel='.$id_mapel.'&sn='.$id_sn));
	}
	
}else{

	if(!empty($nilai)){

		echo form_open(base_url('guru/nilai/update_ujian?kelas='.$id_kelas.'&mapel='.$id_mapel.'&sn='.$id_sn));

	}else{

		echo form_open(base_url('guru/nilai/store_ujian?kelas='.$id_kelas.'&mapel='.$id_mapel.'&sn='.$id_sn));
	}

}

?>

<div class='col-lg-12'>
	<div class='row'>
		<div class="col-md-8">
			
			<div class="form-group">
				<label>Kelas</label>
				<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/nilai/".$key."?")?>kelas="+this.options[this.selectedIndex].value'>
					<option value="">== Pilih Kelas ==</option>
					<?php foreach ($mapel as $kelas ) { ?>
						<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
							<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
						</option>
						<?php } ?>
				</select>
			</div>

			<div class="form-group">
				<label>Mata Pelajaran</label>
				<select class="form-control js-example-basic-single" name="id_mapel" onchange='window.location="<?php echo base_url("guru/nilai/".$key."?"."kelas=".$id_kelas)?>&mapel="+this.options[this.selectedIndex].value'>
					<option value="">== Pilih Mata Pelajaran ==</option>

					<?php if(isset($id_kelas) && !empty($id_kelas)){?>
						<?php foreach ($mapel as $mapel ) { ?>
							<option value="<?php echo $mapel->id_mapel ?>" <?php if($kelas->id_mapel==$id_mapel){echo "selected";}?>>
								<?php echo $mapel->nama_mapel ?>
							</option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>


			<div class="form-group">
				<label>Penilaian</label>
				<select class="form-control" name="id_sn" onchange='window.location="<?php echo base_url("guru/nilai/".$key."?"."kelas=".$id_kelas."&mapel=".$id_mapel)?>&sn="+this.options[this.selectedIndex].value'>
					<option value="">==Pilih Penilaian==</option>
					<?php foreach ($sn as $data_sn) { ?>
					<option value="<?php echo $data_sn->id?>" <?php if($data_sn->id==$id_sn){echo "selected";}?>>
						<?php echo $data_sn->kode_kd ?> <?php echo $data_sn->nama_penilaian ?> 
					</option>

				<?php } ?>

				<?php if($key!='keterampilan'){?>
					
					<?php if(!empty($sn)){?>
					<option value="uts" <?php if($id_sn=='uts'){echo "selected";}?>>UTS</option>
					<option value="uas" <?php if($id_sn=='uas'){echo "selected";}?>>UAS</option>
					<?php } ?>
				<?php }else{ ?>

					<option value="uts" <?php if($id_sn=='uts'){echo "selected";}?>>UTS</option>

				<?php }?>
				</select>
				
			</div>

			
		</div>
	</div>
</div>

<div class="col-lg-12">
	<h4>Table Nilai</h4>
</div>

<div class="col-lg-12 ">
	<div class='card'>

		<div class='card-body'>
				
		<table class='table table-bordered'>
				<thead>
					<tr>
						<th rowspan='2' class='text-center'>
							Nama Siswa
						</th>
						<th class='text-center' >
							Kompetensi Dasar
						</th>
					</tr>

					
					<tr>
					
						<th>
							<?php if(!empty($id_sn)){?>

								<?php foreach($sn as $data_sn){ ?>
									<?php if($data_sn->id == $id_sn){ ?>
										<?php echo $data_sn->kode_kd.' '.$data_sn->nama_penilaian?>
									<?php }elseif($id_sn=='uas'){?>
										UAS
									<?php break;}elseif($id_sn=='uts'){?>
										UTS
									<?php break;} ?>	
								<?php }?>
							<?php }else{?>
								Kode Kd
							<?php }?>
						</th>
					</tr>
				</thead>

				<tbody>

					<?php foreach($siswa as $data_siswa){?>
						<tr>
							<td>
								<?php echo $data_siswa->nama_siswa?>
							</td>

							<td>

								<?php if(!empty($nilai)){?>

									<?php foreach($nilai as $data_nilai){?>

										<?php if($data_nilai->id_siswa==$data_siswa->id_siswa){?>

											<input type='hidden' name='id_nilai[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_nilai->id_nilai?>'>
											<div class="form-group">
												<input type="text" name="nilai[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Nilai" value='<?php echo $data_nilai->nilai?>'>
											</div>
										<?php }?>

									<?php }?>
								<?php }else{?>
									<div class="form-group">
										<input type="text" name="nilai[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Nilai" value=''>
									</div>
								<?php }?>
							</td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>

	</div>
</div>


<div class="col-lg-12 text-center">
	<input type='hidden' name='aspek' value='<?php echo $key?>'>
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan" <?php if(empty($id_sn)){echo 'disabled';}?>>
    </div>
</div>




<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>