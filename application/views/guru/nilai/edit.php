<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('guru/nilai/update_'.$key.'?kelas='.$id_kelas.'&mapel='.$id_mapel.'&kd='.$id_kd));?>
<div class='col-lg-12'>
	<div class='row'>
		<div class="col-md-8">

			<div class="form-group">
				<label>Tahun Ajaran</label>
				<select class="form-control" name="id_ajaran">
					<?php foreach ($ajaran as $ajaran ) { ?>
						<option value="<?php echo $ajaran->id_ajaran ?>"  <?php if($ajaran->id_ajaran==$id_ajaran){echo "selected";}?>>
							<?php echo $ajaran->tahun ?>
						</option>
						<?php } ?>
				</select>
			</div>

			
			<div class="form-group">
				<label>Kelas</label>
				<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/nilai/add_".$key."?")?>kelas="+this.options[this.selectedIndex].value'>
					<option value="">== Pilih Kelas ==</option>
					<?php foreach ($mapel as $kelas ) { ?>
						<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
							<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
						</option>
						<?php } ?>
				</select>
			</div>

			<div class="form-group">
				<label>Mata Pelajaran</label>
				<select class="form-control js-example-basic-single" name="id_mapel" onchange='window.location="<?php echo base_url("guru/nilai/add_".$key."?kelas=".$id_kelas)?>&mapel="+this.options[this.selectedIndex].value'>
					<option value="">== Pilih Mata Pelajaran ==</option>

					<?php if(isset($id_kelas) && !empty($id_kelas)){?>
						<?php foreach ($mapel as $mapel ) { ?>
							<option value="<?php echo $mapel->id_mapel ?>" <?php if($kelas->id_mapel==$id_mapel){echo "selected";}?>>
								<?php echo $mapel->nama_mapel ?>
							</option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>


			<div class="form-group">
				<label>Kode KD</label>
				<select class="form-control" name="id_kd" onchange='window.location="<?php echo base_url("guru/nilai/add_".$key."?kelas=".$id_kelas."&mapel=".$id_mapel)?>&kd="+this.options[this.selectedIndex].value'>
					<option value="">==Pilih Kode Kompetensi Dasar==</option>
					<?php foreach ($kd as $data_kd) { ?>
					<option value="<?php echo $data_kd->id_kd?>" <?php if($data_kd->id_kd==$id_kd){echo "selected";}?>>
						<?php echo $data_kd->kode_kd ?> <?php echo $data_kd->isi_kd ?> 
					</option>
				<?php } ?>
				</select>
			</div>

			
		</div>
	</div>
</div>

<div class="col-lg-12">
	<h4>Table Nilai</h4>
</div>

<div class="col-lg-12 ">
	<div class='card'>

		<div class='card-body'>
				
			<table class='table table-bordered'>
				<thead>
					<tr>
						<th rowspan='2' class='text-center'>
							Nama Siswa
						</th>
						<th class='text-center' >
							Kompetensi Dasar
						</th>
					</tr>

					
					<tr>
					
						<th>
							<?php if(!empty($id_kd)){?>

								<?php foreach($kd as $data_kd){ ?>
									<?php if($data_kd->id_kd == $id_kd){ ?>
										<?php echo $data_kd->kode_kd?>
									<?php }?>

								
								<?php }?>
							<?php }else{?>
								Kode Kd
							<?php }?>
						</th>
					</tr>
				</thead>

				<tbody>

					<?php foreach($siswa as $data_siswa){?>
						<tr>
							<td>
								<?php echo $data_siswa->nama_siswa?>
							</td>

							<td>
							<?php foreach($nilai as $data_nilai){?>
								<?php if($data_nilai->id_siswa==$data_siswa->id_siswa){?>

									<input type='hidden' name='id_nilai[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_nilai->id_nilai?>'>
									<div class="form-group">
										<input type="text" name="nilai[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Nilai" value='<?php echo $data_nilai->nilai?>'>
									</div>
								<?php }?>
							<?php }?>
							</td>
						</tr>
					<?php }?>
				</tbody>
			</table>
		</div>

	</div>
</div>


<div class="col-lg-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('guru/nilai/pengetahuan')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>




<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>