<p><a href="<?php echo base_url('guru/nilai/add_'.$key)?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="5%">Tahun Ajaran</th>
            <th width="5%">Kelas</th>
            <th width="10%">Nama Mapel</th>
            <th width="2%">Kode KD</th>
            <th width="2%">Penilaian</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
         <?php 
            $i=1;
            foreach ($nilai as $nilai)
          { 
        ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $nilai->tahun?></td>
        <td><?php echo $nilai->nama_kelas?> - <?php echo $nilai->nama_jurusan?></td>
        <td><?php echo $nilai->nama_mapel?></td>
        <td><?php echo $nilai->kode_kd?></td>
        <td><?php echo $nilai->nama_penilaian?></td>
        <td>
            <a href="<?php echo base_url('guru/nilai/edit_pengetahuan?kelas='.$nilai->id_kelas.'&mapel='.$nilai->id_mapel.'&sn='.$nilai->id.'&ajaran='.$nilai->id_ajaran) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>