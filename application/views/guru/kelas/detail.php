<button class="btn btn-success btn-xs" data-toggle="modal" data-target="#Detail<?php echo $kelas->id_siswa ?>"><i class="fa fa-eye">Detail</i></button>
<div class="modal fade" id="Detail<?php echo $kelas->id_siswa ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Data siswa: <strong><?php echo $kelas->nama_siswa ?></strong></h4>
            </div>
            <div class="modal-body">
               
<!-- isis -->
<form>
    <div class="col-xs-3">
        <label>Rombel</label>
        <div class="well well-sm"><?php echo $kelas->nama_jurusan ?></div>

        <label>Tahun Ajaran</label>
        <div class="well well-sm"><?php echo $kelas->tahun ?></div>

        <label>Nama</label>
        <div class="well well-sm no-shadow" ><?php echo $kelas->nama_siswa ?></div>

        <label>No Induk</label>
        <div class="well well-sm no-shadow" ><?php echo $kelas->no_induk ?></div>

        <label>NISN</label>
        <div class="well well-sm no-shadow" ><?php echo $kelas->nisn ?></div>

        <label>Jenis Kelamin</label>
        <div class="well well-sm no-shadow"><?php echo $kelas->jk?></div>

        <label>Tempat Lahir</label>
        <div class="well well-sm no-shadow"><?php echo $kelas->tempat_lahir ?></div>

        <label>Tanggal Lahir</label>
        <div class="well well-sm"><?php echo date('d-m-Y', strtotime($kelas->tgl_lahir)) ?></div>

        <label>Agama</label>
        <div class="well well-sm"><?php echo $kelas->agama ?></div>

        <label>Status Anak</label>
        <div class="well well-sm"><?php echo $kelas->status ?></div>

        <label>Anak Ke</label>
        <div class="well well-sm"><?php echo $kelas->anak_ke ?></div>

        <label>Sekolah Asal</label>
        <div class="well well-sm"><?php echo $kelas->sekolah_asal ?></div>
    </div>

    <div class="col-xs-3">
        <label>Alamat</label>
        <div class="alert alert-info" style="margin-right: 10px;"><?php echo $kelas->alamat ?></div>

        <label>RT</label>
        <div class="well well-sm"><?php echo $kelas->rt ?></div>

        <label>RW</label>
        <div class="well well-sm"<?php echo $kelas->rw ?>">
        </div>

        <label>Desa / Kelurahan</label>
        <div class="well well-sm"><?php echo $kelas->desa_kelurahan ?></div>

        <label>Kecamatan</label>
        <div class="well well-sm"><?php echo $kelas->kecamatan ?></div>

        <label>Kabupaten</label>
        <div class="well well-sm"><?php echo $kelas->kabupaten ?></div>

        <label>Kode Pos</label>
        <div class="well well-sm"><?php echo $kelas->kode_pos ?></div>

        <label>Diterima Dikelas</label>
        <div class="well well-sm"><?php echo $kelas->diterima_kelas?></div>

        <label>Diterima Pada Tanggal</label>
        <div class="well well-sm"><?php echo $kelas->diterima ?></div>
    </div>

    <div class="col-xs-3">
        <label>Nama Ayah</label>
        <div class="well well-sm" ><?php echo $kelas->nama_ayah ?></div>

        <label>Pekerjaan Ayah</label>
        <div class="well well-sm"><?php echo $kelas->kerja_ayah ?></div>

        <label>No Telp Ayah</label>
        <div class="well well-sm"><?php echo $kelas->telp_ayah ?></div>

         <label>Nama Ibu</label>
        <div class="well well-sm"><?php echo $kelas->nama_ibu ?></div>

        <label>Pekerjaan Ibu</label>
        <div class="well well-sm"><?php echo $kelas->kerja_ibu ?></div>

        <label>No Telp Ibu</label>
        <div class="well well-sm"><?php echo $kelas->telp_ibu ?></div>

        <label>Nama Wali</label>
        <div class="well well-sm"><?php echo $kelas->nama_wali ?></div>

        <label>Pekerjaan Wali</label>
        <div class="well well-sm"><?php echo $kelas->kerja_wali ?></div>

        <label>Alamat</label>
        <div class="alert alert-info"><?php echo $kelas->alamat_wali ?></div>
    </div>

    <div class="col-md-3">
        <label>No Telp Wali</label>
        <div class="well well-sm"><?php echo $kelas->telp_wali ?></div>

        <label>No HP</label>
        <div class="well well-sm"><?php echo $kelas->no_hp ?></div>

        <label>Email</label>
        <div class="well well-sm"><?php echo $kelas->email ?></div>

        <label>Status Siswa</label>
        <div class="well well-sm"><?php echo $kelas->status_siswa?></div>

    </div>
</form>

<!-- end isi -->

<div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>




        