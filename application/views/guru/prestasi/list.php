<p><a href="<?php echo base_url('guru/prestasi/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="10%">Tahun</th>
            <th width="10%">Kelas</th>
            <th width="10%">Nama Siswa</th>
            <th width="10%">Nama Prestasi</th>
            <th width="45%">Keterangan</th>
            <th width="3%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($prestasi as $prestasi)
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i;?></td>
        <td><?php echo $prestasi->tahun ?></td>
        <td><?php echo $prestasi->nama_jurusan?></td>
        <td><?php echo $prestasi->nama_siswa ?></td>
        <td><?php echo $prestasi->nama_prestasi ?></td>
        <td><?php echo $prestasi->keterangan ?></td>
        <td>
            <a href="<?php echo base_url('guru/prestasi/edit/'.$prestasi->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>