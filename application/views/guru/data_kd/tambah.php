<?php
$id_guru 	= $this->session->userdata('id_guru');
$level 		= $this->session->userdata('akses_level');
$user_aktif = $this->model_guru->detail($id_guru);
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('guru/data_kd/tambah'));
?>

<div class="col-md-8">

	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="id_kelas" onchange='window.location="<?php echo base_url("guru/data_kd/tambah?")?>kelas="+this.options[this.selectedIndex].value'>
			<option value="">== Pilih Kelas ==</option>
			<?php foreach ($mapel as $kelas ) { ?>
				<option value="<?php echo $kelas->id_kelas ?>" <?php if($kelas->id_kelas==$id_kelas){echo "selected";}?>>
					<?php echo $kelas->nama_kelas ?> - <?php echo $kelas->nama_jurusan ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Mata Pelajaran</label>
		<select class="form-control js-example-basic-single" name="id_mapel">
			<option value="">== Pilih Mata Pelajaran ==</option>

			<?php if(isset($id_kelas) && !empty($id_kelas)){?>
			<?php foreach ($mapel as $mapel ) { ?>
				<option value="<?php echo $mapel->id_mapel ?>">
					<?php echo $mapel->nama_mapel ?>
				</option>
				<?php } ?>
			<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Aspek Penilaian</label>
		<select class="form-control" name="id_aspek">
			<?php foreach ($aspek as $aspek) { ?>
				<option value="<?php echo $aspek->id_aspek ?>">
					<?php echo $aspek->nama_aspek?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Kode KD</label>
		<input type="text" name="kode_kd" class="form-control">
	</div>

	<div class="form-group">
		<label>Isi KD</label>
		<textarea name="isi_kd" class="form-control"></textarea>
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('guru/data_kd')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>