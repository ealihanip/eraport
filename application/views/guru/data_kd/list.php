<p><a href="<?php echo base_url('guru/data_kd/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#<!-- <?php echo  $this->session->userdata('guru_mapel'); ?> --></th>
            <th width="10%">Mata Pelajaran</th>
            <th width="2%">Kode</th>
            <th width="5%">Kelas</th>
            <th width="3%">Aspek</th>
            <th width="15%">Isi Kompetensi</th>
            <th width="3%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($kd as $kd)
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i;?></td>
        <td><?php echo $kd->nama_mapel?></td>
        <td><?php echo $kd->kode_kd?></td>
        <td><?php echo $kd->nama_kelas?> - <?php echo $kd->nama_jurusan?></td>
        <td><?php echo $kd->nama_aspek?></td>
        <td><?php echo $kd->isi_kd?></td>
        <td>
            <a href="<?php echo base_url('guru/data_kd/edit/'.$kd->id_kd) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>