

<?php


//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
	echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}



?>


<div class='col-lg-12'>


    <form method='post' action='<?php echo base_url('guru/sikap/store')?>'>
    

    <table class='table table-striped'>
		<thead>
			<tr>
				<th class='text-center'>
					Nama Siswa
				</th>
				<th class='text-center' >
					Catatan Wali Kelas
				</th>
			</tr>

			
			
		</thead>

		<tbody>

			<?php foreach($siswa as $data_siswa){?>
				<tr>
					<td>
						<?php echo $data_siswa->nama_siswa?>
					</td>

					<td>

						<?php if(!empty($sikap)){?>

							<?php foreach($sikap as $data_sikap){?>

								<?php if($data_sikap->id_siswa==$data_siswa->id_siswa){?>

									<input type='hidden' name='id_sikap[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_sikap->id?>'>
									<div class="form-group">
										<textarea name="sikap[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="sikap"><?php echo $data_sikap->sikap?></textarea>
									</div>
								<?php }?>

							<?php }?>
						<?php }else{?>
							<div class="form-group">
								<textarea name="sikap[<?php echo $data_siswa->id_siswa?>]" class="form-control" placeholder="Catatan" rows='3' ></textarea>
							</div>
						<?php }?>
					</td>
				</tr>
			<?php }?>
		</tbody>
	</table>

    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
    </div>

    
    </form>

</div>

