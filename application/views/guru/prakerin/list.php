<p><a href="<?php echo base_url('guru/prakerin/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="10%">Tahun</th>
            <th width="10%">Kelas</th>
            <th width="10%">Nama Siswa</th>
            <th width="10%">Nama Instansi</th>
            <th width="15%">Alamat</th>
            <th width="5%">Lama(bulan)</th>
            <th width="45%">Keterangan</th>
            <th width="3%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($pkl as $pkl)
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i;?></td>
        <td><?php echo $pkl->tahun ?></td>
        <td><?php echo $pkl->nama_jurusan?></td>
        <td><?php echo $pkl->nama_siswa ?></td>
        <td><?php echo $pkl->nama_instansi ?></td>
        <td><?php echo $pkl->alamat ?></td>
        <td><?php echo $pkl->lama ?></td>
        <td><?php echo $pkl->keterangan ?></td>
        <td>
            <a href="<?php echo base_url('guru/prakerin/edit/'.$pkl->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>