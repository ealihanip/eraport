
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama</th>
            <th>NIP</th>
            <th>Jenis Kelamin</th>
            <th width="10%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        
    <tr class="odd gradeX">
        <?php 
            $i=1;
            foreach ($guru as $guru) 
          { 
        ?>
        <td><?php echo $i; ?></td>
        <td><?php echo $guru->nama_guru ?></td>
        <td><?php echo $guru->nip ?></td>
        <td><?php echo $guru->jk ?></td>
        <td><?php include('detail.php'); ?></td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>