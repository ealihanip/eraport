<button class="btn btn-success btn-xs" data-toggle="modal" data-target="#Detail<?php echo $guru->id_guru ?>"><i class="fa fa-eye"></i></button>
<div class="modal fade" id="Detail<?php echo $guru->id_guru ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Data guru: <strong><?php echo $guru->nama_guru ?></strong></h4>
            </div>
            <div class="modal-body">
               
<!-- isis -->
<form>
    <div class="col-xs-4">
        <label>Nama Guru</label>
        <div class="well well-sm"><?php echo $guru->nama_guru ?></div>

        <label>NIP</label>
        <div class="well well-sm no-shadow" ><?php echo $guru->nip ?></div>

        <label>Jenis Kelamin</label>
        <div class="well well-sm no-shadow"><?php echo $guru->jk?></div>

        <label>Tempat Lahir</label>
        <div class="well well-sm no-shadow"><?php echo $guru->tempat_lahir ?></div>

        <label>Tanggal Lahir</label>
        <div class="well well-sm"><?php echo date('d-m-Y',strtotime($guru->tgl_lahir)) ?></div>

        <label>Agama</label>
        <div class="well well-sm"><?php echo $guru->agama ?></div>
    </div>

    <div class="col-xs-4">
        <label>Alamat</label>
        <div class="alert alert-info" style="margin-right: 10px;"><?php echo $guru->alamat ?></div>

        <label>RT</label>
        <div class="well well-sm"><?php echo $guru->rt ?></div>

        <label>RW</label>
        <div class="well well-sm<?php echo $guru->rw ?>">
        </div>

        <label>Desa / Kelurahan</label>
        <div class="well well-sm"><?php echo $guru->desa_kelurahan ?></div>

        <label>Kecamatan</label>
        <div class="well well-sm"><?php echo $guru->kecamatan ?></div>

        <label>Kabupaten</label>
        <div class="well well-sm"><?php echo $guru->kabupaten ?></div>

        <label>Kode Pos</label>
        <div class="well well-sm"><?php echo $guru->kode_pos ?></div>
    </div>

    <div class="col-md-4">
        <label>No HP</label>
        <div class="well well-sm"><?php echo $guru->no_hp ?></div>

        <label>Email</label>
        <div class="well well-sm"><?php echo $guru->email ?></div>

        <!-- <label>Status</label>
        <div class="well well-sm"><?php echo $guru->aktif?></div> -->

       <!--  <label>Username</label>
        <div class="well well-sm"><?php echo $guru->username ?></div>

        <label>Akses Level</label>
        <div class="well well-sm"><?php echo $guru->akses_level?></div> -->

        <!-- <label>Foto</label>
         <div class="wel well-sm"><?php if ($guru->foto != "") { ?><img src="<?php echo base_url('assets/upload/image/'.$guru->foto) ?>" class="img img-thumbnail img-responsive" width="100">
         <?php } ?>
    </div> -->
    </div>
</form>

<!-- end isi -->

<div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>




        