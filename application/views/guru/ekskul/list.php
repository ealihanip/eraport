<p><a href="<?php echo base_url('guru/nilai_ekskul/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>
<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="5%">#</th>
            <th width="15%">Nama Siswa</th>
            <th width="5">Nama Ekskul</th>
            <th width="5%">Nilai Ekskul</th>
            <th width="65">Keterangan</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($ne as $ne)
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i;?></td>
        <td><?php echo $ne->nama_siswa?></td>
        <td><?php echo $ne->nama_ekskul?></td>
        <td><?php echo $ne->nilai?></td>
        <td><?php echo $ne->keterangan?></td>
        <td>
            <a href="<?php echo base_url('guru/nilai_ekskul/edit/'.$ne->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>


<!--  <form>			
 	<input type="hidden" name="ajaran_id" value="1" />
	<input type="hidden" name="rombel_id" value="20" />
	<div class="table-responsive no-padding">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th width="20%">Nama Siswa</th>
					<th width="80%">Deskripsi</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type="hidden" name="siswa_id[]" value="1"/>
					</td>
					<td>
						<textarea name="uraian_deskripsi[]" class="editor">
						</textarea>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="box-footer">
		<button type="submit" class="btn btn-success">Simpan</button>
	</div>
</form>  -->