

<?php


//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}


//notifikasi kalau ada input error
	echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}



?>


<div class='col-lg-12'>


    <form method='post' action='<?php echo base_url('guru/nilai_ekskul/store?id_ekskul='.$id_ekskul)?>'>
    
    <div class="form-group">
        <label>Mata Ekskul</label>
        <select class="form-control" name="id_dataekskul" onchange='window.location="<?php echo base_url("guru/nilai_ekskul/index?")?>id_ekskul="+this.options[this.selectedIndex].value'>
            
			<option>Pilih Ekskul</option>
			<?php foreach ($dataekskul as $dataekskul ) { ?>
				<option value="<?php echo $dataekskul->id ?>"<?php if ($dataekskul->id==$id_ekskul) {
                    echo "selected";} ?>>
                    <?php echo $dataekskul->nama_ekskul ?>
                </option>
                <?php } ?>
        </select>
    </div>

    <table class='table table-striped'>
		<thead>
			<tr>
				<th class='text-center'>
					Nama Siswa
				</th>
				<th class='text-center' >
					Predikat
				</th>

				<th class='text-center' >
					Deskripsi
				</th>
			</tr>

			
			
		</thead>

		<tbody>

			<?php foreach($siswa as $data_siswa){?>
				<tr>
					<td>
						<?php echo $data_siswa->nama_siswa?>
					</td>

					

						<?php if(!empty($ekskul)){?>

							<?php foreach($ekskul as $data_ekskul){?>

								<?php if($data_ekskul->id_siswa==$data_siswa->id_siswa){?>

									<input type='hidden' name='id_ekskul[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_ekskul->id?>'>
									
									<td>
									<div class="form-group">
										
										<select class="form-control" name="predikat[<?php echo $data_siswa->id_siswa?>]">
											
											<option value=''>Pilih Predikat</option>
											<option value='C' <?php if($data_ekskul->predikat=='C'){echo 'selected';}?>>Cukup</option>
											<option value='B' <?php if($data_ekskul->predikat=='B'){echo 'selected';}?>>Baik</option>
											<option value='A' <?php if($data_ekskul->predikat=='A'){echo 'selected';}?>>Sangat Baik</option>
											

										</select>
									</div>
									</td>
									
									<td>
									<div class="form-group">
										<textarea name="deskripsi[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="ekskul"><?php echo $data_ekskul->deskripsi?></textarea>
									</div>

									</td>
								<?php }?>

							<?php }?>
						<?php }else{?>

						
							<div class="form-group">
							<td>
									<div class="form-group">
										
										<select class="form-control" name="predikat[<?php echo $data_siswa->id_siswa?>]">
											
											<option value=''>Pilih Predikat</option>
											<option value='C'>Cukup</option>
											<option value='B'>Baik</option>
											<option value='A'>Sangat Baik</option>
											

										</select>
									</div>
									</td>
									
									<td>
										<div class="form-group">
											<textarea name="deskripsi[<?php echo $data_siswa->id_siswa?>]" class="form-control" rows='3' placeholder="ekskul"></textarea>
										</div>

									</td>
							</div>

						
						<?php }?>
					
				</tr>
			<?php }?>
		</tbody>
	</table>

    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
    </div>

    
    </form>

</div>

