


<html>

    <head>
        <title>Laporan</title>

        <style>
            @page {
                margin-top: 2.54cm;
                margin-bottom: 2.54cm;
                margin-left: 2.54cm;
                margin-right: 2.54cm;
            }
            .font-16{

                font-size:16pt;
            }

            .font-12{

                font-size:12pt;
            }

            .font-14{

                font-size:14pt;
            }

            .bold{
                font-weight: bold;
            }

            .bold{
                font-weight: bold;
            }

            .center{
                text-align: center;
            }
            .right{
                text-align: right;
            }
            .left{
                text-align: left;
            }

            .underline{
                text-decoration: underline;
            }

            .justify{
                text-align: justify;
            }

            body{
                font-family: "Times New Roman", Times, serif;
                line-height: 12pt;
            }

            br {
                display: block;
                margin: 1pt;
                line-height: 0;
            }
            
            .table {
                border-collapse: collapse;
                width:100%;
                margin:0 auto;
            }

            .table th {
                text-align:center;
                border: 1px solid black;
            }

            .table td {
                border: 1px solid black;
                font-size:12pt;
                
            }
            .ttd{
                float:right;
                width:50%; 
                
            }
        </style>
    </head>


    <body>


        <p class='font-16 bold center'>Data Siswa Kelas <?php echo $kelas[0]->nama_kelas;?> <?php echo $kelas[0]->nama_jurusan;?></p>
        <br>
        <br>

        <table class='table'>
            <tr>

                <th>
                    No
                </th>
                <th>
                    No Induk
                </th>

                <th>
                    Nama
                </th>

                <th>
                    Jenis Kelamin
                </th>

                <th>
                    Tempat/Tanggal Lahir
                </th>


            </tr>


            <?php $no=1;foreach($siswa as $siswa){?>
            <tr>
                
                <td>
                    <?php echo $no?>
                </td>
                <td>
                    <?php echo $siswa->no_induk?>
                </td>

                <td>
                    <?php echo $siswa->nama_siswa?>
                </td>

                <td>
                    <?php echo $siswa->jk?>
                </td>

                <td>
                    <?php echo $siswa->tempat_lahir?>, <?php echo $siswa->tgl_lahir?>
                </td>


            </tr>
            
            <?php $no++;}?>
        </table>
            
        

        

        
    </body>

</html>