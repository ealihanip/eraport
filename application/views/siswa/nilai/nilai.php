

<div class="panel panel-default">
           
	<div class="panel-body">
	
		

		<div class='col-lg-12'>
			<div class='row'>
				<div class="col-md-8">

					<div class="form-group">
						<label>Tahun Ajaran</label>
						<select class="form-control" name="id_ajaran" onchange='window.location="<?php echo base_url("siswa/nilai/".$key."?")?>ajaran="+this.options[this.selectedIndex].value'>
						<option value="">== Pilih Tahun Ajaran ==</option>
							<?php foreach ($ajaran as $ajaran ) { ?>
								<option value="<?php echo $ajaran->id_ajaran ?>" <?php if($ajaran->id_ajaran==$id_ajaran){echo "selected";}?>>
									<?php echo $ajaran->tahun ?>
								</option>
								<?php } ?>
						</select>
					</div>


					<div class="form-group">
						<label>Mata Pelajaran</label>
						<select class="form-control js-example-basic-single" name="id_mapel" onchange='window.location="<?php echo base_url("siswa/nilai/".$key."?ajaran=".$id_ajaran."&"."kelas=".$id_kelas)?>&mapel="+this.options[this.selectedIndex].value'>
							<option value="">== Pilih Mata Pelajaran ==</option>

							<?php if(isset($id_kelas) && !empty($id_kelas)){?>
								<?php foreach ($mapel as $mapel ) { ?>
									<option value="<?php echo $mapel->id_mapel ?>" <?php if($mapel->id_mapel==$id_mapel){echo "selected";}?>>
										<?php echo $mapel->nama_mapel ?>
									</option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>


					<div class="form-group">
						<label>Penilaian</label>
						<select class="form-control" name="id_sn" onchange='window.location="<?php echo base_url("siswa/nilai/".$key."?ajaran=".$id_ajaran."&"."kelas=".$id_kelas."&mapel=".$id_mapel)?>&sn="+this.options[this.selectedIndex].value'>
							<option value="">==Pilih Penilaian==</option>
							<?php foreach ($sn as $data_sn) { ?>
							<option value="<?php echo $data_sn->id?>" <?php if($data_sn->id==$id_sn){echo "selected";}?>>
								<?php echo $data_sn->kode_kd ?> <?php echo $data_sn->nama_penilaian ?> 
							</option>

						<?php } ?>

						<?php if($key!='keterampilan'){?>
							
							<?php if(!empty($sn)){?>
							<option value="uts" <?php if($id_sn=='uts'){echo "selected";}?>>UTS</option>
							<option value="uas" <?php if($id_sn=='uas'){echo "selected";}?>>UAS</option>
							<?php } ?>
						<?php }else{ ?>

							<option value="uts" <?php if($id_sn=='uts'){echo "selected";}?>>UTS</option>

						<?php }?>
						</select>
						
					</div>

					
				</div>
			</div>
		</div>

		<div class="col-lg-12">
			<h4>Table Nilai</h4>
		</div>

		<div class="col-lg-12 ">
			<div class='card'>

				<div class='card-body'>
						
				
				</div>

			</div>
		</div>


	
		
	
	
	</div>
</div>


<div class="panel panel-default">
           
	<div class="panel-body">

		</div class='row'>
			
		<table class='table table-bordered'>
						<thead>
							<tr>
								<th rowspan='2' class='text-center'>
									Nama Siswa
								</th>
								<th class='text-center' >
									Kompetensi Dasar
								</th>
							</tr>

							
							<tr>
							
								<th>
									<?php if(!empty($id_sn)){?>

										<?php foreach($sn as $data_sn){ ?>
											<?php if($data_sn->id == $id_sn){ ?>
												<?php echo $data_sn->kode_kd.' '.$data_sn->nama_penilaian?>
											<?php }elseif($id_sn=='uas'){?>
												UAS
											<?php break;}elseif($id_sn=='uts'){?>
												UTS
											<?php break;} ?>	
										<?php }?>
									<?php }else{?>
										Kode Kd
									<?php }?>
								</th>
							</tr>
						</thead>

						<tbody>

							<?php foreach($siswa as $data_siswa){?>
								<tr>
									<td>
										<?php echo $data_siswa->nama_siswa?>
									</td>

									<td>

										<?php if(!empty($nilai)){?>

											<?php foreach($nilai as $data_nilai){?>

												<?php if($data_nilai->id_siswa==$data_siswa->id_siswa){?>

													<input type='hidden' name='id_nilai[<?php echo $data_siswa->id_siswa?>]' value='<?php echo $data_nilai->id_nilai?>'>
													<div class="form-group">
														<?php echo $data_nilai->nilai?>
													</div>
												<?php }?>

											<?php }?>
										<?php }else{?>
											<div class="form-group">
												Belum Ada Nilai
											</div>
										<?php }?>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>


						<br>
		<br>

		</div>

	
	</div>
</div>


