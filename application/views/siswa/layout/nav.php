<?php
//dapatkan id user saat login
$id_siswa        = $this->session->userdata('id_siswa');
$akses_level    = $this->session->userdata('akses_level');
$user_aktif     = $this->model_siswa->detail($id_siswa);
?>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">
            <li><a href="<?php echo base_url('siswa/dasbor')?>"><i class="fa fa-dashboard "></i> Dasbor</a></li>
            <li><a href="<?php echo base_url('siswa/nilai/pengetahuan')?>"><i class="fa fa-users"></i> Nilai Pengetahuan</a></li>
            <li><a href="<?php echo base_url('siswa/nilai/keterampilan')?>"><i class="fa fa-users"></i> Nilai Keterampilan</a></li>
            <li><a href="<?php echo base_url('siswa/nilai_eskul')?>"><i class="fa fa-users"></i> Nilai Eskul</a></li> 
            
           
        </ul>
    </div>   
</nav>  
<!-- /. NAV SIDE  -->
<div id="page-wrapper" >
          
