<button class="btn btn-success btn-xs" data-toggle="modal" data-target="#Detail<?php echo $siswa->id_siswa ?>"><i class="fa fa-eye"></i></button>
<div class="modal fade" id="Detail<?php echo $siswa->id_siswa ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Data siswa: <strong><?php echo $siswa->nama_siswa ?></strong></h4>
            </div>
            <div class="modal-body">
               
<!-- isis -->
<form>
    <div class="col-xs-4">
       <!--  <label>ID Rombel</label>
        <div class="well well-sm"><?php echo $siswa->id_rombel ?></div> -->

         <label>Nama</label>
        <div class="well well-sm no-shadow" ><?php echo $siswa->nama_siswa ?></div>

        <label>NISN</label>
        <div class="well well-sm no-shadow" ><?php echo $siswa->nisn ?></div>

        <label>Jenis Kelamin</label>
        <div class="well well-sm no-shadow"><?php echo $siswa->jk?></div>

        <label>Tempat Lahir</label>
        <div class="well well-sm no-shadow"><?php echo $siswa->tempat_lahir ?></div>

        <label>Tanggal Lahir</label>
        <div class="well well-sm"><?php echo $siswa->tgl_lahir ?></div>

        <label>Agama</label>
        <div class="well well-sm"><?php echo $siswa->agama ?></div>

        <label>Status Anak</label>
        <div class="well well-sm"><?php echo $siswa->status ?></div>

        <label>Status Anak</label>
        <div class="well well-sm"><?php echo $siswa->status ?></div>

        <label>Anak Ke</label>
        <div class="well well-sm"><?php echo $siswa->anak_ke ?></div>
    </div>

    <div class="col-xs-4">
        <label>Alamat</label>
        <div class="alert alert-info" style="margin-right: 10px;"><?php echo $siswa->alamat ?></div>

        <label>RT</label>
        <div class="well well-sm"><?php echo $siswa->rt ?></div>

        <label>RW</label>
        <div class="well well-sm<?php echo $siswa->rw ?>">
        </div>

        <label>Desa / Kelurahan</label>
        <div class="well well-sm"><?php echo $siswa->desa_kelurahan ?></div>

        <label>Kecamatan</label>
        <div class="well well-sm"><?php echo $siswa->kecamatan ?></div>

        <label>Kabupaten</label>
        <div class="well well-sm"><?php echo $siswa->kabupaten ?></div>

        <label>Kode Pos</label>
        <div class="well well-sm"><?php echo $siswa->kode_pos ?></div>
    </div>

    <div class="col-md-4">
        <label>No HP</label>
        <div class="well well-sm"><?php echo $siswa->no_hp ?></div>

        <label>Email</label>
        <div class="well well-sm"><?php echo $siswa->email ?></div>

        <label>Status</label>
        <div class="well well-sm"><?php echo $siswa->status_siswa?></div>

        <label>Username</label>
        <div class="well well-sm"><?php echo $siswa->username ?></div>

    </div>
</form>

<!-- end isi -->

<div class="clearfix"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>




        