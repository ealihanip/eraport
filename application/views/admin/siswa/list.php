<p><a href="<?php echo base_url('admin/siswa/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="15%">Nama</th>
            <th width="7%">No Induk</th>
            <th width="5%">Jenis Kelamin</th>
            <th width="5%">Kelas</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($siswa as $siswa) 
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $siswa->nama_siswa ?></td>
        <td><?php echo $siswa->no_induk ?></td>
        <td><?php echo $siswa->jk ?></td>
        <td><?php echo $siswa->nama_jurusan?></td>
        <td>
            <a href="<?php echo base_url('admin/siswa/edit/'.$siswa->id_siswa) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <?php include('detail.php'); ?>
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>