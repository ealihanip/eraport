<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/siswa/tambah'));
?>

<div class="col-md-3">
     <div class="form-group">
        <label>Kelas</label>
        <select class="form-control" name="id_kelas">
            <?php foreach ($kelas as $kelas) { ?>
                <option value="<?php echo $kelas->id_kelas ?>">
                    <?php echo $kelas->nama_jurusan ?>
                </option>
                <?php } ?>
        </select>
    </div>

     <div class="form-group">
        <label>Semester</label>
        <select class="form-control" name="id_semester">
            <?php foreach ($semester as $semester) { ?>
                <option value="<?php echo $semester->id_semester ?>">
                    <?php echo $semester->nama_semester ?>
                </option>
                <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama_siswa" class="form-control" placeholder="Nama" value="<?php echo set_value('nama_siswa') ?>">
    </div>

     <div class="form-group">
        <label>No Induk</label>
        <input type="text" name="no_induk" class="form-control" placeholder="No Induk" value="<?php echo set_value('no_induk') ?>">
    </div>

     <div class="form-group">
        <label>NISN</label>
        <input type="text" name="nisn" class="form-control" placeholder="NISN" value="<?php echo set_value('nisn') ?>">
    </div>

    <div class="form-group">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jk">
            <option value="Laki-laki">Laki-Laki</option>
            <option value="Prempuan">Perempuan</option>
        </select>
    </div>

    <div class="form-group">
        <label>Tempat Lahir</label>
        <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo set_value('tempat_lahir') ?>" >
    </div>

     <label>Tanggal Lahir</label>
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        <input type="date" name="tgl_lahir" class="form-control" placeholder="DD-MM-YYYY" value="<?php echo set_value('tgl_lahir') ?>" >
    </div>

    <div class="form-group">
        <label>Agama</label>
        <select class="form-control" name="agama">
            <option value="islam">Islam</option>
            <option value="kristen">Kristen</option>
            <option value="khatolik">Khatolik</option>
            <option value="hindu">Hindu</option>
            <option value="budha">Budha</option>
        </select>
    </div>

     <div class="form-group">
        <label>Status Anak</label>
        <select class="form-control" name="status">
            <option value="kandung">Kandung</option>
            <option value="angkat">Angkat</option>
        </select>
    </div>

     <div class="form-group">
        <label>Anak ke</label>
        <input type="number" name="anak_ke" class="form-control" placeholder="Anak Ke" value="<?php echo set_value('anak_ke') ?>">
    </div>

     <div class="form-group">
        <label>Sekolah Asal</label>
        <input type="text" name="sekolah_asal" class="form-control" placeholder="Sekolah Asal" value="<?php echo set_value('sekolah_asal') ?>">
    </div>
</div>

<div class="col-md-3">
    <div class="form-group">
        <label>Alamat</label>
       <textarea name="alamat" class="form-control" placeholder="Alamat"><?php echo set_value('alamat')?></textarea>
    </div>

    <div class="form-group">
        <label>RT</label>
        <input type="text" name="rt" class="form-control" placeholder="RT" value="<?php echo set_value('rt') ?>">
    </div>

    <div class="form-group ">
        <label>RW</label>
        <input type="text" name="rw" class="form-control" placeholder="RW" value="<?php echo set_value('rw') ?>">
    </div>

    <div class="form-group ">
        <label>Desa/Kelurahan</label>
        <input type="text" name="desa_kelurahan" class="form-control" placeholder="Desa / Kelurahan" value="<?php echo set_value('desa_kelurahan') ?>">
    </div>

    <div class="form-group ">
        <label>Kecamatan</label>
        <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" value="<?php echo set_value('kecamatan') ?>">
    </div>

    <div class="form-group ">
        <label>Kabupaten/Kota</label>
        <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" value="<?php echo set_value('kabupaten') ?>">
    </div>

    <div class="form-group ">
        <label>Kode Pos</label>
        <input type="text" name="kode_pos" class="form-control" placeholder="Kode Pos" value="<?php echo set_value('kode_pos') ?>">
    </div>

    <div class="form-group ">
        <label>Diterima Dikelas</label>
        <input type="text" name="diterima_kelas" class="form-control" placeholder="Diterima Dikelas" value="<?php echo set_value('diterima_kelas') ?>">
    </div>

    <label>Diterima Pada Tanggal</label>
    <div class="input-group">
         <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
        <input type="date" name="diterima" class="form-control" placeholder="diterima" value="<?php echo set_value('diterima') ?>">
    </div>
</div>

<div class="col-md-3">

     <div class="form-group ">
        <label>Nama Ayah</label>
        <input type="text" name="nama_ayah" class="form-control" placeholder="Nama Ayah" value="<?php echo set_value('nama_ayah') ?>">
    </div>

     <div class="form-group ">
        <label>Pekerjaan Ayah</label>
        <input type="text" name="kerja_ayah" class="form-control" placeholder="Pekerjaan Ayah" value="<?php echo set_value('kerja_ayah') ?>">
    </div>

     <div class="form-group ">
        <label>No Telp Ayah</label>
        <input type="text" name="telp_ayah" class="form-control" placeholder="No Telp Ayah" value="<?php echo set_value('telp_ayah') ?>">
    </div>

     <div class="form-group ">
        <label>Nama Ibu</label>
        <input type="text" name="nama_ibu" class="form-control" placeholder="Nama Ibu" value="<?php echo set_value('nama_ibu') ?>">
    </div>

     <div class="form-group ">
        <label>Pekerjaan Ibu</label>
        <input type="text" name="kerja_ibu" class="form-control" placeholder="Pekerjaan Ibu" value="<?php echo set_value('kerja_ibu') ?>">
    </div>

     <div class="form-group ">
        <label>No Telp Ibu</label>
        <input type="text" name="telp_ibu" class="form-control" placeholder="No Telp Ibu" value="<?php echo set_value('telp_ibu') ?>">
    </div>

    <div class="form-group ">
        <label>Nama Wali</label>
        <input type="text" name="nama_wali" class="form-control" placeholder="Nama Wali" value="<?php echo set_value('nama_wali') ?>">
    </div>

     <div class="form-group ">
        <label>Pekerjaan Wali</label>
        <input type="text" name="kerja_wali" class="form-control" placeholder="Pekerjaan Wali" value="<?php echo set_value('kerja_wali') ?>">
    </div>

     <div class="form-group">
        <label>Alamat Wali</label>
        <textarea name="alamat_wali" class="form-control" placeholder="Alamat"><?php echo set_value('alamat_wali')?></textarea>      
    </div>
</div>

<div class="col-md-3">

     <div class="form-group ">
        <label>No Telp Wali</label>
        <input type="text" name="telp_wali" class="form-control" placeholder="No Telp Wali" value="<?php echo set_value('telp_wali') ?>">
    </div>

     <div class="form-group">
        <label>No HP</label>
        <input type="text" name="no_hp" class="form-control" placeholder="NO HP" value="<?php echo set_value('no_hp') ?>">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo set_value('email') ?>">
    </div>
     <div class="form-group">
        <label>Status Murid</label>
        <select class="form-control" name="status_siswa">
            <option value="aktif">Aktif</option>
            <option value="keluar">Keluar</option>
            <option value="lulus">Lulus</option>
        </select>
    </div>

    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" placeholder="username" value="<?php echo set_value('username') ?>" required>
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password') ?>" required>
    </div>
    
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/siswa')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>

<?php
//form close
echo form_close();
?>