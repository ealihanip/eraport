<p><a href="<?php echo base_url('admin/set_kelas/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Tahun Ajaran</th>
            <th>Kelas</th>
            <th>Siswa</th>
            <th width="10%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($sk as $sk) 
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $sk->tahun?></td>
        <td><?php echo $sk->nama_jurusan ?></td>
        <td><?php echo $sk->nama_siswa ?></td>
        <td>
            <a href="<?php echo base_url('admin/set_kelas/edit/'.$sk->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
            <!--  <?php include('detail.php'); ?>
            <?php include('delete.php'); ?> -->
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>