<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#Delete<?php echo $kelas->id_kelas ?>"><i class="fa fa-trash-o"></i>
</button>
<div class="modal fade" id="Delete<?php echo $kelas->id_kelas ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Hapus Data Kelas : <strong><?php echo $kelas->nama_jurusan ?></strong></h4>
            </div>
            <div class="modal-body">
            <p class="alert alert-danger"><i class="fa fa-warning"></i> Apakah Yakin Ingin Menghapus Data Ini </p>
            </div>
            <div class="modal-footer">
                <a href="<?php echo base_url('admin/kelas/delete/'.$kelas->id_kelas)?>" class="btn btn-danger"><i class=" fa fa-trash-o"></i> Hapus</a>
               <!--  <a href="<?php echo base_url('admin/mapel/edit/'.$mapel->id_mapel)?>" class="btn btn-warning"><i class=" fa fa-edit"></i> Edit</a> -->
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>