<p><a href="<?php echo base_url('admin/kelas/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}

?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Kompetensi Keahlian</th>
            <th>Kelas</th>
            <th>Wali Kelas</th>
            <th width="10%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($kelas as $kelas) 
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $kelas->nama_lengkap ?></td>
        <td><a href="<?php echo base_url('admin/kelas/anggota_kelas/'.$kelas->id_kelas)?>" class="btn btn-info btn-sm">
            <?php echo $kelas->nama_jurusan ?></a>
        </td>
        <td><?php echo $kelas->nama_guru ?></td>
        <td>
                <a href="<?php echo base_url('admin/kelas/edit/'.$kelas->id_kelas) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
               <!--  <?php include('detail.php'); ?> -->
                <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>