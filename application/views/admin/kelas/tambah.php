<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/kelas/tambah'));
?>

<div class="col-md-8">

	<div class="form-group">
		<label>Kompetensi Keahlian</label>
		<select class="form-control js-example-basic-single" name="id_kompetensi">
			<option value="">==Pilih Kompetensi Keahlian==</option>
			<?php foreach ($kompetensi as $kompetensi ) { ?>
				<option value="<?php echo $kompetensi->id_kompetensi ?>">
					<?php echo $kompetensi->nama_lengkap ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Tahun Ajaran</label>
		<select class="form-control" name="id_ajaran">
			<option><?php echo tahun_ajaran_aktif('tahun')?></option>
		</select>
	</div>

	<div class="form-group">
		<label>Wali Kelas</label>
		<select class="form-control js-example-basic-single" name="id_guru">
			<option value="">==Pilih Wali Kelas==</option>
			<?php foreach ($guru as $guru ) { ?>
				<option value="<?php echo $guru->id_guru ?>">
					<?php echo $guru->nama_guru ?>
				</option>
				<?php } ?>
		</select>
	</div>
	

	<div class="form-group">
		<label>Kelas</label>
		<select class="form-control" name="nama_kelas">
			<option value="10">10</option>
			<option value="11">11</option>
			<option value="12">12</option>
		</select>
	</div>

	<div class="form-group">
		<label>Rombel<span class="text-danger"><small>(contoh : X TKJ atau X TKJ 2)</small></span></label>
		<input type="text" name="nama_jurusan" class="form-control" placeholder="Rombel">
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/kelas')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>