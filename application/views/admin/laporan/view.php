<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nomor Induk</th>
            <th>Nama</th>
            <th>Jenis Kelamin</th>
        </tr>
    </thead>
    <tbody>
         <?php 
            $no=1;
            foreach ($kelas as $kelas) 
          { 
        ?>
        <tr>
            <td><?php echo $no; ?></td>
            <td><?php echo $kelas->no_induk ?></td>
            <td><?php echo $kelas->nama_siswa ?> </td>
            <td><?php echo $kelas->jk ?></td>
        </tr>
         <?php $no++; } ?>
    </tbody>
</table>
