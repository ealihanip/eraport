<div class="text-center">
	<h3><strong>DAFTAR SISWA SMK HIDAYAH SEMARANG</strong></h3>
	<h3><strong>TAHUN PELAJARAN 2017/2018</strong></h3>
	<hr>
</div>
<div>
	<table width="100%" class="table table-bordered">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="15%">NIS</th>
				<th width="20%">Nama</th>
				<th width="50%">Alamat</th>
				<th width="10">Kelas</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; foreach ($siswa as $siswa) { ?>
			<tr>
				<td><?php echo $no ?></td>
				<td><?php echo $siswa->no_induk ?></td>
				<td><?php echo $siswa->nama_siswa ?></td>
				<td><?php echo $siswa->alamat ?></td>
				<td><?php echo $siswa->nama_jurusan ?></td>
			</tr>
			<?php $no++; } ?>
		</tbody>
	</table>
</div>