<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
			<th>Nama Laporan</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <tr class="odd gradeX">
            <td>Semua Data Guru</td>
    		<td><a href="<?php echo base_url('admin/laporan/data_guru')?>" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Lihat</a> - <a href="<?php echo base_url('admin/laporan/export')?>" class="btn btn-success"><i class="fa fa-download"></i> Download</a>
            </td>
        </tr>


        <tr class="odd gradeX">
            <td>Data Siswa Per Kelas </td>
            <td>
                <select name="kelas" id="" onchange='window.location="<?php echo base_url("admin/laporan/index?")?>kelas="+this.options[this.selectedIndex].value'>
                    <option value="">== Pilih Kelas ==
                        <?php foreach($kelas as $kelas){?>
                            
                            <option value='<?php echo $kelas->id_kelas?>' <?php if($id_kelas==$kelas->id_kelas){ echo 'selected';}?>><?php echo $kelas->nama_jurusan?></option>
                            
                        <?php } ?>
                    </option>
                </select>
                <a href="<?php echo base_url('admin/laporan/anggota_kelas/'.$id_kelas)?>" target="_blank" class="btn btn-info" <?php if(empty($id_kelas)){ echo 'disabled';}?>><i class="fa fa-file-pdf-o"></i> Lihat</a> - <a href="<?php echo base_url('admin/laporan/export_siswa_per_kelas/'.$id_kelas)?>" class="btn btn-success" <?php if(empty($id_kelas)){ echo 'disabled';}?>><i class="fa fa-download"></i> Download</a>
            </td>
        </tr>

        <tr class="odd gradeX">
            <td>Semua Data Siswa</td>
            <td><a href="<?php echo base_url('admin/laporan/data_siswa')?>" target="_blank" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Lihat</a> - <a href="<?php echo base_url('admin/laporan/export_siswa')?>" class="btn btn-success"><i class="fa fa-download"></i> Download</a>
            </td>
        </tr>

        
        <tr class="odd gradeX">
            <td>Data Siswa Per Kelas</td>
            <td><a href="<?php echo base_url('admin/laporan/per_kelas')?>" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Lihat</a>
            </td>
        </tr>


        
</tbody>
</table>