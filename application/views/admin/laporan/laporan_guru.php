
<table class="table table-striped table-bordered">
	<thead>
		<tr>
			<th>NO</th>
			<th width="5%">NIP</th>
			<th>NAMA Guru</th>
			<th>Alamat</th>
			<th>NO HP</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; foreach($guru as $guru) { ?>
		<tr>
			<td><?php echo $no;?></td>
			<td><?php echo $guru->nip ?></td>
			<td><?php echo $guru->nama_guru ?></td>
			<td><?php echo $guru->alamat ?> RT.<?php echo $guru->rt?> RW.<?php echo $guru->rw?> Kel.<?php echo $guru->desa_kelurahan?> Kec.<?php echo $guru->kecamatan?> </td>
			<td><?php echo $guru->no_hp ?></td>
		</tr>
		<?php $no++; } ?>
	</tbody>
</table>