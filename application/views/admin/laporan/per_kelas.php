<p><a href="<?php echo base_url('admin/laporan')?>" class="btn btn-success"><i class="fa fa-backward"></i> Kembali</a></p>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="30%">Kompetensi Keahlian</th>
            <th>Kelas</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            foreach ($kelas as $kelas) {
        ?>
    <tr class="odd gradeX">
        <td><?php echo $kelas->nama_lengkap ?></td>
        <td><a href="<?php echo base_url('admin/laporan/anggota_kelas/'.$kelas->id_kelas)?>" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i>
            <?php echo $kelas->nama_jurusan ?></a>  - <a href="<?php echo base_url('admin/laporan/export_siswa_per_kelas/'.$kelas->id_kelas)?>" class="btn btn-success"><i class="fa fa-download"></i> Download</a>
        </td>
    </tr>
    <?php  }  ?>
</tbody>
</table>