<?php
$id_kelas   = $this->session->set_userdata('id_kelas');
$kelas_aktif = $this->model_kelas->detail($id_kelas);
?>
<div class="text-center">
	<h3><strong>SMK HIDAYAH SEMARANG</strong></h3>
	<h3><strong>TAHUN PELAJARAN 2017/2018</strong></h3>
	<hr>
</div>
<div>
	<table>
		<tbody>
			<tr>
				<th style="width: 20%">BIDANG KEAHLIAN</th>
				<td style="width: 5%">:</td>
				<td style="width: 50%"><?php echo $detail_kelas[0]->nama_bidang_keahlian?></td>
				<th style="width: 15%">SEMESTER</th>
				<td style="width: 5%">:</td>
				<td style="width: 20%"><?php echo $semester_aktif?></td>
			</tr>
			<tr>
				<th style="width: 20%">PROGRAM KEAHLIAN</th>
				<td style="width: 5%">:</td>
				<td style="width: 50%"><?php echo $detail_kelas[0]->nama_lengkap?></td>
			</tr>
			<tr>
				<th>KELAS</th>
				<td>:</td>
				<td><?php echo $detail_kelas[0]->nama_kelas?></td>
			</tr>
			<tr>
				<th>WALI KELAS</th>
				<td>:</td>
				<td><?php echo $detail_kelas[0]->nama_guru?></td>
			</tr>
		</tbody>
	</table>
	<br>
	<table width="100%" class="table table-bordered">
		<thead>
			<tr>
				<th width="5%">No</th>
				<th width="15%">NIS</th>
				<th width="20%">Nama</th>
				<th width="50%">Alamat</th>
			</tr>
		</thead>
		<tbody>
			<?php $no=1; foreach ($kelas as $kelas) { ?>
			<tr>
				<td><?php echo $no ?></td>
				<td><?php echo $kelas->no_induk ?></td>
				<td><?php echo $kelas->nama_siswa ?></td>
				<td><?php echo $kelas->alamat ?></td>
			</tr>
		<?php $no++; } ?>
		</tbody>
	</table>
</div>