<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/tahun_dan_semester/update/'));
?>



<div class='col-lg-12'>

	<div class='row'>
		<div class="col-md-4">

			

			<div class="form-group">
				<label for="exampleFormControlSelect1">Tahun Ajaran</label>
				<select class="form-control" name="ajaran">
					
					<?php foreach($ajaran as $ajaran){?>
						
						
						<option value='<?php echo $ajaran->id_ajaran?>' <?php if($ajaran->aktif==true){echo 'selected';}?>><?php echo $ajaran->tahun?></option>


					<?php }?>
				</select>
			</div>

			
			
		</div>

	</div>

	<div class='row'>
		<div class="col-md-4">

			

			<div class="form-group">
				<label for="exampleFormControlSelect1">Semester</label>
				<select class="form-control" name="semester">
					<option value='1' <?php if($semester->id_semester==1){echo 'selected';}?>>Ganjil</option>
					<option value='2' <?php if($semester->id_semester==2){echo 'selected';}?>>Genap</option>
				</select>
			</div>

			
			
		</div>

	</div>

	<div class='row'>

		<div class="col-md-12 text-center">
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
				
				
			</div>
		</div>

	</div>
<hr>
</div>



<?php echo form_close();?>