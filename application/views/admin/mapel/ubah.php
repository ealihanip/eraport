<?php 
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

echo form_open(base_url('admin/set_mapel/edit/'.$setmapel->id_gm)); ?>

 <div class="col-md-6">
 	 <div class="form-group">
        <label>Tahun Ajaran</label>
        <select class="form-control" name="id_ajaran">
            <?php foreach ($ajaran as $ajaran ) { ?>
                <option value="<?php echo $ajaran->id_ajaran ?>"<?php if ($setmapel->id_ajaran==$ajaran->id_ajaran) {
                echo "selected";} ?>>
                    <?php echo $ajaran->tahun ?>
                </option>
                <?php } ?>
        </select>
    </div>

 	 <div class="form-group">
        <label>Nama Guru</label>
        <select class="form-control" name="id_guru">
            <?php foreach ($guru as $guru ) { ?>
                <option value="<?php echo $guru->id_guru ?>"<?php if ($setmapel->id_guru==$guru->id_guru) {
                echo "selected";} ?>>
                    <?php echo $guru->nama_guru ?>
                </option>
                <?php } ?>
        </select>
    </div>
</div>

     <div class="col-md-6">
     <div class="form-group">
        <label>Kelas</label>
        <select class="form-control" name="id_kelas">
            <?php foreach ($kelas as $kelas ) { ?>
                <option value="<?php echo $kelas->id_kelas ?>"<?php if ($setmapel->id_kelas==$kelas->id_kelas) {
                echo "selected";} ?>>
                    <?php echo $kelas->nama_jurusan ?>
                </option>
                <?php } ?>
        </select>
    </div>

     <div class="form-group">
        <label>Mata pelajaran</label>
        <select class="form-control" name="id_mapel">
            <?php foreach ($mapel as $mapel ) { ?>
                <option value="<?php echo $mapel->id_mapel ?>"<?php if ($setmapel->id_mapel==$mapel->id_mapel) {
                echo "selected";} ?>>
                    <?php echo $mapel->nama_mapel ?>
                </option>
                <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label>Bobot</label>
        <input type="text" name="bobot" class="form-control" value="<?php echo $setmapel->bobot?>">
    </div>

</div>
<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
    </div>
</div>

<?php echo form_close(); ?>