<p><a href="<?php echo base_url('admin/mapel/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Kode Mapel</th>
            <th>Mata Pelajaran</th>
            <th width="15%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($mapel as $mapel) 
          { 
        ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $mapel->kode_mapel ?></td>
        <td><?php echo $mapel->nama_mapel ?></td>
        <td>
            <a href="<?php echo base_url('admin/mapel/edit/'.$mapel->id_mapel) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
            <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>