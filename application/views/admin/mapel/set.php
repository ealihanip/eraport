<p><a href="<?php echo base_url('admin/set_mapel/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Tahun Ajaran</th>
            <th>Nama Guru</th>
            <th>Kelas</th>
            <th>Mata Pelajaran</th>
            <th>Bobot</th>
            <th width="15%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($setmapel as $sm) 
          { 
        ?>
    <tr>
        <td><?php echo $i; ?></td>
        <td><?php echo $sm->tahun ?></td>
        <td><?php echo $sm->nama_guru ?></td>
        <td><?php echo $sm->nama_jurusan ?></td>
        <td><?php echo $sm->nama_mapel ?></td>
        <td><?php echo $sm->bobot ?></td>
        <td>
            <a href="<?php echo base_url('admin/set_mapel/edit/'.$sm->id_gm) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i> Edit</a>
            <?php include('hapus.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>