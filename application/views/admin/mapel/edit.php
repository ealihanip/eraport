<?php 
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

echo form_open(base_url('admin/mapel/edit/'.$mapel->id_mapel));
 ?>

 <div class="col-md-6">
 	<div class="form-group">
 		<label>Kode Mapel</label>
 		<input type="text" name="kode_mapel" class="form-control" placeholder="Kode Mapel" value="<?php echo $mapel->kode_mapel ?>">
 	</div>

 	<div class="form-group">
 		<label>Nama Mapel</label>
 		<input type="text" name="nama_mapel" class="form-control" placeholder="Nama Mapel" value="<?php echo $mapel->nama_mapel ?>">
 	</div>
	 <div class="form-group ">
        <label>Set Golongan Mapel</label>
        <select class="form-control" name="id_golongan">


			<?php foreach($golongan as $datagolongan){?>
				
				<option value="<?php echo $datagolongan->id?>" <?php if($mapel->id_golongan==$datagolongan->id){echo 'selected';}?>><?php echo $datagolongan->nama?></option>

			<?php }?>
        </select>
    </div>
</div>
<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
    </div>
</div>

<?php echo form_close(); ?>