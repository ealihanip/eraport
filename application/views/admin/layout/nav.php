<?php
//dapatkan id user saat login
$id_admin = $this->session->userdata('id_admin');
$user_aktif = $this->model_user->detail($id_admin);
?>
<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

        <?php if($this->session->username=='kepalasekolah'){?>
    

            <!-- Menu Guru  -->
            <li><a href="<?php echo base_url('admin/laporan')?>"><i class="fa fa-users"></i> Laporan Guru</a></li>
                <li><a href="<?php echo base_url('admin/laporan/data_siswa')?>"><i class="fa fa-users"></i> Laporan Siswa</a></li>
            <li class="text-center">
                <?php if ($user_aktif->foto != "") { ?>
               <img src="<?php echo base_url('assets/upload/image/'.$user_aktif->foto) ?>" class="img img-thumbnail img-responsive" width="100">

            <?php } ?>
		      

            <?php }else{?>

               </li>
            <li><a href="<?php echo base_url('admin/dasbor')?>"><i class="fa fa-dashboard "></i> Dasbor</a></li>
            <li><a href="<?php echo base_url('admin/data_sekolah')?>"><i class="fa fa-dashboard "></i> Data Sekolah</a></li>
                            
            <!-- Menu Guru  -->
            <li><a href="<?php echo base_url('admin/guru')?>"><i class="fa fa-users"></i> Guru</a></li>

            <!-- Menu Siswa  -->
            <li><a href="<?php echo base_url('admin/siswa')?>"><i class="fa fa-graduation-cap"></i> Siswa</a></li>

            <!-- Mapel -->
            <li><a href="#"><i class="fa fa-book"></i> Mata Pelajaran<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/mapel')?>">Data Mata Pelajaran</a></li>
                    <li><a href="<?php echo base_url('admin/set_mapel')?>">Set Mapel</a></li>
                </ul>
            </li>  

             <!-- Kelas -->
             <li><a href="<?php echo base_url('admin/kelas')?>">Kelas</a></li>
           <!--  <li><a href=""><i class="fa fa-clipboard"></i> Kelas<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/kelas')?>">Data Kelas</a></li>
                    <li><a href="<?php echo base_url('admin/set_kelas')?>">Set Kelas</a></li>
                </ul>
            </li> -->


            <!-- Ekskul -->
            <li><a href="<?php echo base_url('admin/ekskul')?>">Ekstrakurikuler</a></li>
            <!-- Ekskul -->
            <li><a href="<?php echo base_url('admin/predikat')?>">Predikat</a></li>
            <!-- Ekskul -->
            <li><a href="<?php echo base_url('admin/tahun_dan_semester')?>">Setting Tahun Ajaran dan Semester</a></li>

            <!-- Sikap -->
           <!--  <li><a href="<?php echo base_url('admin/sikap')?>">Sikap</a></li> -->

            <!-- laporan -->
            <li><a href="#"><i class="fa fa-file"></i> laporan<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/laporan')?>">Laporan Guru</a></li>
                    <li><a href="<?php echo base_url('admin/laporan/data_siswa')?>">Laporan Siswa</a></li>
                </ul>
            </li>  

            <!-- Menu User  -->
            <li><a href="#"><i class="fa fa-user"></i> User/Admin<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/user')?>">Data User</a></li>
                    <li><a href="<?php echo base_url('admin/user/tambah')?>">Tambah Data User</a></li>
                </ul>
            </li> 

            <?php }?> 
        </ul>
    </div>   
</nav>  
<!-- /. NAV SIDE  -->
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12"><h2><?php echo $title ?></h2></div>
        </div>
         <!-- /. ROW  -->
         <hr />
       
        <div class="row">
            <div class="col-md-12">
            <!-- Advanced Tables -->
                <div class="panel panel-default">
                 <!--    <div class="panel-heading"><?php echo $title ?></div> -->
                    <div class="panel-body">
                        <div class="table-responsive">
