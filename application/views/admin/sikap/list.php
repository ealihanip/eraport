<p><a href="<?php echo base_url('admin/sikap/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Tahun Pelajaran</th>
            <th>Nama Sikap</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
         <?php
                $no=1; foreach ($sikap as $sikap) { ?>
        <tr class="odd gradeX">
            <td><?php echo $no; ?></td>
            <td><?php echo tahun_ajaran_aktif('tahun')?></td>
            <td><?php echo $sikap->butiran_sikap?></td>
            <td> 
                <a href="<?php echo base_url('admin/sikap/edit/'.$sikap->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
                <?php include('delete.php'); ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>
