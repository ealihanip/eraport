<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/ekskul/edit/'.$eks->id));
?>

<div class="col-md-8">

	<div class="form-group">
		<label>Tahun Ajaran</label>
		<select class="form-control" name="id_ajaran" readonly>
			<option><?php echo tahun_ajaran_aktif('tahun')?></option>
		</select>
	</div>

	<div class="form-group">
		<label>Nama Pembina</label>
		<select class="form-control" name="id_guru">
			<?php foreach ($guru as $guru ) { ?>
				<option value="<?php echo $guru->id_guru ?>"<?php if ($eks->id_guru==$guru->id_guru) {
					echo "selected";} ?>>
					<?php echo $guru->nama_guru ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Nama Ekstrakurikuler</label>
		<input type="text" name="nama_ekskul" class="form-control" placeholder="" value="<?php echo $eks->nama_ekskul?>">
	</div>

	<div class="form-group">
		<label>Nama Ketua</label>
		<input type="text" name="nama_ketua" class="form-control" placeholder="" value="<?php echo $eks->nama_ketua?>">
	</div>

	<div class="form-group">
		<label>NO HP</label>
		<input type="text" name="no_hp" class="form-control" placeholder="" value="<?php echo $eks->no_hp?>">
	</div>

	<div class="form-group">
		<label>Alamat</label>
		<textarea name="alamat" class="form-control"><?php echo $eks->alamat?></textarea>
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/ekskul')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>