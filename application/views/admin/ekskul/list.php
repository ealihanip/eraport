<p><a href="<?php echo base_url('admin/ekskul/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="2%">#</th>
            <th width="3%">Tahun Pelajaran</th>
            <th width="5%">Nama Ekskul</th>
            <th width="10%">Pembina</th>
            <th width="10%">Ketua</th>
            <th width="3%">NO HP</th>
            <th width="15%">Alamat</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
         <?php
                $no=1; foreach ($eks as $eks) { ?>
        <tr class="odd gradeX">
            <td><?php echo $no; ?></td>
            <td><?php echo tahun_ajaran_aktif('tahun')?></td>
            <td><?php echo $eks->nama_ekskul?></td>
            <td><?php echo $eks->nama_guru?></td>
            <td><?php echo $eks->nama_ketua?></td>
            <td><?php echo $eks->no_hp?></td>
            <td><?php echo $eks->alamat?></td>
            <td> 
                <a href="<?php echo base_url('admin/ekskul/edit/'.$eks->id) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
               <!--  <?php include('detail.php'); ?> -->
                <?php include('delete.php'); ?></td>
        </tr>
        <?php $no++; } ?>
    </tbody>
</table>
