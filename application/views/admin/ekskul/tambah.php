<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/ekskul/tambah'));
?>

<div class="col-md-8">

	<div class="form-group">
		<label>Tahun Ajaran</label>
		<select class="form-control" name="id_ajaran">
			<option><?php echo tahun_ajaran_aktif('tahun')?></option>
		</select>
	</div>

	<div class="form-group">
		<label>Nama Pembina</label>
		<select class="form-control js-example-basic-single" name="id_guru">
			<option value="">==Pilih Pembina==</option>
			<?php foreach ($guru as $guru ) { ?>
				<option value="<?php echo $guru->id_guru ?>">
					<?php echo $guru->nama_guru ?>
				</option>
				<?php } ?>
		</select>
	</div>

	<div class="form-group">
		<label>Nama Ekstrakurikuler</label>
		<input type="text" name="nama_ekskul" class="form-control" placeholder="">
	</div>

	<div class="form-group">
		<label>Nama Ketua</label>
		<input type="text" name="nama_ketua" class="form-control" placeholder="">
	</div>

	<div class="form-group">
		<label>NO HP</label>
		<input type="text" name="no_hp" class="form-control" placeholder="">
	</div>

	<div class="form-group">
		<label>Alamat</label>
		<textarea name="alamat" class="form-control"></textarea>
	</div>
	
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/ekskul')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>

<script type="text/javascript">
	// In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>