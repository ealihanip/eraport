<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/predikat/update/'));
?>


<?php foreach($predikat as $predikat){?>

<div class='row'>
	<div class="col-md-8">

		<div class="form-group">
			<label>Predikat:</label>
			<label><?php echo $predikat->predikat?></label>
		</div>

		<div class="form-group">
			<label>Batas Bawah</label>
			<input type="text" name="batas_bawah[<?php echo $predikat->id?>]" class="form-control" value="<?php echo $predikat->batas_bawah?>">
		</div>

		<div class="form-group">
			<label>Batas Atas</label>
			<input type="text" name="batas_atas[<?php echo $predikat->id?>]" class="form-control" value="<?php echo $predikat->batas_atas?>">
		</div>

		
		
	</div>
</div>
<hr>

<?php }?>
<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/sikap')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>