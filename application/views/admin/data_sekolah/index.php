<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open_multipart(base_url('admin/data_sekolah/update/'.$data_sekolah->id));
?>



<div class='row'>
	<div class="col-md-6">
	

		<div class="form-group">
			<label>NPSN</label>
			<input type="text" name="npsn" class="form-control" placeholder="" value='<?php echo $data_sekolah->npsn?>'>
		</div>

		<div class="form-group">
			<label>Nama</label>
			<input type="text" name="nama" class="form-control" placeholder="" value='<?php echo $data_sekolah->nama?>'>
		</div>

		<div class="form-group">
			<label>Alamat</label>
			<textarea type="text" name="alamat" class="form-control" placeholder=""><?php echo $data_sekolah->alamat?></textarea>
		</div>

		<div class="form-group">
			<label>Desa/keluarahan</label>
			<input type="text" name="desa_kelurahan" class="form-control" placeholder="" value='<?php echo $data_sekolah->desa_kelurahan?>'>
		</div>

		<div class="form-group">
			<label>Kecamatan</label>
			<input type="text" name="kecamatan" class="form-control" placeholder="" value='<?php echo $data_sekolah->kecamatan?>'>
		</div>

		<div class="form-group">
			<label>Kabupaten</label>
			<input type="text" name="kabupaten" class="form-control" placeholder="" value='<?php echo $data_sekolah->kabupaten?>'>
		</div>

		<div class="form-group">
			<label>Provinsi</label>
			<input type="text" name="provinsi" class="form-control" placeholder="" value='<?php echo $data_sekolah->provinsi?>'>
		</div>

		<div class="form-group">
			<label>Kode Pos</label>
			<input type="text" name="kode_pos" class="form-control" placeholder="" value='<?php echo $data_sekolah->kode_pos?>'>
		</div>

		<div class="form-group">
			<label>Telepon</label>
			<input type="text" name="telepon" class="form-control" placeholder="" value='<?php echo $data_sekolah->telepon?>'>
		</div>

		<div class="form-group">
			<label>Fax</label>
			<input type="text" name="fax" class="form-control" placeholder="" value='<?php echo $data_sekolah->fax?>'>
		</div>

		<div class="form-group">
			<label>Email</label>
			<input type="text" name="email" class="form-control" placeholder="" value='<?php echo $data_sekolah->email?>'>
		</div>

		<div class="form-group">
			<label>Website</label>
			<input type="text" name="website" class="form-control" placeholder="" value='<?php echo $data_sekolah->website?>'>
		</div>

	</div>


	<div class='col-md-6'>


		

		<div class="form-group">
			<label for="exampleInputFile">Logo</label>
			<input type="file" class="form-control-file" name="logo" aria-describedby="fileHelp">
			
		</div>

		<div class="form-group">
			<label for="exampleInputFile">Logo Saat Ini</label>

		</div>

		<div class="form-group">
		
			<img class='' src='<?php echo base_url('assets/upload/logo/'.$data_sekolah->logo_sekolah)?>' width='300px' height='300px'>
		</div>

		
	</div>


	</div>

	
<hr>


<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/sikap')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>


<?php echo form_close();?>