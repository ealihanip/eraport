<?php
//notifikaksi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open_multipart(base_url('admin/user/tambah'));
?>

<div class="col-md-6">
	<div class="form-group">
		<label>Nama</label>
		<input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" value="<?php echo set_value('nama') ?>" required>
	</div>

	<div class="form-group">
		<label>Email</label>
		<input type="email" name="email" class="form-control" placeholder="Masukkan Email" value="<?php echo set_value('email') ?>" required>
	</div>

	<div class="form-group">
		<label>Username</label>
		<input type="text" name="username" class="form-control" placeholder="Masukkan Username" value="<?php echo set_value('username') ?>">
	</div>

	<div class="form-group">
		<label>Password</label>
		<input type="password" name="password" class="form-control" placeholder="Masukkan Password" value="<?php echo set_value('password') ?>" required>
	</div>

	<div class="form-group">
		<label>Upload Foto</label>
		<input type="file" name="foto" class="form-control" placeholder="Foto" value="<?php echo set_value('foto') ?>">
	</div>

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>
</div>


<?php
//form close
echo form_close();
?>