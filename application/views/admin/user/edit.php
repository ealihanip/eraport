<?php
//notifikaksi kala ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>','</div>');

//open form
echo form_open(base_url('admin/user/edit/'.$user->id_admin));
?>

<div class="col-md-6">
	<div class="form-group">
		<label>Nama</label>
		<input type="text" name="nama" class="form-control" placeholder="Masukkan Nama" value="<?php echo $user->nama ?>">
	</div>

	<div class="form-group">
		<label>Email</label>
		<input type="email" name="email" class="form-control" placeholder="Masukkan Email" value="<?php echo  $user->email ?>">
	</div>

	<div class="form-group">
		<label>Username</label>
		<input type="text" name="username" class="form-control" placeholder="Masukkan Username" value="<?php echo $user->username ?>" >
	</div>

	<div class="form-group">
		<label>Password<span class="text-danger"><small>(password minimal 5 karakter atau dibiarkan kosong)</small></span></label>
		<input type="password" name="password" class="form-control" placeholder="Masukkan Password" value="<?php echo set_value('password') ?>">
	</div>

	<div class="form-group">
		<label>Foto(<span class="text-warning">atau biarkan kosong</span>)</label>
		<input type="file" name="foto" class="form-control" placeholder="Foto" value="<?php echo $user->foto ?>">
		<br>

		<?php if ($user->foto =="") { ?>
			<span class="text-danger"><small>Belum ada Foto yang di upload</small></span>
			<?php }else{?>
				<img src="<?php echo base_url('assets/upload/image/thumbs/'.$user->foto) ?>" class="img img-thumbnail" width="60">
			<?php }?>
	</div>

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>
</div>


<?php
//form close
echo form_close();
?>