<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/guru/tambah'));
?>

<div class="col-md-4">
    <div class="form-group ">
        <label>Nama Guru</label>
        <input type="text" name="nama_guru" class="form-control" placeholder="Nama Guru" value="<?php echo set_value('nama_guru') ?>" required>
    </div>

    <div class="form-group ">
        <label>NIP</label>
        <input type="text" name="nip" class="form-control" placeholder="NIP" value="<?php echo set_value('nip') ?>">
    </div>

    <div class="form-group ">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jk">
            <option value="Laki-laki">Laki-Laki</option>
            <option value="Perempuan">Perempuan</option>
        </select>
    </div>

    <div class="form-group ">
        <label>Tempat Lahir</label>
        <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo set_value('tempat_lahir') ?>" >
    </div>

    <div class="form-group ">
        <label>Tanggal Lahir</label>
        <input type="date" name="tgl_lahir" class="form-control" placeholder="DD-MM-YYYY" value="<?php echo set_value('tgl_lahir') ?>" >
    </div>

    <div class="form-group ">
        <label>Agama</label>
        <select class="form-control" name="agama">
            <option value="islam">Islam</option>
            <option value="kristen">Kristen</option>
            <option value="khatolik">Khatolik</option>
            <option value="hindu">Hindu</option>
            <option value="budha">Budha</option>
        </select>
    </div>
</div>

<div class="col-md-4">

    <div class="form-group">
        <label>Alamat</label>
       <textarea name="alamat" class="form-control" placeholder="Alamat"></textarea>
    </div>

    <div class="form-group">
        <label>RT</label>
        <input type="text" name="rt" class="form-control" placeholder="RT" value="<?php echo set_value('rt') ?>">
    </div>

    <div class="form-group ">
        <label>RW</label>
        <input type="text" name="rw" class="form-control" placeholder="RW" value="<?php echo set_value('rw') ?>">
    </div>

    <div class="form-group ">
        <label>Desa / Kelurahan</label>
        <input type="text" name="desa_kelurahan" class="form-control" placeholder="Desa / Kelurahan" value="<?php echo set_value('desa_kelurahan') ?>">
    </div>

    <div class="form-group ">
        <label>Kecamatan</label>
        <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" value="<?php echo set_value('kecamatan') ?>">
    </div>

    <div class="form-group ">
        <label>Kabupaten</label>
        <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" value="<?php echo set_value('kabupaten') ?>">
    </div>

    <div class="form-group ">
        <label>Kode Pos</label>
        <input type="text" name="kode_pos" class="form-control" placeholder="Kode Pos" value="<?php echo set_value('kode_pos') ?>">
    </div>

</div>

<div class="col-md-4">
    <div class="form-group">
        <label>No HP</label>
        <input type="text" name="no_hp" class="form-control" placeholder="NO HP" value="<?php echo set_value('no_hp') ?>">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo set_value('email') ?>">
    </div>

    <!-- <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="aktif">
            <option value="1">Aktif</option>
            <option value="0">Non-AKtif</option>
        </select>
    </div> -->

    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" placeholder="username" value="<?php echo set_value('username') ?>" required>
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password') ?>" required>
    </div>

     <div class="form-group">
        <label>Akses Level</label>
        <select class="form-control" name="akses_level">
            <option value="1">Guru</option>
            <option value="2">Wali Kelas</option>
            <option value="3">Kepala Sekolah</option>
           <!--  <option value="4">Staff Tu</option>
            <option value="5">Karyawan</option> -->
        </select>
    </div>

   <!--  <div class="form-group">
        <label>Upload Cover/Gambar Buku</label>
        <input type="file" name="foto" class="form-control" placeholder="Cover Buku" value="<?php echo set_value('foto') ?>">
    </div> -->
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/guru')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>

<?php
//form close
echo form_close();
?>