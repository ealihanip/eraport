<p><a href="<?php echo base_url('admin/guru/tambah')?>" class="btn btn-success"><i class="fa fa-plus"></i> Tambah</a></p>

<?php
//notifikasi
if ($this->session->flashdata('sukses')) 
{
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>
<table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th width="3%">#</th>
            <th width="15%">Nama</th>
            <th width="10%">NIP</th>
            <th width="5%">Jenis Kelamin</th>
            <th width="5%">Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
            $i=1;
            foreach ($guru as $guru) 
          { 
        ?>
    <tr class="odd gradeX">
        <td><?php echo $i; ?></td>
        <td><?php echo $guru->nama_guru ?></td>
        <td><?php echo $guru->nip ?></td>
        <td><?php echo $guru->jk ?></td>
        <td>
                <a href="<?php echo base_url('admin/guru/edit/'.$guru->id_guru) ?>" class="btn btn-warning btn-xs"><i class="fa fa-edit"></i></a> 
                <?php include('detail.php'); ?>
                <?php include('delete.php'); ?>
        </td>
    </tr>
    <?php $i++; } ?>
</tbody>
</table>