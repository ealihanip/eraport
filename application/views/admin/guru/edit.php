<?php
//notifikasi kalau ada input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning">','</div>');

//kalau ada error upload tampilkan
if (isset($error)) 
{
    echo '<div class="alert alert-warning">';
    echo $error;
    echo '</div>';
}

//open form
echo form_open(base_url('admin/guru/edit/'.$guru->id_guru));
?>

<div class="col-md-4">
    <div class="form-group form-group-lg">
        <label>Nama Guru</label>
        <input type="text" name="nama_guru" class="form-control" placeholder="Nama Guru" value="<?php echo $guru->nama_guru ?>" required>
    </div>

    <div class="form-group form-group-lg">
        <label>NIP</label>
        <input type="text" name="nip" class="form-control" placeholder="NIP" value="<?php echo $guru->nip ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>Jenis Kelamin</label>
        <select class="form-control" name="jk">
            <option value="Laki-laki">Laki-Laki</option>
            <option value="Perempuan"<?php if ($guru->jk=="Perempuan") { echo "selected"; }?>>Perempuan</option>
        </select>
    </div>

    <div class="form-group form-group-lg">
        <label>Tempat Lahir</label>
        <input type="text" name="tempat_lahir" class="form-control" placeholder="Tempat Lahir" value="<?php echo $guru->tempat_lahir ?>" >
    </div>

    <div class="form-group form-group-lg">
        <label>Tanggal Lahir</label>
        <input type="date" name="tgl_lahir" class="form-control" value="<?php echo $guru->tgl_lahir ?>" >
    </div>

    <div class="form-group form-group-lg">
        <label>Agama</label>
        <select class="form-control" name="agama">
            <option value="islam">Islam</option>
            <option value="kristen"<?php if ($guru->agama=="kristen") { echo "selected"; }?>>Kristen</option>
            <option value="khatolik"<?php if ($guru->agama=="khatolik") { echo "selected"; }?>>Khatolik</option>
            <option value="hindu"<?php if ($guru->agama=="hindu") { echo "selected"; }?>>Hindu</option>
            <option value="budha"<?php if ($guru->agama=="budha") { echo "selected"; }?>>Budha</option>
        </select>
    </div>

</div>

<div class="col-md-4">
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" placeholder="Alamat"><?php echo $guru->alamat ?></textarea>
    </div>

    <div class="form-group form-group-lg">
        <label>RT</label>
        <input type="text" name="rt" class="form-control" placeholder="RT" value="<?php echo $guru->rt ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>RW</label>
        <input type="text" name="rw" class="form-control" placeholder="RW" value="<?php echo $guru->rw ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>Desa / Kelurahan</label>
        <input type="text" name="desa_kelurahan" class="form-control" placeholder="Desa / Kelurahan" value="<?php echo $guru->desa_kelurahan ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>Kecamatan</label>
        <input type="text" name="kecamatan" class="form-control" placeholder="Kecamatan" value="<?php echo $guru->kecamatan ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>Kabupaten</label>
        <input type="text" name="kabupaten" class="form-control" placeholder="Kabupaten" value="<?php echo $guru->kabupaten ?>">
    </div>

    <div class="form-group form-group-lg">
        <label>Kode Pos</label>
        <input type="text" name="kode_pos" class="form-control" placeholder="Kode Pos" value="<?php echo $guru->kode_pos ?>">
    </div>

</div>

<div class="col-md-4">
    <div class="form-group">
        <label>No HP</label>
        <input type="text" name="no_hp" class="form-control" placeholder="NO HP" value="<?php echo $guru->no_hp ?>">
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $guru->email ?>">
    </div>

    <!-- <div class="form-group">
        <label>Status</label>
        <select class="form-control" name="aktif">
            <option value="1">Aktif</option>
            <option value="0"<?php if ($guru->aktif=="0") { echo "selected"; }?>>Non-AKtif</option>
        </select>
    </div> -->

    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" placeholder="username" value="<?php echo $guru->username ?>" required>
    </div>

    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo set_value('password') ?>">
    </div>

     <div class="form-group">
        <label>Akses Level</label>
        <select class="form-control" name="akses_level">
            <option value="1">Guru</option>
            <option value="2"<?php if ($guru->akses_level=="2") { echo "selected"; }?>>Wali Kelas</option>
            <option value="3"<?php if ($guru->akses_level=="3") { echo "selected"; }?>>Kepala Sekolah</option>
        </select>
    </div>

  <!--  <div class="form-group">
        <label>Upload Foto(<span class="text-warning">atau biarkan kosong</span>)</label>
        <input type="file" name="foto" class="form-control" placeholder="Foto" value="<?php echo $guru->foto ?>">
        <br>

        <?php if ($guru->foto =="") { ?>
            <span class="text-danger"><small>Belum ada cover yang di upload</small></span>
            <?php }else{?>
                <img src="<?php echo base_url('assets/upload/image/thumbs/'.$guru->foto) ?>" class="img img-thumbnail img-responsive" width="100" >
            <?php }?>
    </div> -->
</div>

<div class="col-md-12 text-center">
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan Data">
        <input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
        <a href="<?php echo base_url('admin/guru')?>" class="btn btn-danger btn-lg"><i class="fa fa-backward"> Kembali</i></a>
    </div>
</div>

<?php
//form close
echo form_close();
?>