<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nilai');
		$this->load->model('model_ajaran');
		$this->load->model('model_mapel');
		$this->load->model('model_kelas');
		$this->load->model('model_sn');
		$this->load->model('model_siswa');
		$this->load->model('model_sn');
	}

	public function pengetahuan()
	{
		// asumsi
		$id_ajaran='';
		// asumsi
		$id_kelas='';
		// asumsi
		$id_mapel='';
		// asumsi
		$id_sn='';


		if(isset($_GET['ajaran']) && $_GET['ajaran']!=''){


            $where=array(

                'id_siswa'=>$this->session->id_siswa
            );
    
            $siswa=$this->model_siswa->listing($where);

            
			$id_ajaran=$_GET['ajaran'];

            $id_kelas=$siswa[0]->id_kelas;

			$where=array(

				'kelas.id_kelas'=>$id_kelas
	
			);
	
			$kelas 	= $this->model_kelas->listing($where);

		}else{

			$kelas=array();
		}
		
		// get siswa sesuai kelas not jquery :D
		if(!empty($id_kelas)){

			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);

			$mapel = $this->model_mapel->listing($where);

			
		}else{

            $mapel=array();
			$siswa = array();

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){

			$id_mapel=$_GET['mapel'];

			$where=array(

				'set_nilai.id_ajaran'=>$id_ajaran,
				'set_nilai.id_kelas'=>$id_kelas,
				'set_nilai.id_mapel'=>$id_mapel,
				'set_nilai.id_aspek'=>'1'


			);

			$sn = $this->model_sn->listing($where);


		}else{

			$sn=array();

		}


		if(isset($_GET['sn']) && $_GET['sn']!=''){
			
			$id_sn=$_GET['sn'];

			
			//kondisi jika memilih uas atau uts
			if($id_sn!='uas' && $id_sn!='uts'){

				$where=array(

					'nilai.id_sn'=>$id_sn
				);
	
				$nilai=$this->model_nilai->listing($where);
	

			}else{
				
				
				//jika itu adalah uts maka tipe adalah 1
				if($this->input->get('sn')=='uts'){

					$tipe=1;

				}else{

					$tipe=2;

				}

				$where=array(


					'nilai_ujian.id_aspek'=>1,
					'nilai_ujian.id_ajaran'=>$this->input->get('ajaran'),
					'nilai_ujian.id_mapel'=>$this->input->get('mapel'),
					'nilai_ujian.id_kelas'=>$this->input->get('kelas'),
					'nilai_ujian.tipe_ujian'=>$tipe
				);
				
				
				$nilai=$this->model_nilai->listing_ujian($where);	
				
				
			}
			

		}else{

			$nilai=array();
		}

		

		$ajaran = $this->model_ajaran->listing();
		


		
		$data = array(	'title' 	=> 'Nilai Pengetahuan',
						'key' 		=> 'pengetahuan',
						'id_kelas'  => $id_kelas,
						'id_mapel'  => $id_mapel,
						'id_sn'    	=> $id_sn,
						'id_ajaran' => $id_ajaran,
						'ajaran' 	=> $ajaran,
						'mapel' 	=> $mapel,
						'kelas' 	=> $kelas,
						'nilai' 	=> $nilai,
						'siswa' 	=> $siswa,
						'sn' 		=> $sn,
						'isi' 		=> 'siswa/nilai/nilai');
		$this->load->view('siswa/layout/wrapper', $data, FALSE);
	}

	public function keterampilan()
	{
		// asumsi
		$id_ajaran='';
		// asumsi
		$id_kelas='';
		// asumsi
		$id_mapel='';
		// asumsi
		$id_sn='';


		if(isset($_GET['ajaran']) && $_GET['ajaran']!=''){


            $where=array(

                'id_siswa'=>$this->session->id_siswa
            );
    
            $siswa=$this->model_siswa->listing($where);

            
			$id_ajaran=$_GET['ajaran'];

            $id_kelas=$siswa[0]->id_kelas;

			$where=array(

				'kelas.id_kelas'=>$id_kelas
	
			);
	
			$kelas 	= $this->model_kelas->listing($where);

		}else{

			$kelas=array();
		}
		
		// get siswa sesuai kelas not jquery :D
		if(!empty($id_kelas)){

			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);

			$mapel = $this->model_mapel->listing($where);

			
		}else{

            $mapel=array();
			$siswa = array();

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){

			$id_mapel=$_GET['mapel'];

			$where=array(

				'set_nilai.id_ajaran'=>$id_ajaran,
				'set_nilai.id_kelas'=>$id_kelas,
				'set_nilai.id_mapel'=>$id_mapel,
				'set_nilai.id_aspek'=>'2'


			);

			$sn = $this->model_sn->listing($where);


		}else{

			$sn=array();

		}


		if(isset($_GET['sn']) && $_GET['sn']!=''){
			
			$id_sn=$_GET['sn'];

			
			//kondisi jika memilih uas atau uts
			if($id_sn!='uas' && $id_sn!='uts'){

				$where=array(

					'nilai.id_sn'=>$id_sn
				);
	
				$nilai=$this->model_nilai->listing($where);
	

			}else{
				
				
				//jika itu adalah uts maka tipe adalah 1
				if($this->input->get('sn')=='uts'){

					$tipe=1;

				}else{

					$tipe=2;

				}

				$where=array(


					'nilai_ujian.id_aspek'=>1,
					'nilai_ujian.id_ajaran'=>$this->input->get('ajaran'),
					'nilai_ujian.id_mapel'=>$this->input->get('mapel'),
					'nilai_ujian.id_kelas'=>$this->input->get('kelas'),
					'nilai_ujian.tipe_ujian'=>$tipe
				);
				
				
				$nilai=$this->model_nilai->listing_ujian($where);	
				
				
			}
			

		}else{

			$nilai=array();
		}

		

		$ajaran = $this->model_ajaran->listing();
		


		
		$data = array(	'title' 	=> 'Nilai Pengetahuan',
						'key' 		=> 'keterampilan',
						'id_kelas'  => $id_kelas,
						'id_mapel'  => $id_mapel,
						'id_sn'    	=> $id_sn,
						'id_ajaran' => $id_ajaran,
						'ajaran' 	=> $ajaran,
						'mapel' 	=> $mapel,
						'kelas' 	=> $kelas,
						'nilai' 	=> $nilai,
						'siswa' 	=> $siswa,
						'sn' 		=> $sn,
						'isi' 		=> 'siswa/nilai/nilai');
		$this->load->view('siswa/layout/wrapper', $data, FALSE);
	}


	

}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */