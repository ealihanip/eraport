<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller 
{

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_login');
	}

	public function index()
	{
			$data = array(	'title' => 'Login');
			$this->load->view('login_view', $data, FALSE);
			//check username dan password apakah cocok dengan database atao tidak
	}

	public function auth()
	{
	 	$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		//cek didatabse
		$check_admin = $this->model_login->login_admin($username, $password);
		//kalo ada record, maka redirect ke dasbor
		if (count($check_admin)==1) 
		{
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('id_admin', $check_admin->id_admin);
			$this->session->set_userdata('nama', $check_admin->nama);
			redirect(base_url('admin/dasbor'),'refresh');
		}
		
		else
		{
			$check_guru = $this->model_login->login_guru($username, $password);
			if (count($check_guru)>0) 
			{
			 	$this->session->set_userdata('username', $username);
				$this->session->set_userdata('id_guru', $check_guru->id_guru);
				$this->session->set_userdata('nama_guru', $check_guru->nama_guru);
				$this->session->set_userdata('akses_level',$check_guru->akses_level);
				$this->session->set_userdata('guru_mapel', $check_guru->mapel);
				redirect(base_url('guru/dasbor'),'refresh');	
			}
			else
			{
				$check_siswa = $this->model_login->login_siswa($username, $password);
				if (count($check_siswa)>0) 
				{
					$this->session->set_userdata('username', $username);
					$this->session->set_userdata('id_siswa', $check_siswa->id_siswa);
					$this->session->set_userdata('nama_siswa', $check_siswa->nama_siswa);
					redirect(base_url('siswa/dasbor'),'refresh');
				}
				else
				{
					//kalo username dan password tidak cocok tampilkan peringatan dan kembali ke login 
					$this->session->set_flashdata('sukses','Username dan Password Tidak Cocok');
					redirect(base_url('login'),'refresh');
				}
			}
		}
		//end cheking
	}
		

	//logout
	public function logout()
	{
		// $this->session->unset_userdata('username');
		// $this->session->unset_userdata('id_admin');
		// $this->session->unset_userdata('nama');
		$this->session->sess_destroy();
		$this->session->set_flashdata('sukses','and berhasil logout');
		redirect(base_url('login'),'refresh');
	}

}

/* End of file login.php */
/* Location: ./application/controllers/login.php */