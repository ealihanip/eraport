<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
	}

	//homepage
	public function index()
	{
		$data = array(	'title' => 'SMK Hidayah Semarang',
					 	'isi' 	=> 'home/list');
		$this->load->view('layout/wrapper', $data, FALSE);
		
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */