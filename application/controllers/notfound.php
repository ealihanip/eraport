<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notfound extends CI_Controller {

	public function index()
	{
		$data = array('title' =>'notfound',
							'n' => $this->output->set_status_header('404'),
							'isi' => 'v_nf');
		$this->load->view('layout/wrapper', $data);
	}

}

/* End of file notfound.php */
/* Location: ./application/controllers/notfound.php */