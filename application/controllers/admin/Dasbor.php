<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller {

	public function index()
	{
		$data = array(	'title' => 'Halaman Dasbor',
						'isi' 	=> 'admin/dasbor/list');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//profile
	public function profile()
	{
		$id_admin 	= $this->session->userdata('id_admin');//diambil dari session
		$user 		= $this->model_user->detail($id_admin);
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama','Nama','required',
			array(	'required' 	=>	'Nama harus diisi'));


		$valid->set_rules('email','Email','required|valid_email',
			array(	'required' 	=>	'Email harus diisi',
					'valid_email' => 'Format email salah'));
		
		if ($valid->run()===FALSE) 
		{
			//end validasi
			$data = array(	'title' => 'Update Data User :'.$user->nama,
							'user' 	=> $user,
							'isi' 	=> 'admin/dasbor/profile');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
		//gax ada eror, maka masuk database
		}
		else
		{
			$i= $this->input;
			//jika input password lebih dari 5 karakter
			if (strlen($i->post('password'))> 5) 
			{
				$data = array(	'id_admin' 	=> $id_admin,
								'nama' 		=> $i->post('nama'),
								'email' 	=> $i->post('email'),
								'username' 	=> $i->post('username'),
								'password' 	=> sha1($i->post('password'))
							);
			}
			else
			{
				$data = array(	'id_admin' 	=>$id_admin,
								'nama' 		=>$i->post('nama'),
								'email' 	=>$i->post('email'),
								'username' 	=>$i->post('username')
							);
			}
			//end if
			$this->model_user->edit($data);
			$this->session->set_flashdata('sukses', 'profile Berhasil Diupdate');
			redirect(base_url('admin/dasbor/profile'),'refresh');
		}
	}
}

/* End of file dasbor.php */
/* Location: ./application/controllers/admin/dasbor.php */