<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	//load model
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_user');
	}

	public function index()
	{
		$user = $this->model_user->listing();

		$data = array(	'title' => 'Data User('.count($user).')',
						'user' =>	$user,
						'isi' 	=> 'admin/user/list');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//tambah
	public function tambah()
	{
		$user = $this->model_user->listing();
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama','Nama','required',
			array(	'required' 	=>	'Nama harus diisi'));


		$valid->set_rules('email','Email','required|valid_email',
			array(	'required' 	=>	'Email harus diisi',
					'valid_email' => 'Format email salah'));

		$valid->set_rules('username','Username','required|is_unique[admin.username]',
			array(	'required' 	=>	'Username harus diisi',
					'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah ada. Buat User Baru'));

		$valid->set_rules('password','Password','required|min_length[5]',
			array(	'required' 		=>	'Password harus diisi',
					'min_length'	=> 'Password minimal 5 karakter'));
		
		if ($valid->run()) 
		{
			//kalau foto kosong
			if (!empty($_FILES['foto']['name'])) 
			{
				$config['upload_path']   = './assets/upload/image/';
				$config['allowed_types'] = 'gif|jpg|png|svg|jpeg';
				$config['max_size']      = '12000'; // KB  
				$this->upload->initialize($config);
				if (! $this->upload->do_upload('foto')) 
				{
					//end validasi

					$data = array(	'title' => 'Tambah Data User',
									'user' 	=> $user,
									'error'	=> $this->upload->display_errors(),
									'isi' 	=> 'admin/user/tambah');
					$this->load->view('admin/layout/wrapper', $data, FALSE);
					//gax ada eror, maka masuk database
				}
				else
				{
					//upload	
					$upload_data        		= array('uploads' =>$this->upload->data());
					// Image Editor
					$config['image_library']  	= 'gd2';
					$config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
					$config['new_image']     	= './assets/upload/image/thumbs/';
					$config['create_thumb']   	= TRUE;
					$config['quality']       	= "100%";
					$config['maintain_ratio']   = TRUE;
					$config['width']       		= 360; // Pixel
					$config['height']       	= 360; // Pixel
					$config['x_axis']       	= 0;
					$config['y_axis']       	= 0;
					$config['thumb_marker']   	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$i= $this->input;
					$data = array(	'nama' 		=>$i->post('nama'),
									'email' 	=>$i->post('email'),
									'username' 	=>$i->post('username'),
									'password' 	=>sha1($i->post('password')),
									'foto'		=> $upload_data['uploads']['file_name']
									);
					$this->model_user->tambah($data);
					$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
					redirect(base_url('admin/user'),'refresh');
				}
			}
			else
			{
				//skrip input tanpa foto
				$i= $this->input;
				$data = array(	'nama' 		=>$i->post('nama'),
								'email' 	=>$i->post('email'),
								'username' 	=>$i->post('username'),
								'password' 	=>sha1($i->post('password'))
						);
				$this->model_user->tambah($data);
				$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			}
		}
		//end masuk database
		$data = array(	'title' => 'Tambah Data User',
						'user' 	=> $user,
						'isi' 	=> 'admin/user/tambah');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//edit
	public function edit($id_admin)
	{
		$user = $this->model_user->detail($id_admin);
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama','Nama','required',
			array(	'required' 	=>	'Nama harus diisi'));


		$valid->set_rules('email','Email','required|valid_email',
			array(	'required' 	=>	'Email harus diisi',
					'valid_email' => 'Format email salah'));
		
		if ($valid->run()) 
		{
			if (!empty($_FILES['foto']['name'])) 
			{
				$config['upload_path']   = './assets/upload/image/';
				$config['allowed_types'] = 'gif|jpg|png|svg|jpeg';
				$config['max_size']      = '12000'; // KB  
				$this->upload->initialize($config);
				if (! $this->upload->do_upload('foto')) 
				{
					//end validasi

					$data = array(	'title' => 'Edit Data User :'.$user->nama,
									'user' 	=> $user,
									'isi' 	=> 'admin/user/edit');
					$this->load->view('admin/layout/wrapper', $data, FALSE);
					//gax ada eror, maka masuk database
				}
				else
				{
					//upload	
					$upload_data        		= array('uploads' =>$this->upload->data());
					// Image Editor
					$config['image_library']  	= 'gd2';
					$config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name']; 
					$config['new_image']     	= './assets/upload/image/thumbs/';
					$config['create_thumb']   	= TRUE;
					$config['quality']       	= "100%";
					$config['maintain_ratio']   = TRUE;
					$config['width']       		= 360; // Pixel
					$config['height']       	= 360; // Pixel
					$config['x_axis']       	= 0;
					$config['y_axis']       	= 0;
					$config['thumb_marker']   	= '';
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					//jika upload foto user, maka file foto lama harus dihapus
					if ($user->foto != "") 
					{
						unlink('./assets/upload/image/'.$user->foto);
						unlink('./assets/upload/image/thumbs/'.$user->foto);
					}
					//end upload cover baru
					$i = $this->input;
					$data = array(	'id_admin' 	=> $id_admin,
									'nama' 		=> $i->post('nama'),
									'email' 	=> $i->post('email'),
									'username' 	=> $i->post('username'),
									'password' 	=> sha1($i->post('password')),
									'foto'		=> $upload_data['uploads']['file_name']
							);
					$this->model_user->edit($data);
					$this->session->set_flashdata('sukses', 'Data Berhasil Diupdate');
					redirect(base_url('admin/user'),'refresh');
				}
			}
			else
			{
				//update tannpa foto
				$i = $this->input;
				$data = array(	'id_admin' 	=> $id_admin,
								'nama' 		=>$i->post('nama'),
								'email' 	=>$i->post('email'),
								'username' 	=>$i->post('username')
							);
				$this->model_user->edit($data);
				$this->session->set_flashdata('sukses', 'Data Berhasil Diupdate');
				redirect(base_url('admin/user'),'refresh');
			}
			
		}
		//end masuk database
		$data = array(	'title' => 'Edit Data User :'.$user->nama,
									'user' 	=> $user,
									'isi' 	=> 'admin/user/edit');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//delete
	public function delete($id_admin)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi
		$data = array('id_admin' => $id_admin);
		$this->model_user->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('admin/user'),'refresh');
	}

}

/* End of file user.php */
/* Location: ./application/controllers/admin/user.php */