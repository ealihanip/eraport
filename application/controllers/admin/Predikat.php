<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Predikat extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_predikat');
	}

	//listing mapel
	public function index()
	{
		$predikat = $this->model_predikat->get();

		$data = array(	'title' => 'Set Nilai Predikat',
						'predikat' => $predikat,
					 	'isi' 	=> 'admin/predikat/index');
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
		
    }

    public function update()
	{
		$predikat = $this->model_predikat->get();

        
        foreach ($predikat as $datapredikat){

            $batas_atas=$this->input->post('batas_atas');
            $batas_bawah=$this->input->post('batas_bawah');
            
            $batas_atas[$datapredikat->id];

            $data=array(

                'batas_bawah'=>$batas_bawah[$datapredikat->id],
                'batas_atas'=>$batas_atas[$datapredikat->id]
            );

            $where=array(

                'id'=>$datapredikat->id
            );

            $this->model_predikat->update($data,$where);

        }


        redirect(base_url('admin/predikat'),'refresh');
        
        
        
		
    }
    



	

}

/* End of file mapel.php */
/* Location: ./application/controllers/admin/mapel.php */