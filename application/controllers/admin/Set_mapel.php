<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_mapel extends CI_Controller 
{
		public function __construct()
	{
		parent::__construct();
		$this->load->model('model_setmapel');
		$this->load->model('model_ajaran');
		$this->load->model('model_guru');
		$this->load->model('model_kelas');
		$this->load->model('model_mapel');
	}

	public function index()
	{
		$sm = $this->model_setmapel->listing();
		$data = array(	'title' 	=> 'Set Mata Pelajaran',
						'setmapel' 	=> $sm,
					 	'isi' 		=> 'admin/mapel/set');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//tambah
	public function tambah()
	{
		$ajaran = $this->model_ajaran->listing();
		$guru 	= $this->model_guru->listing();
		$kelas 	= $this->model_kelas->listing();
		$mapel 	= $this->model_mapel->listing();

		$this->form_validation->set_rules('id_mapel', 'Mata Pelajaran', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' 	=> 'Tambah Data',
							'ajaran' 	=> $ajaran,
							'guru' 		=> $guru,
							'kelas' 	=> $kelas,
							'mapel' 	=> $mapel,
							'isi' 		=> 'admin/mapel/set_add');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'id_ajaran' =>$i->post('id_ajaran'),
							'id_guru' 	=>$i->post('id_guru'),
							'id_kelas' 	=>$i->post('id_kelas'),
							'id_mapel' 	=>$i->post('id_mapel'),
							'bobot' 	=>$i->post('bobot')
						);
			$this->model_setmapel->tambah($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			redirect(base_url('admin/set_mapel'),'refresh');
		}
		//and masuk database
	}

	//edit
	public function edit($id_gm)
	{
		$sm 	= $this->model_setmapel->detail($id_gm);
		$ajaran = $this->model_ajaran->listing();
		$guru 	= $this->model_guru->listing();
		$kelas 	= $this->model_kelas->listing();
		$mapel 	= $this->model_mapel->listing();

		$this->form_validation->set_rules('id_mapel', 'Mata Pelajaran', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' 	=>'Edit Data',
							'setmapel' 	=>$sm,
							'ajaran' 	=>$ajaran,
							'guru' 		=>$guru,
							'kelas'		=>$kelas,
							'mapel' 	=>$mapel,
							'isi' 		=>'admin/mapel/ubah');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'id_gm'		=> $id_gm,
							'id_ajaran' =>$i->post('id_ajaran'),
							'id_guru' 	=>$i->post('id_guru'),
							'id_kelas' 	=>$i->post('id_kelas'),
							'id_mapel' 	=>$i->post('id_mapel'),
							'bobot' 	=>$i->post('bobot')
						);
			$this->model_setmapel->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Diupdate');
			redirect(base_url('admin/set_mapel'),'refresh');
		}
		//and masuk database
	}

	//delete
	public function delete($id_gm)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		$data = array('id_gm' => $id_gm);
		$this->model_setmapel->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('admin/set_mapel'),'refresh');
	}

}

/* End of file set_mapel.php */
/* Location: ./application/controllers/admin/set_mapel.php */