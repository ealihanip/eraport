<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekskul extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_dataekskul');
		$this->load->model('model_ajaran');
		$this->load->model('model_guru');
	}

	public function index()
	{
		$eks = $this->model_dataekskul->listing();

		$data = array(	'title' => 'Data Ekstrakurikuler',
						'eks' 	=> $eks,
						'isi' 	=> 'admin/ekskul/list'
					 );
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	public function tambah()
	{
		$ajaran = $this->model_ajaran->listing();
		$guru 	= $this->model_guru->listing();
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_guru','ID Guru','required',
			array(	'required' 	=>	'Nama Guru harus diisi'));

		if ($valid->run()===FALSE) 
		{
			//end validasi

			$data = array(	'title' 	=> 'Tambah Data Ekskul',
							'ajaran' 	=> $ajaran,
							'guru' 		=> $guru,
							'isi' 		=> 'admin/ekskul/tambah');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		} 
		else
		{
			$i= $this->input;
			$data = array(	'id_ajaran' 	=>$i->post('id_ajaran'),
							'id_guru' 		=>$i->post('id_guru'),
							'nama_ekskul' 	=>$i->post('nama_ekskul'),
							'nama_ketua' 	=>$i->post('nama_ketua'),
							'no_hp' 		=>$i->post('no_hp'),
							'alamat' 		=>$i->post('alamat')
							);
			$this->model_dataekskul->tambah($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			redirect(base_url('admin/ekskul'),'refresh');
		}
	}

	public function edit($id)
	{
		$eks 	= $this->model_dataekskul->detail($id);
		$ajaran = $this->model_ajaran->listing();
		$guru 	= $this->model_guru->listing();
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_guru','ID Guru','required',
			array(	'required' 	=>	'Nama Guru harus diisi'));

		if ($valid->run()===FALSE) 
		{
			//end validasi

			$data = array(	'title' 	=> 'Edit Data Ekskul : '. $eks->nama_ekskul,
							'eks' 		=> $eks,
							'ajaran' 	=> $ajaran,
							'guru' 		=> $guru,
							'isi' 		=> 'admin/ekskul/edit');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		} 
		else
		{
			$i= $this->input;
			$data = array(	'id' 			=> $id,
							'id_ajaran' 	=>$i->post('id_ajaran'),
							'id_guru' 		=>$i->post('id_guru'),
							'nama_ekskul' 	=>$i->post('nama_ekskul'),
							'nama_ketua' 	=>$i->post('nama_ketua'),
							'no_hp' 		=>$i->post('no_hp'),
							'alamat' 		=>$i->post('alamat')
							);
			$this->model_dataekskul->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil DiUpdate');
			redirect(base_url('admin/ekskul'),'refresh');
		}
	}

	//delete
	public function delete($id)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		$data = array('id' => $id);
		$this->model_dataekskul->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('admin/ekskul'),'refresh');
	}

}

/* End of file ekskul.php */
/* Location: ./application/controllers/admin/ekskul.php */