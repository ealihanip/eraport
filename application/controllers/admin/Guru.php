<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_guru');
	}

	public function index()
	{
		$guru = $this->model_guru->listing();

		$data = array(	'title' => 'Data Guru',
						'guru' 	=> $guru,
						'isi' 	=> 'admin/guru/list');
		$this->load->view('admin/layout/wrapper', $data, FALSE);	
	}

	//tambah
	public function tambah() 
	{
		$guru = $this->model_guru->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama_guru','Nama','required',
			array(	'required' 	=>	'Nama Guru harus diisi'));


		$valid->set_rules('username','Username','required|is_unique[guru.username]',
			array(	'required' 	=>	'Username harus diisi',
					'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah ada. Buat Guru Baru'));

		$valid->set_rules('password','Password','required|min_length[5]',
			array(	'required' 		=>	'Password harus diisi',
					'min_length'	=> 'Password minimal 5 karakter'));
		
		if ($valid->run()===FALSE) 
		{
			//end validasi

			$data = array(	'title' => 'Tambah Data Guru',
							'guru' 	=> $guru,
							'isi' 	=> 'admin/guru/tambah');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		} 
		else
		{
			$i= $this->input;
			$data = array(	'nama_guru' 		=>$i->post('nama_guru'),
							'nip' 				=>$i->post('nip'),
							'jk' 				=>$i->post('jk'),
							'tempat_lahir' 		=>$i->post('tempat_lahir'),
							'tgl_lahir' 		=>$i->post('tgl_lahir'),
							'agama' 			=>$i->post('agama'),
							'alamat' 			=>$i->post('alamat'),
							'rt' 				=>$i->post('rt'),
							'rw' 				=>$i->post('rw'),
							'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
							'kecamatan' 		=>$i->post('kecamatan'),
							'kabupaten' 		=>$i->post('kabupaten'),
							'kode_pos' 			=>$i->post('kode_pos'),
							'no_hp' 			=>$i->post('no_hp'),
							'email' 			=>$i->post('email'),
							'username' 			=>$i->post('username'),
							'password' 			=>sha1($i->post('password')),
							'akses_level' 		=>$i->post('akses_level')
							);
			$this->model_guru->tambah($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			redirect(base_url('admin/guru'),'refresh');
		}
		//end masuk database
		
	}

	//edit
	public function edit($id_guru)
	{
		$guru = $this->model_guru->detail($id_guru);
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama_guru','Nama','required',
			array(	'required' 	=>	'Nama harus diisi'));

		
		if ($valid->run()===FALSE) {
			//end validasi

		$data = array(	'title' => 'Edit Data Guru :'.$guru->nama_guru,
						'guru' 	=> $guru,
						'isi' 	=> 'admin/guru/edit');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		//gax ada eror, maka masuk database
		}else{
			$i= $this->input;

			//jika input password lebih dari 5 karakter
			if (strlen($i->post('password'))> 5) {
				$data = array(	'id_guru' 			=> $id_guru,
								'nama_guru' 		=>$i->post('nama_guru'),
								'nip' 				=>$i->post('nip'),
								'jk' 				=>$i->post('jk'),
								'tempat_lahir' 		=>$i->post('tempat_lahir'),
								'tgl_lahir' 		=>$i->post('tgl_lahir'),
								'agama' 			=>$i->post('agama'),
								'alamat' 			=>$i->post('alamat'),
								'rt' 				=>$i->post('rt'),
								'rw' 				=>$i->post('rw'),
								'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
								'kecamatan' 		=>$i->post('kecamatan'),
								'kabupaten' 		=>$i->post('kabupaten'),
								'kode_pos' 			=>$i->post('kode_pos'),
								'no_hp' 			=>$i->post('no_hp'),
								'email' 			=>$i->post('email'),
								'username' 			=>$i->post('username'),
								'password' 			=>sha1($i->post('password')),
								'akses_level' 		=>$i->post('akses_level')
							);
			}else{
				$data = array(	'id_guru' 			=> $id_guru,
								'nama_guru' 		=>$i->post('nama_guru'),
								'nip' 				=>$i->post('nip'),
								'jk' 				=>$i->post('jk'),
								'tempat_lahir' 		=>$i->post('tempat_lahir'),
								'tgl_lahir' 		=>$i->post('tgl_lahir'),
								'agama' 			=>$i->post('agama'),
								'alamat' 			=>$i->post('alamat'),
								'rt' 				=>$i->post('rt'),
								'rw' 				=>$i->post('rw'),
								'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
								'kecamatan' 		=>$i->post('kecamatan'),
								'kabupaten' 		=>$i->post('kabupaten'),
								'kode_pos' 			=>$i->post('kode_pos'),
								'no_hp' 			=>$i->post('no_hp'),
								'email' 			=>$i->post('email'),
								'username' 			=>$i->post('username'),
								'akses_level' 		=>$i->post('akses_level')

							);

			}
			//end if
			$this->model_guru->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Diupdate');
			redirect(base_url('admin/guru'),'refresh');
		}
		//end masuk database
	}

	//delete
	public function delete($id_guru)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		//delete foto
		$guru = $this->model_guru->detail($id_guru);
		if ($guru->foto != "") {
			unlink('./assets/upload/image/'.$guru->foto);
			unlink('./assets/upload/image/thumbs/'.$guru->foto);
		}
		//end delete gambar

		$data = array('id_guru' => $id_guru);
		$this->model_guru->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('admin/guru'),'refresh');
	}

}

/* End of file guru.php */
/* Location: ./application/controllers/guru/guru.php */