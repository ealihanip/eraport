<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_kelas extends CI_Controller 
{
		public function __construct()
	{
		parent::__construct();
		$this->load->model('model_setkelas');
		$this->load->model('model_ajaran');
		$this->load->model('model_kelas');
		$this->load->model('model_siswa');
	}

	public function index()
	{
		$sk = $this->model_setkelas->listing();
		$data = array(	'title' => 'Set Kelas',
						'sk' 	=> $sk,
					 	'isi' 	=> 'admin/kelas/set');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	//tambah
	public function tambah()
	{
		$ajaran = $this->model_ajaran->listing();
		$kelas 	= $this->model_kelas->listing();
		$siswa 	= $this->model_siswa->listing();

		$this->form_validation->set_rules('id_kelas', 'Kelas', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' 	=> 'Tambah Data',
							'ajaran' 	=> $ajaran,
							'kelas' 	=> $kelas,
							'siswa' 	=> $siswa,
							'isi' 		=> 'admin/kelas/set_add');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'id_ajaran' =>$i->post('id_ajaran'),
							'id_kelas' 	=>$i->post('id_kelas'),
							'id_siswa' 	=>$i->post('id_siswa')
						);
			$this->model_setkelas->tambah($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			redirect(base_url('admin/set_kelas'),'refresh');
		}
		//and masuk database
	}

	//edit
	public function edit($id)
	{
		$sk 	= $this->model_setkelas->detail($id);
		$ajaran = $this->model_ajaran->listing();
		$kelas 	= $this->model_kelas->listing();
		$siswa 	= $this->model_siswa->listing();

		$this->form_validation->set_rules('id_ajaran', 'Tahun Ajaran', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' 	=>'Edit Data',
							'sk' 		=>$sk,
							'ajaran' 	=>$ajaran,
							'kelas'		=>$kelas,
							'siswa' 	=>$siswa,
							'isi' 		=>'admin/kelas/set_edit');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'id'		=> $id,
							'id_ajaran' =>$i->post('id_ajaran'),
							'id_kelas' 	=>$i->post('id_kelas'),
							'id_siswa' 	=>$i->post('id_siswa')
						);
			$this->model_setkelas->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Diedit');
			redirect(base_url('admin/set_kelas'),'refresh');
		}
		//and masuk database
	}

}

/* End of file set_mapel.php */
/* Location: ./application/controllers/admin/set_mapel.php */