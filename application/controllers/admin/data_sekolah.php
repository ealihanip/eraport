<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_sekolah extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_data_sekolah');
	}

	//listing mapel
	public function index()
	{
		$data_sekolah = $this->model_data_sekolah->get();

		$data = array(	'title' => 'Data Sekolah',
						'data_sekolah' => $data_sekolah,
					 	'isi' 	=> 'admin/data_sekolah/index');
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
		
    }

    
    public function update($id)
	{

		
        $where=array(

            'id'=>$id

        );
        
        
        
        //cek tidak input logo
        if (empty($_FILES['logo']['name']))
        {
            
            //array ke model insert
            $update=array(

                'npsn'=>$this->input->post('npsn'),
                'nama'=>$this->input->post('nama'),
                'alamat'=>$this->input->post('alamat'),
                'desa_kelurahan'=>$this->input->post('desa_kelurahan'),
                'kecamatan'=>$this->input->post('kecamatan'),
                'kabupaten'=>$this->input->post('kabupaten'),
                'provinsi'=>$this->input->post('provinsi'),
                'kode_pos'=>$this->input->post('kode_pos'),
                'telepon'=>$this->input->post('telepon'),
                'fax'=>$this->input->post('fax'),
                'email'=>$this->input->post('email'),
                'website'=>$this->input->post('website')
                

            );
            
            $update=$this->model_data_sekolah->update($update,$where);

        }else{
            
            $file_name='logo';

            $config['upload_path']= 'assets/upload/logo/';
            $config['allowed_types']= 'gif|jpg|png';
            $config['file_name']= $file_name;
            $config['overwrite']= true;
            
            // // $config['max_size']             = 100;
            // $config['max_width']            = 1024;
            // $config['max_height']           = 768;
            $this->load->library('upload');
            $this->upload->initialize($config);
            
            
            //proses upload
            if ( ! $this->upload->do_upload('logo'))
            {
                
                
                //array ke model insert
                $update=array(

                    'npsn'=>$this->input->post('npsn'),
                    'nama'=>$this->input->post('nama'),
                    'alamat'=>$this->input->post('alamat'),
                    'desa_kelurahan'=>$this->input->post('desa_kelurahan'),
                    'kecamatan'=>$this->input->post('kecamatan'),
                    'kabupaten'=>$this->input->post('kabupaten'),
                    'provinsi'=>$this->input->post('provinsi'),
                    'kode_pos'=>$this->input->post('kode_pos'),
                    'telepon'=>$this->input->post('telepon'),
                    'fax'=>$this->input->post('fax'),
                    'email'=>$this->input->post('email'),
                    'website'=>$this->input->post('website')
                    

                );
                

                $update=$this->model_data_sekolah->update($update,$where);
                    
            }
            else
            {
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = 'assets/upload/logo/'.$this->upload->data('file_name');
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = false;
                $config['width']         = 400;
                $config['height']       = 400;

                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                $this->image_lib->resize();

            
                
                    //array ke model insert
                $update=array(

                    'npsn'=>$this->input->post('npsn'),
                    'nama'=>$this->input->post('nama'),
                    'alamat'=>$this->input->post('alamat'),
                    'desa_kelurahan'=>$this->input->post('desa_kelurahan'),
                    'kecamatan'=>$this->input->post('kecamatan'),
                    'kabupaten'=>$this->input->post('kabupaten'),
                    'provinsi'=>$this->input->post('provinsi'),
                    'kode_pos'=>$this->input->post('kode_pos'),
                    'telepon'=>$this->input->post('telepon'),
                    'fax'=>$this->input->post('fax'),
                    'email'=>$this->input->post('email'),
                    'website'=>$this->input->post('website'),
                    'logo_sekolah'=>$this->upload->data('file_name')
                    

                );
                

                
                $update=$this->model_data_sekolah->update($update,$where);

            }


        }
			
		redirect(base_url('admin/data_sekolah'),'refresh');	
	}
    



	

}

/* End of file mapel.php */
/* Location: ./application/controllers/admin/mapel.php */