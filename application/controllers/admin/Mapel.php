<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapel extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_mapel');
		$this->load->model('model_golongan');
	}

	//listing mapel
	public function index()
	{
		$mapel = $this->model_mapel->listing();

		$data = array(	'title' => 'Data Mata Pelajaran',
						'mapel' => $mapel,
					 	'isi' 	=> 'admin/mapel/list');
		$this->load->view('admin/layout/wrapper', $data, FALSE);
		
	}

	//tambah
	public function tambah()
	{
		$mapel = $this->model_mapel->listing();
		$golongan = $this->model_golongan->get();

		$this->form_validation->set_rules('nama_mapel', 'Mata Pelajaran', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' => 'Tambah Data Mata Pelajaran',
							'golongan' =>$golongan ,
							'isi' 	=> 'admin/mapel/tambah');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'kode_mapel' =>$i->post('kode_mapel'),
							'id_golongan' =>$i->post('id_golongan'),
							'nama_mapel' =>$i->post('nama_mapel')
						);
			$this->model_mapel->tambah($data);
			redirect(base_url('admin/mapel'),'refresh');
		}
		//and masuk database
	}

	//edit
	public function edit($id_mapel)
	{
		$mapel = $this->model_mapel->detail($id_mapel);
		$golongan = $this->model_golongan->get();
		$this->form_validation->set_rules('nama_mapel', 'Mata Pelajaran', 'required');
		if ($this->form_validation->run()=== FALSE) 
		{
			# end validasi

			$data = array(	'title' => 'Edit Data Mata Pelajaran',
							'mapel' => $mapel,
							'golongan' =>$golongan ,
							'isi' 	=> 'admin/mapel/edit');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//masuk database
		}
		else
		{
			$i= $this->input;
			$data = array(	'id_mapel'		=> $id_mapel,
							'kode_mapel'	=>$i->post('kode_mapel'),
							'id_golongan' =>$i->post('id_golongan'),
							'nama_mapel'	=>$i->post('nama_mapel')
						);
			$this->model_mapel->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Diedit');
			redirect(base_url('admin/mapel'),'refresh');
		}
		//and masuk database
	}

	//delete
	public function delete($id_mapel)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		$data = array('id_mapel' => $id_mapel);
		$this->model_mapel->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('admin/mapel'),'refresh');
	}

}

/* End of file mapel.php */
/* Location: ./application/controllers/admin/mapel.php */