<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Load library phpspreadsheet
// require('./spreadsheet/vendor/autoload.php');

// use PhpOffice\PhpSpreadsheet\Helper\Sample;
// use PhpOffice\PhpSpreadsheet\IOFactory;
// use PhpOffice\PhpSpreadsheet\Spreadsheet;
// End load library phpspreadsheet

class Laporan extends CI_Controller 
{
	// Load database
 	public function __construct() 
 	{
 		parent::__construct();
 		$this->load->model('model_guru');
        $this->load->model('model_siswa');
        $this->load->model('model_kelas');
        $this->load->model('model_keahlian');
        $this->load->model('model_ajaran');
        $this->load->model('model_semester');


        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
        


 	}

	// Main page
public function index()
{

    $id_kelas='';

    if(isset($_GET['kelas']) && !empty($_GET['kelas'])){

        $id_kelas=$_GET['kelas'];

    }


    
    $kelas= $this->model_kelas->listing();
	$guru = $this->model_guru->listing();
    $siswa = $this->model_siswa->listing();
    $data = array( 'title' => 'Laporan',
                    'kelas' => $kelas,
					'guru' => $guru,
                    'siswa' => $siswa,
                    'id_kelas' => $id_kelas,
					'isi' => 'admin/laporan/v_laporan');
	$this->load->view('admin/layout/wrapper', $data, FALSE);
}

public function data_guru()
{
	$guru = $this->model_guru->listing();
	$data = array( 'title' => 'Data Guru',
					'guru' => $guru,
					'isi' => 'admin/laporan/laporan_guru');
	$this->load->view('admin/layout/wrapper', $data, FALSE);
}

public function data_siswa()
{
    $siswa = $this->model_siswa->listing_laporan();
    $data = array( 'title' => 'Data Siswa',
                    'siswa' => $siswa,
                    'isi' => 'admin/laporan/laporan_siswa');
    $this->load->view('admin/layout/wrapper', $data, FALSE);
}

//menampilkan daftar anggota kelas
    public function per_kelas()
    {
        $kelas = $this->model_kelas->listing();

        $data = array(  'title'     => '',
                        'kelas'     => $kelas,
                        'isi'       => 'admin/laporan/per_kelas' 
                    );
        $this->load->view('admin/layout/wrapper', $data, FALSE);
    }

//menampilkan daftar anggota kelas
    public function anggota_kelas($id_kelas)
    {

        $where=array(

            'kelas.id_kelas'=>$id_kelas
        );

        $detail_kelas = $this->model_kelas->listing($where);
        
        $kelas = $this->model_kelas->walikelas($id_kelas);

        
        $keahlian = $this->model_keahlian->kompetensi_keahlian();

        $data = array(  'title'     => '',
                        'kelas'     => $kelas,
                        'detail_kelas'  => $detail_kelas,
                        'keahlian'  => $keahlian,
                        'semester_aktif'  => $this->semester_aktif,
                        'isi'       => 'admin/laporan/siswa_per_kelas' 
                    );
        $this->load->view('admin/layout/wrapper', $data, FALSE);
    }

// Export ke pdf
public function export()
{
	
	$pdf = new FPDF('p','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(190,7,' DAFTAR GURU SMK HIDAYAH SEMARANG',0,1,'C');
    $pdf->SetFont('Arial','B',12);
   $pdf->Cell(190,7,'TAHUN PELAJARAN 2017/2018',0,1,'C');
    // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);
    $pdf->SetFont('Arial','B',10);
    $pdf->Cell(20,6,'NIP',1,0);
    $pdf->Cell(60,6,'NAMA GURU',1,0);
    $pdf->Cell(25,6,'NO HP',1,0);
    $pdf->Cell(80,6,'ALAMAT',1,1);
    $pdf->SetFont('Arial','',10);
    $guru = $this->model_guru->listing();
    foreach ($guru as $guru)
    {
        $pdf->Cell(20,6,$guru->nip,1,0);
        $pdf->Cell(60,6,$guru->nama_guru,1,0);
        $pdf->Cell(25,6,$guru->no_hp,1,0);
        $pdf->Cell(80,6,$guru->alamat,1,1); 
    }
    $pdf->Output('Data Guru.pdf','i');
	}

    // Export ke pdf
public function export_siswa()
{
    
    $pdf = new FPDF('p','mm','A4');
    // membuat halaman baru
    $pdf->AddPage();
    // setting jenis font yang akan digunakan
    $pdf->SetFont('Arial','B',16);
    // mencetak string 
    $pdf->Cell(190,7,'DAFTAR SISWA SMK HIDAYAH SEMARANG',0,1,'C');
    $pdf->SetFont('Arial','B',16);
    $pdf->Cell(190,7,'TAHUN PELAJARAN 2017/2018',0,1,'C');
     // Memberikan space kebawah agar tidak terlalu rapat
    $pdf->Cell(10,7,'',0,1);
    // menentukan font size untuk tabel
     $pdf->SetFont('Arial','B',10);
     // set tabel
    $pdf->Cell(10,6,'NO',1,0);
    $pdf->Cell(20,6,'NIS',1,0);
    $pdf->Cell(60,6,'NAMA',1,0);
    $pdf->Cell(15,6,'KELAS',1,0);
    $pdf->Cell(80,6,'ALAMAT',1,1);
    // set font untuk isi tabel
    $pdf->SetFont('Arial','',10);
    $siswa = $this->model_siswa->listing_laporan();
    $no=1;
    foreach ($siswa as $siswa)
    {
        $pdf->Cell(10,6,$no,1,0);
        $pdf->Cell(20,6,$siswa->no_induk,1,0);
        $pdf->Cell(60,6,$siswa->nama_siswa,1,0);
        $pdf->Cell(15,6,$siswa->nama_jurusan,1,0);
        $pdf->Cell(80,6,$siswa->alamat,1,1);
         $no++;

    }
  
    $pdf->Output('Data Siswa.pdf','i');
    }

    public function export_siswa_per_kelas($id_kelas)
    {

        $where=array(

            'kelas.id_kelas'=>$id_kelas
        );
        
        $kelas = $this->model_kelas->listing($where);

        $where=array(

            'kelas.id_kelas'=>$id_kelas
        );

        $siswa = $this->model_siswa->listing($where);

        $mpdf = new \Mpdf\Mpdf(['format' => 'Letter','margin_left'=>'0','margin_right'=>'0','margin_top'=>'0','margin_bottom'=>'0']);
        
        
        $data['siswa']=$siswa;
        $data['kelas']=$kelas;  
        $html=$this->load->view('laporan/siswaperkelas',$data,true);

       

        $mpdf->WriteHTML($html);
        $mpdf->output('print.pdf', 'I');
    }

}

/* End of file laporan.php */
/* Location: ./application/controllers/admin/laporan.php */