<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahun_dan_semester extends CI_Controller {
    
    
   

    
    public function __construct()
	{
		parent::__construct();
        $this->load->model('model_semester');
        $this->load->model('model_ajaran');
        
    }
    
    

	//listing mapel
	public function index()
	{
        

       

        $ajaran = $this->model_ajaran->get()->result();


      
        
        $where=array(

            'aktif'=>true
        );


        $semester = $this->model_semester->get($where)->row();

        $data = array(	'title' => 'Set Nilai Tahun Ajaran Dan Semester',
                        'semester' => $semester,
                        'ajaran' => $ajaran,
					 	'isi' 	=> 'admin/tahun_dan_semester/index');
        $this->load->view('admin/layout/wrapper', $data, FALSE);
        
		
    }

    public function update()
	{
        
        if($this->input->post('semester')==1){


            $where=array(

                'id_semester'=>1
            );

            $data=array(

                'aktif'=>true
    
            );
    
            
            $this->model_semester->update($data,$where);


            $where=array(

                'id_semester'=>2
            );

            $data=array(

                'aktif'=>false
    
            );

            $this->model_semester->update($data,$where);

        }else{

           

            $where=array(

                'id_semester'=>2
            );

            $data=array(

                'aktif'=>true
    
            );
    
            
            $this->model_semester->update($data,$where);


            $where=array(

                'id_semester'=>1
            );

            $data=array(

                'aktif'=>false
    
            );

            $this->model_semester->update($data,$where);
            

        }



        $ajaran = $this->model_ajaran->get()->result();

        foreach($ajaran as $ajaran){

            $where=array(

                'id_ajaran'=>$ajaran->id_ajaran
            );

            $data=array(

                'aktif'=>false

            );

            $this->model_ajaran->update($data,$where);

        }

        $where=array(

            'id_ajaran'=>$this->input->post('ajaran')
        );
        
        $data=array(

            'aktif'=>true

        );

        $this->model_ajaran->update($data,$where);



        
        
        
        redirect(base_url('admin/tahun_dan_semester'),'refresh');
        
		
    }
    



	

}

/* End of file mapel.php */
/* Location: ./application/controllers/admin/mapel.php */