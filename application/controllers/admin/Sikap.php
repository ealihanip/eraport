<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sikap extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_sikap');
		$this->load->model('model_ajaran');
	}

	public function index()
	{
		$sikap = $this->model_sikap->listing();

		$data = array(	'title' => 'Data Sikap',
						'sikap' => $sikap,
						'isi' 	=> 'admin/sikap/list'
						 );
		$this->load->view('admin/layout/wrapper', $data, FALSE);
	}

	public function tambah()
	{
		$ajaran = $this->model_ajaran->listing();

		$valid = $this->form_validation;

		$valid->set_rules('butiran_sikap','Butiran Sikap','required',
			array(	'required' 	=>	'Nama Guru harus diisi'));

		if ($valid->run()===FALSE) 
		{
			//end validasi

			$data = array(	'title' 	=> 'Tambah Data Sikap',
							'ajaran' 	=> $ajaran,
							'isi' 		=> 'admin/sikap/tambah');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		} 
		else
		{
			$i= $this->input;
			$data = array(	'id_ajaran' 	=>$i->post('id_ajaran'),
							'butiran_sikap' =>$i->post('butiran_sikap')
							);
			$this->model_sikap->tambah($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
			redirect(base_url('admin/sikap'),'refresh');
		}
	}

	public function edit($id)
	{
		$sikap = $this->model_sikap->detail($id);
		$ajaran = $this->model_ajaran->listing();

		$valid = $this->form_validation;

		$valid->set_rules('butiran_sikap','Butiran Sikap','required',
			array(	'required' 	=>	'Nama Guru harus diisi'));

		if ($valid->run()===FALSE) 
		{
			//end validasi

			$data = array(	'title' 	=> 'Edit Data Sikap',
							'sikap' 	=> $sikap,
							'ajaran' 	=> $ajaran,
							'isi' 		=> 'admin/sikap/edit');
			$this->load->view('admin/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		} 
		else
		{
			$i= $this->input;
			$data = array(	'id' 			=>$id,
							'id_ajaran' 	=>$i->post('id_ajaran'),
							'butiran_sikap' =>$i->post('butiran_sikap')
							);
			$this->model_sikap->edit($data);
			$this->session->set_flashdata('sukses', 'Data Berhasil DiUpdate');
			redirect(base_url('admin/sikap'),'refresh');
		}
	}

}

/* End of file sikap.php */
/* Location: ./application/controllers/admin/sikap.php */