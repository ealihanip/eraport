<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai_ekskul extends CI_Controller
{
    
    private $semester_aktif='';
    
    public function __construct()
	{
		parent::__construct();
		
        $this->load->model('model_siswa');
        $this->load->model('model_ekskul');
        $this->load->model('model_ajaran');
        
        $this->load->model('model_semester');
        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();

	}

	public function index(){

		
		$id_ekskul='';

		if(isset($_GET['id_ekskul']) && !empty($_GET['id_ekskul'])){

			$id_ekskul=$_GET['id_ekskul'];

		}
		
		$dataekskul=$this->model_ekskul->get_data_ekskul()->result();
		
		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);


        $siswa=$this->model_siswa->listing($where);
        

        //cek dulu data ada apa tidak
        $where=array(

            'nilai_ekskul.id_semester'=>$this->semester_aktif,
            'nilai_ekskul.id_ajaran'=>$this->ajaran_aktif,
			'kelas.id_guru'=>$this->session->id_guru,
			
			'nilai_ekskul.id_ekskul'=>$id_ekskul
        );


        $ekskul=$this->model_ekskul->listing($where);
            
        
        
		

		$data = array(	'title' => 'Catatan Wali Kelas',
						
                        'siswa'=> $siswa,
                        'ekskul'=> $ekskul,
						'dataekskul'=>$dataekskul,
						'id_ekskul'=>$id_ekskul,
						'isi' 	=> 'guru/ekskul/index');
        $this->load->view('guru/layout/wrapper', $data, FALSE);	
        
    }
    

    public function store(){

        $id_ajaran=$this->ajaran_aktif;
		
		$id_ekskul='';

		if(isset($_GET['id_ekskul']) && !empty($_GET['id_ekskul'])){

			$id_ekskul=$_GET['id_ekskul'];

		}

        $where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);

        $siswa=$this->model_siswa->listing($where);

        

        //cek dulu data ada apa tidak
        $where=array(
            'nilai_ekskul.id_semester'=>$this->semester_aktif,
            'nilai_ekskul.id_ajaran'=>$this->ajaran_aktif,
			'kelas.id_guru'=>$this->session->id_guru,
			'nilai_ekskul.id_ekskul'=>$id_ekskul
        );

        $ekskul=$this->model_ekskul->listing($where);

		

        // jika data telah ada maka hanya di update jika tidak ada lakukan input
        if(count($ekskul)!=0){

           // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){


                $id_ekskul_pref=$this->input->post('id_ekskul');
               
                $predikat=$this->input->post('predikat');
				$deskripsi=$this->input->post('deskripsi');

                
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_siswa'=>$data_siswa->id_siswa,
					'id_kelas'=>$data_siswa->id_kelas,
					'id_ekskul'=>$id_ekskul,
					'predikat'=>$predikat[$data_siswa->id_siswa],
					'deskripsi'=>$deskripsi[$data_siswa->id_siswa]
                );


                $where=array(

                    'id'=>$id_ekskul_pref[$data_siswa->id_siswa]
                );

                $this->model_ekskul->edit($data,$where);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Update');
            redirect(base_url('guru/nilai_ekskul/index?id_ekskul='.$id_ekskul),'refresh');

        }else{


            // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){



				$predikat=$this->input->post('predikat');
				$deskripsi=$this->input->post('deskripsi');

                
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_siswa'=>$data_siswa->id_siswa,
					'id_kelas'=>$data_siswa->id_kelas,
					'id_ekskul'=>$id_ekskul,
					'predikat'=>$predikat[$data_siswa->id_siswa],
					'deskripsi'=>$deskripsi[$data_siswa->id_siswa]
                );

                $this->model_ekskul->tambah($data);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
            redirect(base_url('guru/nilai_ekskul/index?id_ekskul='.$id_ekskul),'refresh');
            
        }



        
       

    }

	
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */