<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_catatan');
		$this->load->model('model_ajaran');
		$this->load->model('model_kelas');
		$this->load->model('model_siswa');
	}

	public function index()
	{
		$catatan = $this->model_catatan->listing();

		$data = array(	'title' 	=> 'Catatan Wali Kelas',
						'catatan' 	=> $catatan,
						'isi' 		=> 'guru/laporan/list' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);
	}

	public function tambah()
	{	

		// asumsi
		$id_kelas='';


		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$kelas 	= $this->model_kelas->listing($where);
		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			$where=array(

				'siswa.id_kelas'=>$id_kelas
				
			);

			
			$siswa 	= $this->model_siswa->listing($where);


		}else{

		
			$siswa=array();

		}

		// end get siswa sesuai kelas not jquery :D

		$ajaran = $this->model_ajaran->listing();
		
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_kelas', ' Kelas & Jurusan', 'required',
			array('required' => ' Kelas & jurusan Harus dipilih'));

		if ($valid->run()===FALSE) 
		{

		$data = array(	'title' 	=> 'Tambah Catatan Wali Kelas',
						'id_kelas'	=> $id_kelas,
						'ajaran' 	=> $ajaran,
						'kelas' 	=> $kelas,
						'siswa' 	=> $siswa,
						'isi' 		=> 'guru/laporan/tambah' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_ajaran' => $i->post('id_ajaran'),
							'id_kelas' 	=> $i->post('id_kelas'),
							'id_siswa' 	=> $i->post('id_siswa'),
							'catatan' 	=> $i->post('catatan')
						);
			$this->model_catatan->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/laporan'),'refresh');
		}
	}

}

/* End of file laporan.php */
/* Location: ./application/controllers/guru/laporan.php */