<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Catatan_wali_kelas extends CI_Controller
{
    
    private $semester_aktif='';
    
    public function __construct()
	{
		parent::__construct();
		
        $this->load->model('model_siswa');
        $this->load->model('model_catatan');
        $this->load->model('model_ajaran');
        
        $this->load->model('model_semester');
        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();

	}

	public function index(){

		
		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);



        $siswa=$this->model_siswa->listing($where);
        

        //cek dulu data ada apa tidak
        $where=array(

            'catatan_wk.id_semester'=>$this->semester_aktif,
            'catatan_wk.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );


        $catatan=$this->model_catatan->listing($where);
            
        
        
		

		$data = array(	'title' => 'Catatan Wali Kelas',
						
                        'siswa'=> $siswa,
                        'catatan'=> $catatan,
						
						'isi' 	=> 'guru/catatan_wali_kelas/index');
        $this->load->view('guru/layout/wrapper', $data, FALSE);	
        
    }
    

    public function store(){

        $id_ajaran=$this->ajaran_aktif;
        
        $where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);

        $siswa=$this->model_siswa->listing($where);

        
        $catatan=$this->input->post('catatan');

        

        //cek dulu data ada apa tidak
        $where=array(
            'catatan_wk.id_semester'=>$this->semester_aktif,
            'catatan_wk.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );

        $catatan=$this->model_catatan->listing($where);



        // jika data telah ada maka hanya di update jika tidak ada lakukan input
        if(count($catatan)!=0){

           // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){


                $id_catatan=$this->input->post('id_catatan');
                $catatan=$this->input->post('catatan');

               
                $data=array(
                    
                    'id_semester'=>$this->semester_aktif,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'catatan'=>$catatan[$data_siswa->id_siswa]
                );

                $where=array(

                    'id'=>$id_catatan[$data_siswa->id_siswa]
                );

                $this->model_catatan->edit($data,$where);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Update');
            redirect(base_url('guru/catatan_wali_kelas/index?'),'refresh');

        }else{


            // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){



                $catatan=$this->input->post('catatan');

                
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'catatan'=>$catatan[$data_siswa->id_siswa]
                );

                $this->model_catatan->tambah($data);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
            redirect(base_url('guru/catatan_wali_kelas/index?'),'refresh');
            
        }



        
       

    }

	
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */