<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_kd extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_kd');
		$this->load->model('model_mapel');
		$this->load->model('model_kelas');
		$this->load->model('model_aspek');
	}

	public function index()
	{

		$where=array(

			'guru_mapel.id_guru'=>$this->session->id_guru
			
		);
		$kd = $this->model_kd->listing($where);

		$data = array(	'title' => 'Kompetensi Dasar',
						'kd' 	=> $kd,
						'isi' 	=> 'guru/data_kd/list');
		$this->load->view('guru/layout/wrapper', $data, FALSE);	
	}

	public function tambah()
	{

		// asumsi
		$id_kelas='';


		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$mapel = $this->model_mapel->listing($where);

		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			
			$where=array(
				'guru.id_guru'=>$this->session->id_guru,
				'kelas.id_kelas'=>$id_kelas
				
			);

			$mapel = $this->model_mapel->listing($where);


		}else{

		
			

		}

		
		
		$aspek = $this->model_aspek->listing();

		
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_kelas', ' Kelas & Jurusan', 'required',
			array('required' => ' Kelas & jurusan Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			//end validasi
			$data = array(	'title' => 'Tambah Data Kompetensi Dasar',
							'id_kelas'=>$id_kelas,
							'mapel' => $mapel,
							'aspek' => $aspek,
							'isi' 	=> 'guru/data_kd/tambah' );
			$this->load->view('guru/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_kelas' 	=> $i->post('id_kelas'),
							'id_mapel' 	=> $i->post('id_mapel'),
							'id_aspek' 	=> $i->post('id_aspek'), 
							'kode_kd' 	=> $i->post('kode_kd'),
							'isi_kd' 	=> $i->post('isi_kd')
						);
			$this->model_kd->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/data_kd'),'refresh');
		}
	}

	public function perkd($id_mapel)
	{

		$kd = $this->model_kd->listing1($id_mapel);
		$data = array(	'title' => 'Kompetensi Dasar',
						'kd' 	=> $kd,
						'isi' 	=> 'guru/data_kd/list');
		$this->load->view('guru/layout/wrapper', $data, FALSE);	
	}

	public function edit($id_kd)
	{
		$kd = $this->model_kd->detail($id_kd);
		$mapel = $this->model_mapel->listing();
		$kelas = $this->model_kelas->listing();
		$aspek = $this->model_aspek->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_kelas', ' Kelas & Jurusan', 'required',
			array('required' => ' Kelas & jurusan Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			//end validasi
			$data = array(	'title' => 'Tambah Data Kompetensi Dasar',
							'kd' 	=> $kd,
							'mapel' => $mapel,
							'kelas' => $kelas,
							'aspek' => $aspek,
							'isi' 	=> 'guru/data_kd/edit' );
			$this->load->view('guru/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_kd' 	=> $id_kd,
							'id_kelas' 	=> $i->post('id_kelas'),
							'id_mapel' 	=> $i->post('id_mapel'),
							'id_aspek' 	=> $i->post('id_aspek'), 
							'kode_kd' 	=> $i->post('kode_kd'),
							'isi_kd' 	=> $i->post('isi_kd')
						);
			$this->model_kd->edit($data);
			$this->session->set_flashdata('sukses', 'data telah diupdate');
			redirect(base_url('guru/data_kd'),'refresh');
		}
	}


	function delete($id_kd)
	{
		$data = array('id_kd' => $id_kd);
		$this->model_kd->delete($data);
		$this->session->set_flashdata('sukses', 'data telah dihapus');
		redirect(base_url('guru/data_kd'),'refresh');
	}
}

/* End of file data_kd.php */
/* Location: ./application/controllers/guru/data_kd.php */