<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prakerin extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_pkl');
		$this->load->model('model_ajaran');
		$this->load->model('model_kelas');
		$this->load->model('model_siswa');
	}

	public function index()
	{
		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
			
		);
		
		$pkl = $this->model_pkl->listing($where);

		$data = array(	'title' => 'Praktek Kerja Lapangan',
						'pkl' 	=> $pkl,
						'isi' 	=> 'guru/prakerin/list' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);	
	}

	public function tambah()
	{


		// asumsi
		$id_kelas='';


		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$kelas 	= $this->model_kelas->listing($where);
		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			$where=array(

				'siswa.id_kelas'=>$id_kelas
				
			);

			
			$siswa 	= $this->model_siswa->listing($where);


		}else{

		
			$siswa=array();

		}

		// end get siswa sesuai kelas not jquery :D

		$ajaran = $this->model_ajaran->listing();
		
		

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_kelas', ' Kelas & Jurusan', 'required',
			array('required' => ' Kelas & jurusan Harus dipilih'));

		if ($valid->run()===FALSE) 
		{

		$data = array(	'title' 	=> 'Praktek Kerja Lapangan',
						'id_kelas'	=> $id_kelas,
						'ajaran' 	=> $ajaran,
						'kelas' 	=> $kelas,
						'siswa' 	=> $siswa,
						'isi' 		=> 'guru/prakerin/tambah' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_ajaran' 	=> $i->post('id_ajaran'),
							'id_kelas' 		=> $i->post('id_kelas'),
							'id_siswa' 		=> $i->post('id_siswa'),
							'nama_instansi' => $i->post('nama_instansi'),
							'alamat' 		=> $i->post('alamat'),
							'lama' 			=> $i->post('lama'),
							'keterangan' 	=> $i->post('keterangan')
						);
			$this->model_pkl->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/prakerin'),'refresh');
		}	
	}

}

/* End of file prakerin.php */
/* Location: ./application/controllers/guru/prakerin.php */