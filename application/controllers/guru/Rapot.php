<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Rapot extends CI_Controller
	{

		private $semester_aktif='';
		public function __construct()
		{
			parent::__construct();
			$this->load->model('model_siswa');
			$this->load->model('model_mapel');
			$this->load->model('model_kd');
			$this->load->model('model_aspek');
			$this->load->model('model_absen');
			$this->load->model('model_ajaran');
			$this->load->model('model_catatan');
			$this->load->model('model_ne');
			$this->load->model('model_sn');
			$this->load->model('model_nilai');
			$this->load->model('model_pkl');
			$this->load->model('model_prestasi');
			$this->load->model('model_semester');
			$this->load->model('model_sikap');
			$this->load->model('model_predikat');
			$this->load->model('model_deskripsi_mapel');
			$this->load->model('model_golongan');
			$this->load->model('model_pkl');
			$this->load->model('model_ekskul');
			$this->load->model('model_prestasi');
			$this->load->model('model_absen');
			$this->load->model('model_catatan');
			$this->load->model('model_data_sekolah');
			$this->load->model('model_kelas');


			$this->semester_aktif=$this->model_semester->getsemesteraktif();
			$this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
		}

		public function index()
		{


			$id_ajaran=$this->ajaran_aktif;

			$where=array(

				'kelas.id_guru'=>$this->session->id_guru
			);
			$siswa = $this->model_siswa->listing($where);


			$ajaran = $this->model_ajaran->listing();

			$data = array(	'title' =>'Cetak Rapor',
							'siswa' =>	$siswa,
							
							
							'isi'	=>'guru/rapor/list' );

			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}


		public function cetak($id_siswa)
		{



			$id_ajaran=$this->ajaran_aktif;
			$id_semester=$this->semester_aktif;


			$mpdf = new \Mpdf\Mpdf(['format' => 'A4','margin_left'=>'5','margin_right'=>'5','margin_top'=>'5','margin_bottom'=>'5']);


			//datasiswa

			$where=array(

				'aktif'=>true
			);
			$data['semester']=$this->model_semester->getsemesteraktif();

			$where=array(

				'siswa.id_siswa'=>$id_siswa
			);
			$data['siswa']=$this->model_siswa->get($where)->row();

			$where=array(

				'kelas.id_kelas'=>$data['siswa']->id_kelas
			);
			$data['kelas']=$this->model_kelas->listing($where);

			$where=array(

				'tahun_ajaran.id_ajaran'=>$id_ajaran
			);
			$data['tahun_ajaran']=$this->model_ajaran->get($where)->row();

			$where=array(

				'siswa.id_siswa'=>$id_siswa,
				'sikap.id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				'sikap.id_semester'=>$data['semester']
			);
			
			$data['sikap']=$this->model_sikap->listing($where);

			$data['mapel']=$this->hitungrapor($id_ajaran,$id_semester,$id_siswa);
			
			
			$data['golongan_mapel']=$this->model_golongan->get();

			$where=array(

				'id_siswa'=>$id_siswa,
				'id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				
			);

			$data['prakerin']=$this->model_pkl->get($where);

			
			$where=array(

				'siswa.id_siswa'=>$id_siswa,
				'nilai_ekskul.id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				'nilai_ekskul.id_semester'=>$data['semester']
			);
			$data['ekskul']=$this->model_ekskul->listing($where);


			$where=array(

				'siswa.id_siswa'=>$id_siswa,
				'prestasi.id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				'prestasi.id_semester'=>$data['semester']
			);
			$data['prestasi']=$this->model_prestasi->listing($where);

			$where=array(

				'siswa.id_siswa'=>$id_siswa,
				'absen.id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				'absen.id_semester'=>$data['semester']
			);
			$data['ketidakhadiran']=$this->model_absen->get($where);

			$data['data_sekolah']=$this->model_data_sekolah->get();

			$where=array(

				'siswa.id_siswa'=>$id_siswa,
				'catatan_wk.id_ajaran'=>$data['tahun_ajaran']->id_ajaran,
				'catatan_wk.id_semester'=>$data['semester']
			);
			$data['catatan']=$this->model_catatan->get($where);


			$data['kelulusan']=$this->hitungkelulusan($id_ajaran,$id_siswa);


			$html=$this->load->view('rapor/cover_rapor',$data,true);
			
			$mpdf->AddPage('P');
			$mpdf->WriteHTML($html);

			$html=$this->load->view('rapor/datasiswa',$data,true);
			
			$mpdf->AddPage('P');
			$mpdf->WriteHTML($html);


			$html=$this->load->view('rapor/datadiri',$data,true);
			$mpdf->AddPage('P');
			$mpdf->WriteHTML($html);

			//end datasiswa

			$html=$this->load->view('rapor/hasil_belajar',$data,true);
			$mpdf->AddPage('L');
			$mpdf->WriteHTML($html);

			$html=$this->load->view('rapor/pengetahuan_dan_keterampilan',$data,true);
			$mpdf->AddPage('L');
			$mpdf->WriteHTML($html);

			$html=$this->load->view('rapor/catatan_wali_kelas',$data,true);
			$mpdf->AddPage('L');
			$mpdf->WriteHTML($html);


			$mpdf->Output($data['siswa']->nama_siswa.'.pdf', 'I');


			
		}





		function hitungrapor($id_ajaran,$id_semester,$id_siswa){


			$where=array(

				'siswa.id_siswa'=>$id_siswa
			);


			$siswa=$this->model_siswa->get($where)->row();

			$where=array(

				'guru_mapel.id_kelas'=>$siswa->id_kelas
			);

			$mapel=$this->model_mapel->mapelperkelas($where)->result();

			// $nilai=$this->model_nilai->listing($where);






			$nilaimapel=array();
			foreach($mapel as $datamapel){
				$aspek=$this->model_aspek->listing();
				$nilaikdmapel=array();
				foreach($aspek as $dataaspek){

					$where=array(

						'id_mapel'=>$datamapel->id_mapel,
						'id_kelas'=>$datamapel->id_kelas,
						'id_aspek'=>$dataaspek->id_aspek
					);

					$kd=$this->model_kd->getdata($where)->result();

					$ratarata=0;
					$nilaiuts=0;
					$nilaiuas=0;
					foreach($kd as $datakd){

						$where=array(
							'id_ajaran'=>$id_ajaran,
							'id_kd'=>$datakd->id_kd
						);



						$klasifikasibobotsn=$this->model_sn->getklasifikasibobot($where)->result();


						$total=0;
						$totalbobot=0;
						foreach($klasifikasibobotsn as $klasifikasibobotsn){

							$totalbobot +=$klasifikasibobotsn->bobot_nilai;
							$where=array(
								'id_ajaran'=>$id_ajaran,
								'id_semester'=>$id_semester,
								'id_kd'=>$datakd->id_kd,
								'bobot_nilai'=>$klasifikasibobotsn->bobot_nilai

							);

							$sn=$this->model_sn->getdata($where)->result();


							$totalnilai=0;


							//mengulang sesua klasifikasibobot
							if(!empty($sn)){

								foreach($sn as $datasn){

									$where=array(
										'id_semester'=>$id_semester,
										'id_siswa'=>$id_siswa,
										'id_sn'=>$datasn->id
									);

									$nilai=$this->model_nilai->getdata($where)->row();

									//jika nilai belum diisi
									if(count($nilai)!=0){


										//get nilai
										$isidatanilai=$nilai->nilai;

									}else{


										//dianggap 0
										$isidatanilai=0;


									}


									//rata rata setiap klasifikasi bobot
									$totalnilai +=($datasn->bobot_nilai*$isidatanilai);



								}

								$total +=($totalnilai/count($sn));

							}
							


						}

						if($totalbobot!=0){

							$total=($total/$totalbobot);

						}else{


						}

						$ratarata +=$total;

						// echo $datamapel->nama_mapel.':'.$datakd->kode_kd.':'.$total;
						// echo '<br>';



						//cari nilai uts
						$where=array(
							'id_semester'=>$id_semester,
							'id_siswa'=>$id_siswa,
							'id_ajaran'=>$id_ajaran,
							'id_mapel'=>$datamapel->id_mapel,
							'id_kelas'=>$datamapel->id_kelas,
							'id_aspek'=>$dataaspek->id_aspek,
							'tipe_ujian'=>1
						);

						$nilaiujian=$this->model_nilai->getdata_ujian($where)->row();

						if(!empty($nilaiujian)){

							$nilaiuts=$nilaiujian->nilai;
						}else{

							$nilaiuts=0;

						}

						//cari nilai uas
						$where=array(
							'id_semester'=>$id_semester,
							'id_siswa'=>$id_siswa,
							'id_ajaran'=>$id_ajaran,
							'id_mapel'=>$datamapel->id_mapel,
							'id_kelas'=>$datamapel->id_kelas,
							'id_aspek'=>$dataaspek->id_aspek,
							'tipe_ujian'=>2
						);

						$nilaiujian=$this->model_nilai->getdata_ujian($where)->row();

						if(!empty($nilaiujian)){

							$nilaiuas=$nilaiujian->nilai;
						}else{

							$nilaiuas=0;

						}


						

					}

					// cari deskripsi
					$where=array(
						'id_semester'=>$id_semester,
						'id_siswa'=>$id_siswa,
						'id_ajaran'=>$id_ajaran,
						'id_mapel'=>$datamapel->id_mapel,
						'id_kelas'=>$datamapel->id_kelas
					);

					$deskripsi_mapel=$this->model_deskripsi_mapel->getdata($where)->row();




					

					if(count($kd)!=0){

						$ratarata=$ratarata/count($kd);

					}else{

						$ratarata = $ratarata;
					}


					//hitung nilai akhir
					if($dataaspek->id_aspek==1){

						$nilaiakhir=(((($ratarata+$nilaiuts)/2)*2)+($nilaiuas*2))/(2+2);
						
						if(!empty($deskripsi_mapel)){
							$deskripsi=$deskripsi_mapel->pengetahuan;
						}else{
							$deskripsi='';
						}
						
					}else{

						$nilaiakhir=($ratarata+$nilaiuts)/2;
						if(!empty($deskripsi_mapel)){
							$deskripsi=$deskripsi_mapel->keterampilan;
						}else{

							$deskripsi='';
						}
					}


					
					//menghitung predikat

					$data_predikat=$this->model_predikat->get();

					foreach($data_predikat as $data_predikat){


						if($nilaiakhir>=$data_predikat->batas_bawah && $nilaiakhir<=$data_predikat->batas_atas){

							$predikat=$data_predikat->predikat;

						}

					}


					$array=array(

						'id_mapel'=>$datamapel->id_mapel,
						'nama_mapel'=>$datamapel->nama_mapel,
						'id_aspek'=>$dataaspek->id_aspek,
						'nama_aspek'=>$dataaspek->nama_aspek,
						'nilai_rata_rata'=>$ratarata,
						'nilai_uts'=>$nilaiuts,
						'nilai_uas'=>$nilaiuas,
						'nilai_akhir'=>round($nilaiakhir),
						'kkm'=>'75',
						'deskripsi'=>$deskripsi,
						'predikat'=>$predikat
					);

					array_push($nilaikdmapel,$array);


				}

				$array=array(

					'id_mapel'=>$datamapel->id_mapel,
					'id_semester'=>$id_semester,
					'nama_mapel'=>$datamapel->nama_mapel,
					'nama_aspek'=>$dataaspek->nama_aspek,
					'id_golongan'=>$datamapel->id_golongan,
					'nilai'=>$nilaikdmapel,

				);



				array_push($nilaimapel,$array);

			}


			return $nilaimapel;
		}


		function hitungkelulusan($id_ajaran,$id_siswa){
			


			$where=array(

				'siswa.id_siswa'=>$id_siswa
			);
			$siswa=$this->model_siswa->get($where)->row();

			$where=array(

				'kelas.id_kelas'=>$siswa->id_kelas
			);
			$kelas=$this->model_kelas->listing($where);

			$result=array();
			$kkm=75;
			$semester=$this->model_semester->get()->result();

			$jumlahtidaklulus=0;
			//mengulang sebanyak semester
			foreach($semester as $data_semester){

				
				//mengambil data 2 semseter
				$nilai_mapel=$this->hitungrapor($id_ajaran,$data_semester->id_semester,$id_siswa);
				
				foreach($nilai_mapel as $data_nilai_mapel){

					foreach($data_nilai_mapel['nilai'] as $data_nilai ){

						if($data_nilai['nilai_akhir']<$kkm){

							$jumlahtidaklulus++;
						}
						
					}
				}
				
			}

			//mencari kelas sekarang
			$dikelas=$kelas[0]->nama_kelas;
			if($jumlahtidaklulus>2){

				

				$lulus=false;
				
			}else{

				
				
				
				if($dikelas!=13){
					$dikelas++;	
				}else{


				}

				$lulus=true;
			}


			//cari nama jurusan
			$nama_jurusan=$kelas[0]->nama_jurusan;
			$nama_jurusan=explode(" ",$nama_jurusan);
			$nama_jurusan=$nama_jurusan[1];

			$result=array(

				'jumlah_tidak_lulus'=>$jumlahtidaklulus,
				'lulus'=>$lulus,
				'di_kelas'=>$this->getRomawi($dikelas),
				'nama_jurusan'=>$nama_jurusan
			);

			
			
			return $result;
		}

		function getRomawi($angka){
			switch ($angka){
				case 1: 
					return "I";
					break;
				case 2:
					return "II";
					break;
				case 3:
					return "III";
					break;
				case 4:
					return "IV";
					break;
				case 5:
					return "V";
					break;
				case 6:
					return "VI";
					break;
				case 7:
					return "VII";
					break;
				case 8:
					return "VIII";
					break;
				case 9:
					return "IX";
					break;
				case 10:
					return "X";
					break;
				case 11:
					return "XI";
					break;
				case 12:
					return "XII";
					break;
			}
		}

	}

	/* End of file rapot.php */
	/* Location: ./application/controllers/guru/rapot.php */