<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_siswa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_siswa');
		$this->load->model('model_kelas');
		$this->load->model('model_mapel');
		$this->load->model('model_ajaran');
		$this->load->model('model_semester');
	}

	public function index()
	{
		$where=array(

			'guru_mapel.id_guru'=>$this->session->id_guru
			
		);
		$kelas = $this->model_mapel->siswa_mapel($where)->result();

		$data = array(	'title' => 'Data Siswa',
						'kelas' => $kelas,
						'isi' 	=> 'guru/data_siswa/list');
		$this->load->view('guru/layout/wrapper', $data, FALSE);	
	}

	// //tambah
	// public function tambah()
	// {
	// 	$kelas 		= $this->model_kelas->listing();
	// 	$ajaran 	= $this->model_ajaran->listing();
	// 	$semester 	= $this->model_semester->listing();

	// 	//validasi
	// 	$valid = $this->form_validation;

	// 	$valid->set_rules('id_kelas','Nama Jurusan','required',
	// 		array(	'required' 	=>	'Nama Jurusan harus dipilih'));

	// 	$valid->set_rules('nama_siswa','Nama','required',
	// 		array(	'required' 	=>	'Nama harus diisi'));


	// 	$valid->set_rules('username','Username','required|is_unique[siswa.username]',
	// 		array(	'required' 	=>	'Username harus diisi',
	// 				'is_unique' => 'Username <strong>'.$this->input->post('username').'</strong> Sudah ada. Buat Siswa Baru'));

	// 	$valid->set_rules('password','Password','required|min_length[5]',
	// 		array(	'required' 		=>	'Password harus diisi',
	// 				'min_length'	=> 'Password minimal 5 karakter'));
		
	// 	if ($valid->run()===FALSE) {
	// 		//end validasi

	// 	$data = array(	'title' 	=> 'Tambah Data Siswa',
	// 					'kelas' 	=> $kelas,
	// 					'ajaran' 	=> $ajaran,
	// 					'semester' 	=> $semester,
	// 					'isi' 		=> 'admin/siswa/tambah');
	// 	$this->load->view('admin/layout/wrapper', $data, FALSE);
	// 	//gax ada eror, maka masuk database
	// 	}else{
	// 		$i= $this->input;
	// 		$data = array(	'id_kelas' 			=>$i->post('id_kelas'),
	// 						'id_ajaran' 		=>$i->post('id_ajaran'),
	// 						'id_semester' 		=>$i->post('id_semester'),
	// 						'nama_siswa' 		=>$i->post('nama_siswa'),
	// 						'no_induk' 			=>$i->post('no_induk'),
	// 						'nisn' 				=>$i->post('nisn'),
	// 						'jk' 				=>$i->post('jk'),
	// 						'tempat_lahir' 		=>$i->post('tempat_lahir'),
	// 						'tgl_lahir' 		=>$i->post('tgl_lahir'),
	// 						'agama' 			=>$i->post('agama'),
	// 						'status' 			=>$i->post('status'),
	// 						'anak_ke' 			=>$i->post('anak_ke'),
	// 						'alamat' 			=>$i->post('alamat'),
	// 						'rt' 				=>$i->post('rt'),
	// 						'rw' 				=>$i->post('rw'),
	// 						'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
	// 						'kecamatan' 		=>$i->post('kecamatan'),
	// 						'kabupaten' 		=>$i->post('kabupaten'),
	// 						'kode_pos' 			=>$i->post('kode_pos'),
	// 						'no_hp' 			=>$i->post('no_hp'),
	// 						'sekolah_asal' 		=>$i->post('sekolah_asal'),
	// 						'diterima_kelas' 	=>$i->post('diterima_kelas'),
	// 						'diterima' 			=>$i->post('diterima'),
	// 						'email' 			=>$i->post('email'),
	// 						'nama_ayah' 		=>$i->post('nama_ayah'),
	// 						'kerja_ayah' 		=>$i->post('kerja_ayah'),
	// 						'telp_ayah' 		=>$i->post('telp_ayah'),
	// 						'nama_ibu' 			=>$i->post('nama_ibu'),
	// 						'kerja_ibu' 		=>$i->post('kerja_ibu'),
	// 						'telp_ibu' 			=>$i->post('telp_ibu'),
	// 						'nama_wali' 		=>$i->post('nama_wali'),
	// 						'kerja_wali' 		=>$i->post('kerja_wali'),
	// 						'telp_wali' 		=>$i->post('telp_wali'),
	// 						'alamat_wali' 		=>$i->post('alamat_wali'),
	// 						'status_siswa' 		=>$i->post('status_siswa'),
	// 						'username' 			=>$i->post('username'),
	// 						'password' 			=>sha1($i->post('password'))
	// 						);
	// 		$this->model_siswa->tambah($data);
	// 		$this->session->set_flashdata('sukses', 'Data Berhasil Ditambah');
	// 		redirect(base_url('admin/siswa'),'refresh');
	// 	}
	// 	//end masuk database
	// }

	// //edit
	// public function edit($id_siswa)
	// {
	// 	$siswa 		= $this->model_siswa->detail($id_siswa);
	// 	$kelas 		= $this->model_kelas->listing();
	// 	$ajaran 	= $this->model_ajaran->listing();
	// 	$semester 	= $this->model_semester->listing();
	// 	//validasi
	// 	$valid = $this->form_validation;

	// 	$valid->set_rules('id_kelas','Nama Jurusan','required',
	// 		array(	'required' 	=>	'Nama Jurusan harus dipilih'));

	// 	$valid->set_rules('nama_siswa','Nama','required',
	// 		array(	'required' 	=>	'Nama harus diisi'));

		
	// 	if ($valid->run()===FALSE) {
	// 		//end validasi

	// 	$data = array(	'title' 	=> 'Edit Data Siswa :'.$siswa->nama_siswa,
	// 					'siswa' 	=> $siswa,
	// 					'kelas' 	=> $kelas,
	// 					'ajaran' 	=> $ajaran,
	// 					'semester' 	=> $semester,
	// 					'isi' 		=> 'admin/siswa/edit');
	// 	$this->load->view('admin/layout/wrapper', $data, FALSE);
	// 	//gax ada eror, maka masuk database
	// 	}else{
	// 		$i= $this->input;

	// 		//jika input password lebih dari 5 karakter
	// 		if (strlen($i->post('password'))> 5) {
	// 			$data = array(	'id_siswa' 			=>$id_siswa,
	// 							'id_kelas' 			=>$i->post('id_kelas'),
	// 							'id_ajaran' 		=>$i->post('id_ajaran'),
	// 							'id_semester' 		=>$i->post('id_semester'),
	// 							'nama_siswa' 		=>$i->post('nama_siswa'),
	// 							'no_induk' 			=>$i->post('no_induk'),
	// 							'nisn' 				=>$i->post('nisn'),
	// 							'jk' 				=>$i->post('jk'),
	// 							'tempat_lahir' 		=>$i->post('tempat_lahir'),
	// 							'tgl_lahir' 		=>$i->post('tgl_lahir'),
	// 							'agama' 			=>$i->post('agama'),
	// 							'status' 			=>$i->post('status'),
	// 							'anak_ke' 			=>$i->post('anak_ke'),
	// 							'alamat' 			=>$i->post('alamat'),
	// 							'rt' 				=>$i->post('rt'),
	// 							'rw' 				=>$i->post('rw'),
	// 							'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
	// 							'kecamatan' 		=>$i->post('kecamatan'),
	// 							'kabupaten' 		=>$i->post('kabupaten'),
	// 							'kode_pos' 			=>$i->post('kode_pos'),
	// 							'no_hp' 			=>$i->post('no_hp'),
	// 							'sekolah_asal' 		=>$i->post('sekolah_asal'),
	// 							'diterima_kelas' 	=>$i->post('diterima_kelas'),
	// 							'diterima' 			=>$i->post('diterima'),
	// 							'email' 			=>$i->post('email'),
	// 							'nama_ayah' 		=>$i->post('nama_ayah'),
	// 							'kerja_ayah' 		=>$i->post('kerja_ayah'),
	// 							'telp_ayah' 		=>$i->post('telp_ayah'),
	// 							'nama_ibu' 			=>$i->post('nama_ibu'),
	// 							'kerja_ibu' 		=>$i->post('kerja_ibu'),
	// 							'telp_ibu' 			=>$i->post('telp_ibu'),
	// 							'nama_wali' 		=>$i->post('nama_wali'),
	// 							'kerja_wali' 		=>$i->post('kerja_wali'),
	// 							'telp_wali' 		=>$i->post('telp_wali'),
	// 							'alamat_wali' 		=>$i->post('alamat_wali'),
	// 							'status_siswa' 		=>$i->post('status_siswa'),
	// 							'username' 			=>$i->post('username'),
	// 							'password' 			=>sha1($i->post('password'))
	// 						);
	// 		}else{
	// 			$data = array(	'id_siswa' 			=>$id_siswa,
	// 							'id_kelas' 			=>$i->post('id_kelas'),
	// 							'id_ajaran' 		=>$i->post('id_ajaran'),
	// 							'id_semester' 		=>$i->post('id_semester'),
	// 							'nama_siswa' 		=>$i->post('nama_siswa'),
	// 							'no_induk' 			=>$i->post('no_induk'),
	// 							'nisn' 				=>$i->post('nisn'),
	// 							'jk' 				=>$i->post('jk'),
	// 							'tempat_lahir' 		=>$i->post('tempat_lahir'),
	// 							'tgl_lahir' 		=>$i->post('tgl_lahir'),
	// 							'agama' 			=>$i->post('agama'),
	// 							'status' 			=>$i->post('status'),
	// 							'anak_ke' 			=>$i->post('anak_ke'),
	// 							'alamat' 			=>$i->post('alamat'),
	// 							'rt' 				=>$i->post('rt'),
	// 							'rw' 				=>$i->post('rw'),
	// 							'desa_kelurahan' 	=>$i->post('desa_kelurahan'),
	// 							'kecamatan' 		=>$i->post('kecamatan'),
	// 							'kabupaten' 		=>$i->post('kabupaten'),
	// 							'kode_pos' 			=>$i->post('kode_pos'),
	// 							'no_hp' 			=>$i->post('no_hp'),
	// 							'sekolah_asal' 		=>$i->post('sekolah_asal'),
	// 							'diterima_kelas' 	=>$i->post('diterima_kelas'),
	// 							'diterima' 			=>$i->post('diterima'),
	// 							'email' 			=>$i->post('email'),
	// 							'nama_ayah' 		=>$i->post('nama_ayah'),
	// 							'kerja_ayah' 		=>$i->post('kerja_ayah'),
	// 							'telp_ayah' 		=>$i->post('telp_ayah'),
	// 							'nama_ibu' 			=>$i->post('nama_ibu'),
	// 							'kerja_ibu' 		=>$i->post('kerja_ibu'),
	// 							'telp_ibu' 			=>$i->post('telp_ibu'),
	// 							'nama_wali' 		=>$i->post('nama_wali'),
	// 							'kerja_wali' 		=>$i->post('kerja_wali'),
	// 							'telp_wali' 		=>$i->post('telp_wali'),
	// 							'alamat_wali' 		=>$i->post('alamat_wali'),
	// 							'status_siswa' 		=>$i->post('status_siswa'),
	// 							'username' 			=>$i->post('username')
	// 						);

	// 		}
	// 		//end if
	// 		$this->model_siswa->edit($data);
	// 		$this->session->set_flashdata('sukses', 'Data Berhasil Diupdate');
	// 		redirect(base_url('admin/siswa'),'refresh');
	// 	}
	// 	//end masuk database
	// }

	// //delete
	// public function delete($id_siswa)
	// {
	// 	//proteksi hapus disini
	// 	if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
	// 	$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
	// 	redirect(base_url('login'),'refresh');
	// 	}
	// 	//end proteksi
	// 	$data = array('id_siswa' => $id_siswa);
	// 	$this->model_siswa->delete($data);
	// 	$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
	// 	redirect(base_url('admin/siswa'),'refresh');
	// }


}

/* End of file siswa.php */
/* Location: ./application/controllers/siswa/siswa.php */