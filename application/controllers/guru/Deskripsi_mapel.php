<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deskripsi_mapel extends CI_Controller
{
    
    private $semester_aktif='';
    
    public function __construct()
	{
		parent::__construct();
		
        $this->load->model('model_mapel');
        $this->load->model('model_siswa');
        $this->load->model('model_deskripsi_mapel');
        $this->load->model('model_ajaran');
        $this->load->model('model_kelas');
        
        $this->load->model('model_semester');
        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
	}

	public function index(){

        $id_ajaran=$this->ajaran_aktif;
        $id_mapel='';
        $id_kelas='';

		
        
        if(isset($_GET['mapel']) && !empty($_GET['mapel'])){

			$id_mapel=$_GET['mapel'];

        }
        
        if(isset($_GET['kelas']) && !empty($_GET['kelas'])){

			$id_kelas=$_GET['kelas'];

		}

		
        $where=array(

            'kelas.id_kelas'=>$id_kelas
        );



        $siswa=$this->model_siswa->listing($where);

       
       
        if(!empty($id_ajaran)){



            // mendapatkan data kelas yang di wali
            $where=array(

                'guru_mapel.id_guru'=>$this->session->id_guru
            );
            
    
            $kelas=$this->model_mapel->listing($where);
            
           
            
            // mendapatkan mapel yang di wali
            $where=array(
                'guru_mapel.id_guru'=>$this->session->id_guru,
                'kelas.id_kelas'=>$id_kelas
            );

            $mapel=$this->model_mapel->listing($where);


            

            $where=array(

                'deskripsi_mapel.id_semester'=>$this->semester_aktif,
                'deskripsi_mapel.id_ajaran'=>$id_ajaran,
                'kelas.id_kelas'=>$id_kelas,
                'deskripsi_mapel.id_mapel'=>$id_mapel
            );

            $deskripsi_mapel=$this->model_deskripsi_mapel->listing($where);
            
        }else{

            $deskripsi_mapel=array();
            $mapel=array();
        }
        
		$ajaran=$this->model_ajaran->listing();

		$data = array(	'title' => 'Deskripi Mata Pelajaran',
                        'ajaran'=> $ajaran,
                        'mapel'=>$mapel,
                        'kelas'=>$kelas,
                        'siswa'=> $siswa,
                        'deskripsi_mapel'=> $deskripsi_mapel,
                        'id_ajaran'=> $id_ajaran,
                        'id_kelas'=> $id_kelas,
                        'id_mapel'=> $id_mapel,
						'isi' 	=> 'guru/deskripsi_mapel/index');
        $this->load->view('guru/layout/wrapper', $data, FALSE);	
        
    }
    

    public function store(){

        $id_ajaran=$this->ajaran_aktif;
        $id_mapel='';
        $id_kelas='';

		
        
        if(isset($_GET['mapel']) && !empty($_GET['mapel'])){

			$id_mapel=$_GET['mapel'];

        }
        
        if(isset($_GET['kelas']) && !empty($_GET['kelas'])){

			$id_kelas=$_GET['kelas'];

		}

		
        $where=array(

            'kelas.id_kelas'=>$id_kelas
        );



        $siswa=$this->model_siswa->listing($where);

        

        //cek dulu data ada apa tidak
        $where=array(

            'deskripsi_mapel.id_semester'=>$this->semester_aktif,
            'deskripsi_mapel.id_ajaran'=>$id_ajaran,
            'kelas.id_kelas'=>$id_kelas,
            'deskripsi_mapel.id_mapel'=>$id_mapel
        );

        $deskripsi_mapel=$this->model_deskripsi_mapel->listing($where);

        

        // jika data telah ada maka hanya di update jika tidak ada lakukan input
        if(count($deskripsi_mapel)!=0){

           // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){


                $id_deskripsi_mapel=$this->input->post('id_deskripsi_mapel');
                $pengetahuan=$this->input->post('pengetahuan');
                $keterampilan=$this->input->post('keterampilan');

                
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_mapel'=>$id_mapel,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'pengetahuan'=>$pengetahuan[$data_siswa->id_siswa],
                    'keterampilan'=>$keterampilan[$data_siswa->id_siswa]
                );

                $where=array(

                    'id'=>$id_deskripsi_mapel[$data_siswa->id_siswa]
                );

                $this->model_deskripsi_mapel->edit($data,$where);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Update');
            
        }else{


            // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){

                $pengetahuan=$this->input->post('pengetahuan');
                $keterampilan=$this->input->post('keterampilan');

                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_mapel'=>$id_mapel,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'pengetahuan'=>$pengetahuan[$data_siswa->id_siswa],
                    'keterampilan'=>$keterampilan[$data_siswa->id_siswa]
                );

                $this->model_deskripsi_mapel->tambah($data);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
             
        }



        redirect(base_url('guru/deskripsi_mapel/index?kelas='.$id_kelas.'&mapel='.$id_mapel),'refresh');
           
        
       

    }

	
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */