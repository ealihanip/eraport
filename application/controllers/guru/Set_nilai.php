<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Set_nilai extends CI_Controller 
{
	
	private $semester_aktif='';
	private $ajaran_aktif='';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_sn');
		$this->load->model('model_ajaran');
		$this->load->model('model_mapel');
		$this->load->model('model_kelas');
		$this->load->model('model_kd');
		$this->load->model('model_aspek');

		$this->load->model('model_semester');
		$this->semester_aktif=$this->model_semester->getsemesteraktif();
		$this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
	}

	// public function index()
	// {
	// 	$pn = $this->model_sn->listing1();

	// 	$data = array(	'title' => 'Rencana Penilaian',
	// 					'pn' 	=> $pn,
	// 					'isi' 	=> 'guru/set_nilai/list' );
	// 	$this->load->view('guru/layout/wrapper', $data, FALSE);
	// }

	public function pengetahuan()
	{
		$where=array(
			'set_nilai.id_ajaran'=>$this->ajaran_aktif,
			'set_nilai.id_semester'=>$this->semester_aktif,
			'kd.id_mapel'=>$this->session->guru_mapel,
			'aspek.id_aspek'=>1
		);

		$sn = $this->model_sn->listing($where);
		$data = array(	'title' => 'Rencana Penilaian Pengetahuan',
						'key' => 'pengetahuan',
						'sn' 	=> $sn,
	 					'isi' 	=> 'guru/set_nilai/list' );
	 	$this->load->view('guru/layout/wrapper', $data, FALSE);
	}

	public function keterampilan()
	{
		$where=array(

			'kd.id_mapel'=>$this->session->guru_mapel,
			'aspek.id_aspek'=>2
		);

		$sn = $this->model_sn->listing($where);

		$data = array(	'title' => 'Rencana Penilaian Keterampilan',
						'key' => 'keterampilan',
	 					'sn' 	=> $sn,
	 					'isi' 	=> 'guru/set_nilai/list' );
	 	$this->load->view('guru/layout/wrapper', $data, FALSE);
	}

	
	public function tambah_pengetahuan()
	{
		
		
		
		$id_kelas='';
		// asumsi
		$id_mapel='';



		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$mapel = $this->model_mapel->listing($where);

		
		


		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			
			$where=array(
				'guru.id_guru'=>$this->session->id_guru,
				'kelas.id_kelas'=>$id_kelas
				
			);

			$mapel = $this->model_mapel->listing($where);


		}else{

			
			

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){
			
			$id_mapel=$_GET['mapel'];
			
			$where=array(
				
				'kd.id_kelas'=>$id_kelas,
				'kd.id_mapel'=>$id_mapel,
				'kd.id_aspek'=>'1'
				
				

			);

			$kd = $this->model_kd->listing($where);


		}else{

			$kd=array();

		}
		


	
		
		$aspek 	= $this->model_aspek->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('tahun', 'required',
			array('required' => ' Tahun Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			$data = array(	'title' 	=> 'Set Nilai Pengetahuan',
							'key' 	=> 'pengetahuan',
							
							'mapel' 	=> $mapel,
							'id_kelas' 	=> $id_kelas,
							'id_mapel' 	=> $id_mapel,
							'kd' 		=> $kd,
							'aspek' 	=> $aspek,
					 		'isi' 		=> 'guru/set_nilai/tambah_pengetahuan');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_ajaran' 		=> $this->ajaran_aktif,
							'id_semester' 		=> $this->semester_aktif,
						 	'id_mapel' 			=> $i->post('id_mapel'),
						 	'id_kelas' 			=> $i->post('id_kelas'),
						 	'nama_penilaian' 	=> $i->post('nama_penilaian'),
						 	'bobot_nilai' 		=> $i->post('bobot_nilai'),
						 	'id_kd' 			=> $i->post('id_kd'),
						 	'id_aspek' 			=> '1',
						 	'keterangan' 		=> $i->post('keterangan')
						 );
			$this->model_sn->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/set_nilai/pengetahuan'),'refresh');
		}
	}


	public function tambah_keterampilan()
	{

	
		$id_kelas='';
		// asumsi
		$id_mapel='';



		

		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$mapel = $this->model_mapel->listing($where);


		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			
			$where=array(
				'guru.id_guru'=>$this->session->id_guru,
				'kelas.id_kelas'=>$id_kelas
				
			);

			$mapel = $this->model_mapel->listing($where);


		}else{

			
			

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){
			
			$id_mapel=$_GET['mapel'];
			
			$where=array(
				
				'kd.id_kelas'=>$id_kelas,
				'kd.id_mapel'=>$id_mapel,
				'kd.id_aspek'=>'2'
				
				

			);

			$kd = $this->model_kd->listing($where);


		}else{

			$kd=array();

		}
		


	
		
		$aspek 	= $this->model_aspek->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('tahun', 'required',
			array('required' => ' Tahun Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			$data = array(	'title' 	=> 'Set Nilai Keterampilan',
							'key' 		=> 'keterampilan',
						
							'mapel' 	=> $mapel,
							'id_kelas' 	=> $id_kelas,
							'id_mapel' 	=> $id_mapel,
							
							'kd' 		=> $kd,
							'aspek' 	=> $aspek,
					 		'isi' 		=> 'guru/set_nilai/tambah_keterampilan');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_ajaran' 		=> $this->ajaran_aktif,
							'id_semester' 		=> $this->semester_aktif,
						 	'id_mapel' 			=> $i->post('id_mapel'),
						 	'id_kelas' 			=> $i->post('id_kelas'),
						 	'nama_penilaian' 	=> $i->post('nama_penilaian'),
						 	'bobot_nilai' 		=> $i->post('bobot_nilai'),
						 	'id_kd' 			=> $i->post('id_kd'),
						 	'id_aspek' 			=> '2',
						 	'keterangan' 		=> $i->post('keterangan')
						 );
			$this->model_sn->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/set_nilai/keterampilan'),'refresh');
		}
	}

	public function edit_keterampilan($id)
	{

		$where=array(

			'set_nilai.id'=>$id
		);

		$sn = $this->model_sn->listing($where);

		// asumsi
		$id_kelas=$sn[0]->id_kelas;
		// asumsi
		$id_mapel=$sn[0]->id_mapel;
		
	
		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$mapel = $this->model_mapel->listing($where);


		// get siswa sesuai kelas not jquery :D
			
		$where=array(
	
			'guru.id_guru'=>$this->session->id_guru,
			'kelas.id_kelas'=>$id_kelas
			
		);

		$mapel = $this->model_mapel->listing($where);

		$where=array(

			'kd.id_kelas'=>$id_kelas,
			'kd.id_mapel'=>$id_mapel,
			'kd.id_aspek'=>'2'
			
	
		);

		$kd = $this->model_kd->listing($where);

		

		$ajaran = $this->model_ajaran->listing();
		
		$aspek 	= $this->model_aspek->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('tahun', 'required',
			array('required' => ' Tahun Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			$data = array(	'title' 	=> 'Set Nilai Keterampilan',
							'key' 		=> 'keterampilan',
							
							'mapel' 	=> $mapel,
							'sn' 		=> $sn[0],
							'id_kelas' 	=> $id_kelas,
							'id_mapel' 	=> $id_mapel,
							
							'kd' 		=> $kd,
							'aspek' 	=> $aspek,
					 		'isi' 		=> 'guru/set_nilai/edit_keterampilan');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			
			$i = $this->input;
			$data = array(	'id' 				=> $id,
							'id_ajaran' 		=> $this->ajaran_aktif,
							'id_semester' 		=> $this->semester_aktif,
						 	'id_mapel' 			=> $i->post('id_mapel'),
						 	'id_kelas' 			=> $i->post('id_kelas'),
						 	'nama_penilaian' 	=> $i->post('nama_penilaian'),
						 	'bobot_nilai' 		=> $i->post('bobot_nilai'),
						 	'id_kd' 			=> $i->post('id_kd'),
						 	'id_aspek' 			=> '2',
						 	'keterangan' 		=> $i->post('keterangan')
						 );
			$this->model_sn->edit($data);
			$this->session->set_flashdata('sukses', 'data telah diubah');
			redirect(base_url('guru/set_nilai/keterampilan'),'refresh');
		}
	}
	


	public function edit_pengetahuan($id)
	{

		$where=array(

			'set_nilai.id'=>$id
		);

		$sn = $this->model_sn->listing($where);

		
		// asumsi
		$id_kelas=$sn[0]->id_kelas;
		// asumsi
		$id_mapel=$sn[0]->id_mapel;
	
	
		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$mapel = $this->model_mapel->listing($where);


		// get siswa sesuai kelas not jquery :D
			
		$where=array(
	
			'guru.id_guru'=>$this->session->id_guru,
			'kelas.id_kelas'=>$id_kelas
			
		);

		$mapel = $this->model_mapel->listing($where);

		$where=array(

			'kd.id_kelas'=>$id_kelas,
			'kd.id_mapel'=>$id_mapel,
			'kd.id_aspek'=>'1'
			
	
		);

		$kd = $this->model_kd->listing($where);

		

		$ajaran = $this->model_ajaran->listing();
		
		$aspek 	= $this->model_aspek->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('tahun', 'required',
			array('required' => ' Tahun Harus dipilih'));

		if ($valid->run()===FALSE) 
		{
			$data = array(	'title' 	=> 'Set Nilai pengetahuan',
							'key' 	=> 'pengetahuan',
						
							'mapel' 	=> $mapel,
							'sn' 		=> $sn[0],
							'id_kelas' 	=> $id_kelas,
							'id_mapel' 	=> $id_mapel,
						
							'kd' 		=> $kd,
							'aspek' 	=> $aspek,
					 		'isi' 		=> 'guru/set_nilai/edit_pengetahuan');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id' 				=> $id,
							'id_ajaran' 		=> $this->ajaran_aktif,
							'id_semester' 		=> $this->semester_aktif,
						 	'id_mapel' 			=> $i->post('id_mapel'),
						 	'id_kelas' 			=> $i->post('id_kelas'),
						 	'nama_penilaian' 	=> $i->post('nama_penilaian'),
						 	'bobot_nilai' 		=> $i->post('bobot_nilai'),
						 	'id_kd' 			=> $i->post('id_kd'),
						 	'id_aspek' 			=> '1',
						 	'keterangan' 		=> $i->post('keterangan')
						 );
			$this->model_sn->edit($data);
			$this->session->set_flashdata('sukses', 'data telah diubah');
			redirect(base_url('guru/set_nilai/pengetahuan'),'refresh');
		}
	}
	

	public function delete($id)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama_guru')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		
		$where=array(

			'set_nilai.id'=>$id
		);

		$sn = $this->model_sn->listing($where);

		$data = array('id' => $id);
		$this->model_sn->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');

		if($sn[0]->id_aspek==1){
			redirect(base_url('guru/set_nilai/pengetahuan'),'refresh');
		}else{
			redirect(base_url('guru/set_nilai/keterampilan'),'refresh');
		}
			
		
	}
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */