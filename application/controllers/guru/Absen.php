<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Absen extends CI_Controller
{
    
    private $semester_aktif='';
    
    public function __construct()
	{
		parent::__construct();
		
        $this->load->model('model_siswa');
        $this->load->model('model_absen');
        $this->load->model('model_ajaran');
        
        $this->load->model('model_semester');
        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();

	}

	public function index(){

		
		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);



        $siswa=$this->model_siswa->listing($where);
        

        //cek dulu data ada apa tidak
        $where=array(

            'absen.id_semester'=>$this->semester_aktif,
            'absen.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );


        $absen=$this->model_absen->listing($where);
            
        
        
		

		$data = array(	'title' => 'Absen',
						
                        'siswa'=> $siswa,
                        'absen'=> $absen,
						
						'isi' 	=> 'guru/absen/index');
        $this->load->view('guru/layout/wrapper', $data, FALSE);	
        
    }
    

    public function store(){

        $id_ajaran=$this->ajaran_aktif;
        
        $where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);

        $siswa=$this->model_siswa->listing($where);

        
        $absen=$this->input->post('absen');

        

        //cek dulu data ada apa tidak
        $where=array(
            'absen.id_semester'=>$this->semester_aktif,
            'absen.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );

        $absen=$this->model_absen->listing($where);



        // jika data telah ada maka hanya di update jika tidak ada lakukan input
        if(count($absen)!=0){

           // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){


                $id_absen=$this->input->post('id_absen');
                
                
                $izin=$this->input->post('izin');
                $sakit=$this->input->post('sakit');
                $tanpa_keterangan=$this->input->post('tanpa_keterangan');


               
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'izin'=>$izin[$data_siswa->id_siswa],
                    'sakit'=>$sakit[$data_siswa->id_siswa],
                    'tanpa_keterangan'=>$tanpa_keterangan[$data_siswa->id_siswa]
                );

                $where=array(

                    'id'=>$id_absen[$data_siswa->id_siswa]
                );

                $this->model_absen->edit($data,$where);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Update');

        }else{


            // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){



                $izin=$this->input->post('izin');
                $sakit=$this->input->post('sakit');
                $tanpa_keterangan=$this->input->post('tanpa_keterangan');

                
                $data=array(

                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$id_ajaran,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'izin'=>$izin[$data_siswa->id_siswa],
                    'sakit'=>$sakit[$data_siswa->id_siswa],
                    'tanpa_keterangan'=>$tanpa_keterangan[$data_siswa->id_siswa]
                );

                $this->model_absen->tambah($data);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
            
            
        }


        redirect(base_url('guru/absen/index?'),'refresh');

        
       

    }

	
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */