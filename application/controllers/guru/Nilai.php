<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends CI_Controller
{
	
	private $semester_aktif='';
	private $ajaran_aktif='';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_nilai');
		$this->load->model('model_ajaran');
		$this->load->model('model_mapel');
		$this->load->model('model_kelas');
		$this->load->model('model_sn');
		$this->load->model('model_siswa');
		$this->load->model('model_sn');

		$this->load->model('model_semester');
		$this->semester_aktif=$this->model_semester->getsemesteraktif();
		$this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
	}

	public function pengetahuan()
	{
		
		$id_kelas='';
		// asumsi
		$id_mapel='';

		// asumsi
		$id_sn='';


		
	

		$where=array(

			'guru.id_guru'=>$this->session->id_guru

		);

		$mapel = $this->model_mapel->listing($where);

		
		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){

			$id_kelas=$_GET['kelas'];

			$where=array(
				'guru.id_guru'=>$this->session->id_guru,
				'kelas.id_kelas'=>$id_kelas

			);

			$mapel = $this->model_mapel->listing($where);

			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);
			$siswa = $this->model_siswa->listing($where);

		}else{


			$siswa = array();

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){

			$id_mapel=$_GET['mapel'];

			$where=array(

				'set_nilai.id_ajaran'=>$this->ajaran_aktif,
				'set_nilai.id_kelas'=>$id_kelas,
				'set_nilai.id_mapel'=>$id_mapel,
				'set_nilai.id_aspek'=>'1'


			);

			$sn = $this->model_sn->listing($where);


		}else{

			$sn=array();

		}


		if(isset($_GET['sn']) && $_GET['sn']!=''){
			
			$id_sn=$_GET['sn'];

			
			//kondisi jika memilih uas atau uts
			if($id_sn!='uas' && $id_sn!='uts'){

				$where=array(
					
					'nilai.id_semester'=>$this->semester_aktif,
					'nilai.id_sn'=>$id_sn
				);
	
				$nilai=$this->model_nilai->listing($where);
	

			}else{
				
				
				//jika itu adalah uts maka tipe adalah 1
				if($this->input->get('sn')=='uts'){

					$tipe=1;

				}else{

					$tipe=2;

				}

				$where=array(

					'nilai_ujian.id_semester'=>$this->semester_aktif,
					'nilai_ujian.id_aspek'=>1,
					'nilai_ujian.id_ajaran'=>$this->ajaran_aktif,
					'nilai_ujian.id_mapel'=>$this->input->get('mapel'),
					'nilai_ujian.id_kelas'=>$this->input->get('kelas'),
					'nilai_ujian.tipe_ujian'=>$tipe
				);
				
				
				$nilai=$this->model_nilai->listing_ujian($where);	
				
				
			}
			

		}else{

			$nilai=array();
		}

		

		
		$kelas 	= $this->model_kelas->listing();


		
		$data = array(	'title' 	=> 'Nilai Pengetahuan',
						'key' 		=> 'pengetahuan',
						'id_kelas'  => $id_kelas,
						'id_mapel'  => $id_mapel,
						'id_sn'    	=> $id_sn,
						
						
						'mapel' 	=> $mapel,
						'kelas' 	=> $kelas,
						'nilai' 	=> $nilai,
						'siswa' 	=> $siswa,
						'sn' 		=> $sn,
						'isi' 		=> 'guru/nilai/nilai');
		$this->load->view('guru/layout/wrapper', $data, FALSE);
	}


	public function keterampilan()
	{
		
		// asumsi
		$id_kelas='';
		// asumsi
		$id_mapel='';

		// asumsi
		$id_sn='';


		
		

		$where=array(

			'guru.id_guru'=>$this->session->id_guru

		);

		$mapel = $this->model_mapel->listing($where);

		
		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){

			$id_kelas=$_GET['kelas'];

			$where=array(
				'guru.id_guru'=>$this->session->id_guru,
				'kelas.id_kelas'=>$id_kelas

			);

			$mapel = $this->model_mapel->listing($where);

			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);
			$siswa = $this->model_siswa->listing($where);

		}else{


			$siswa = array();

		}

		if(isset($_GET['mapel']) && $_GET['mapel']!=''){

			$id_mapel=$_GET['mapel'];

			$where=array(
				'set_nilai.id_ajaran'=>$this->ajaran_aktif,
				'set_nilai.id_kelas'=>$id_kelas,
				'set_nilai.id_mapel'=>$id_mapel,
				'set_nilai.id_aspek'=>'2'


			);

			$sn = $this->model_sn->listing($where);


		}else{

			$sn=array();

		}


		if(isset($_GET['sn']) && $_GET['sn']!=''){
			
			$id_sn=$_GET['sn'];

			
			//kondisi jika memilih uas atau uts
			if($id_sn!='uas' && $id_sn!='uts'){

				$where=array(
					'nilai.id_semester'=>$this->semester_aktif,
					'nilai.id_sn'=>$id_sn
				);
	
				$nilai=$this->model_nilai->listing($where);
	

			}else{
				

				//jika itu adalah uts maka tipe adalah 1
				if($this->input->get('sn')=='uts'){

					$tipe=1;

				}else{

					$tipe=2;

				}

				$where=array(

					'nilai_ujian.id_semester'=>$this->semester_aktif,
					'nilai_ujian.id_aspek'=>2,
					'nilai_ujian.id_ajaran'=>$this->ajaran_aktif,
					'nilai_ujian.id_mapel'=>$this->input->get('mapel'),
					'nilai_ujian.id_kelas'=>$this->input->get('kelas'),
					'nilai_ujian.tipe_ujian'=>$tipe
				);
				
				
				$nilai=$this->model_nilai->listing_ujian($where);	
				
				
			}
			

		}else{

			$nilai=array();
		}

		

		$ajaran = $this->model_ajaran->listing();
		$kelas 	= $this->model_kelas->listing();


		
		$data = array(	'title' 	=> 'Nilai Keterampilan',
						'key' 		=> 'keterampilan',
						'id_kelas'  => $id_kelas,
						'id_mapel'  => $id_mapel,
						'id_sn'    	=> $id_sn,
						
						
						'mapel' 	=> $mapel,
						'kelas' 	=> $kelas,
						'nilai' 	=> $nilai,
						'siswa' 	=> $siswa,
						'sn' 		=> $sn,
						'isi' 		=> 'guru/nilai/nilai');
		$this->load->view('guru/layout/wrapper', $data, FALSE);
	}

	

	public function store(){

		//cek apakah data tersedia

		$where=array(
			'nilai.id_semester'=>$this->semester_aktif,
			'nilai.id_sn'=>$this->input->post('id_sn')

		);

		
		$nilai = $this->model_nilai->listing($where);
		
		if(count($nilai)==0){

			if(isset($_GET['kelas']) && $_GET['kelas']!=''){

				$id_kelas=$_GET['kelas'];
				$where=array(

					'kelas.id_kelas'=>$id_kelas

				);
				$siswa = $this->model_siswa->listing($where);

			}else{


				$siswa = array();

			}

			foreach($siswa as $data_siswa){
				$input_nilai=$this->input->post('nilai');

				if(!empty($input_nilai[$data_siswa->id_siswa])){

					$nilai=$input_nilai[$data_siswa->id_siswa];
				}else{

					$nilai=0;
				}


				$data=array(

					'id_semester'=>$this->semester_aktif,
					'id_sn'=>$this->input->post('id_sn'),
					'id_siswa'=>$data_siswa->id_siswa,
					'nilai'=>$nilai
				);

				
				$this->model_nilai->tambah($data);

			}

			$this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
			redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');
		

		}else{

			$this->session->set_flashdata('gagal','Data Telah Tersedia');
			redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');
		

		}


			
		
	}

	public function update(){

		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){

			$id_kelas=$_GET['kelas'];
			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);
			$siswa = $this->model_siswa->listing($where);

		}else{


			$siswa = array();

		}

		foreach($siswa as $data_siswa){
			$input_nilai=$this->input->post('nilai');
			$input_id_nilai=$this->input->post('id_nilai');


			if(!empty($input_nilai[$data_siswa->id_siswa])){

				$nilai=$input_nilai[$data_siswa->id_siswa];
			}else{

				$nilai=0;
			}

			$id_nilai=$input_id_nilai[$data_siswa->id_siswa];


			$data=array(
				'id_semester'=>$this->semester_aktif,
				'id_sn'=>$this->input->post('id_sn'),
				'id_siswa'=>$data_siswa->id_siswa,
				'nilai'=>$nilai
			);

			$where=array(

				'id_nilai'=>$id_nilai

			);

			
			
			$this->model_nilai->edit($data,$where);

		}
		$this->session->set_flashdata('sukses','Data Berhasil Di Update');
		
		redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');
		
	}




	public function store_ujian(){

		

		if($this->input->post('aspek')=='pengetahuan'){

			$id_aspek=1;

		}else{

			$id_aspek=2;

		}



		//jika itu adalah uts maka tipe adalah 1
		if($this->input->post('id_sn')=='uts'){

			$tipe=1;

		}else{

			$tipe=2;

		}

		


		$id_ajaran=$this->input->post('id_ajaran');
		$id_mapel=$this->input->post('id_mapel');
		
		
		//cek data tersedia

		$where=array(

			'nilai_ujian.id_semester'=>$this->semester_aktif,
			'nilai_ujian.id_aspek'=>$id_aspek,
			'nilai_ujian.id_ajaran'=>$this->input->post('id_ajaran'),
			'nilai_ujian.id_mapel'=>$this->input->post('id_mapel'),
			'nilai_ujian.id_kelas'=>$this->input->post('id_kelas'),
			'nilai_ujian.tipe_ujian'=>$tipe

		);

		
		$nilai = $this->model_nilai->listing_ujian($where);


		
		if(count($nilai)==0){

			if(isset($_GET['kelas']) && $_GET['kelas']!=''){

				$id_kelas=$_GET['kelas'];
				$where=array(

					'kelas.id_kelas'=>$id_kelas

				);
				$siswa = $this->model_siswa->listing($where);

			}else{


				$siswa = array();

			}

			foreach($siswa as $data_siswa){
				$input_nilai=$this->input->post('nilai');

				if(!empty($input_nilai[$data_siswa->id_siswa])){

					$nilai=$input_nilai[$data_siswa->id_siswa];
				}else{

					$nilai=0;
				}


				$data=array(


					'id_semester'=>$this->semester_aktif,
					'id_aspek'=>$id_aspek,
					'id_siswa'=>$data_siswa->id_siswa,
					'id_mapel'=>$id_mapel,
					'id_kelas'=>$id_kelas,
					'id_ajaran'=>$this->ajaran_aktif,
					'tipe_ujian'=>$tipe,
					'nilai'=>$nilai
				);

				
				$this->model_nilai->tambah_ujian($data);

			}

			$this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
			
			redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');
		

		}else{

			$this->session->set_flashdata('gagal','Data Telah Tersedia');
			
			redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');
		

		}

			
		
	}


	public function update_ujian(){

		if($this->input->post('aspek')=='pengetahuan'){

			$id_aspek=1;

		}else{

			$id_aspek=2;

		}

		//jika itu adalah uts maka tipe adalah 1
		if($this->input->post('id_sn')=='uts'){

			$tipe=1;

		}else{

			$tipe=2;

		}


		$id_ajaran=$this->input->post('id_ajaran');
		$id_mapel=$this->input->post('id_mapel');
		

		if(isset($_GET['kelas']) && $_GET['kelas']!=''){

			$id_kelas=$_GET['kelas'];
			$where=array(

				'kelas.id_kelas'=>$id_kelas

			);
			$siswa = $this->model_siswa->listing($where);

		}else{


			$siswa = array();

		}

		foreach($siswa as $data_siswa){
			$input_nilai=$this->input->post('nilai');
			$input_id_nilai=$this->input->post('id_nilai');


			if(!empty($input_nilai[$data_siswa->id_siswa])){

				$nilai=$input_nilai[$data_siswa->id_siswa];
			}else{

				$nilai=0;
			}

			$id_nilai=$input_id_nilai[$data_siswa->id_siswa];


			$where=array(
				
				'nilai_ujian.id_nilai'=>$id_nilai
			);

			$data=array(
				'id_semester'=>$this->semester_aktif,
				'id_aspek'=>$id_aspek,
				'id_siswa'=>$data_siswa->id_siswa,
				'id_mapel'=>$id_mapel,
				'id_kelas'=>$id_kelas,
				'id_ajaran'=>$this->ajaran_aktif,
				'tipe_ujian'=>$tipe,
				'nilai'=>$nilai
			);

			
			$this->model_nilai->edit_ujian($data,$where);

		}

		$this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
		
		redirect(base_url('guru/nilai/'.$this->input->post('aspek').'?kelas='.$_GET['kelas'].'&mapel='.$_GET['mapel'].'&sn='.$_GET['sn']),'refresh');


		
	}


	

	


	//delete
	public function delete($id)
	{
		//proteksi hapus disini
		if ($this->session->userdata('username')=="" && $this->session->userdata('nama_guru')=="") {
		$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
		redirect(base_url('login'),'refresh');
		}
		//end proteksi

		$data = array('id' => $id);
		$this->model_sn->delete($data);
		$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
		redirect(base_url('guru/nilai/pengetahuan'),'refresh');
	}

}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */