<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasi extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_prestasi');
		$this->load->model('model_ajaran');
		$this->load->model('model_kelas');
		$this->load->model('model_siswa');
	}

	public function index()
	{

		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
			
		);
		

		$prestasi = $this->model_prestasi->listing($where);

		$data = array(	'title' 	=> 'Prestasi',
						'prestasi' 	=> $prestasi,
						'isi' 		=> 'guru/prestasi/list' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);	
	}

	public function tambah()
	{



		// asumsi
		$id_kelas='';


		$where=array(

			'guru.id_guru'=>$this->session->id_guru
			
		);

		$kelas 	= $this->model_kelas->listing($where);
		
		// get siswa sesuai kelas not jquery :D
		if(isset($_GET['kelas']) && $_GET['kelas']!=''){
			
			$id_kelas=$_GET['kelas'];
			$where=array(

				'siswa.id_kelas'=>$id_kelas
				
			);

			
			$siswa 	= $this->model_siswa->listing($where);


		}else{

		
			$siswa=array();

		}

		// end get siswa sesuai kelas not jquery :D


		$ajaran = $this->model_ajaran->listing();
		
		

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('id_kelas', ' Kelas & Jurusan', 'required',
			array('required' => ' Kelas & jurusan Harus dipilih'));

		if ($valid->run()===FALSE) 
		{

		$data = array(	'title' 	=> 'Prestasi',
						'id_kelas'	=> $id_kelas,
						'ajaran' 	=> $ajaran,
						'kelas' 	=> $kelas,
						'siswa' 	=> $siswa,
						'isi' 		=> 'guru/prestasi/tambah' );
		$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		else
		{
			$i = $this->input;
			$data = array(	'id_ajaran' 	=> $i->post('id_ajaran'),
							'id_kelas' 		=> $i->post('id_kelas'),
							'id_siswa' 		=> $i->post('id_siswa'),
							'nama_prestasi' => $i->post('nama_prestasi'),
							'keterangan' 	=> $i->post('keterangan')
						);
			$this->model_prestasi->tambah($data);
			$this->session->set_flashdata('sukses', 'data telah ditambah');
			redirect(base_url('guru/prestasi'),'refresh');
		}	
	}

}

/* End of file prestasi.php */
/* Location: ./application/controllers/guru/prestasi.php */