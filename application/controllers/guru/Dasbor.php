<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dasbor extends CI_Controller 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_guru');
		$this->load->model('model_setmapel');
	}

	public function index()
	{
		if ($this->session->userdata('akses_level')==1) 
		{
			$gm = $this->model_setmapel->perguru();

			$data = array(	'title' 	=> 'Dasbor',
							'gm' 		=> $gm,
							'isi' 		=> 'guru/dasbor/list');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		elseif ($this->session->userdata('akses_level')==2) 
		{
			$gm = $this->model_setmapel->perguru();

			$data = array(	'title' 	=> 'Dasbor',
							'gm' 		=> $gm,
							'isi' 		=> 'guru/dasbor/list1');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		} 
		else 
		{
			$gm = $this->model_setmapel->perguru();

			$data = array(	'title' 	=> 'Dasbor',
							'gm' 		=> $gm,
							'isi' 		=> 'guru/dasbor/list2');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
		}
		
	}

	//profile
	public function profile()
	{
		$id_guru = $this->session->userdata('id_guru');//diambil dari session
		$guru 	= $this->model_guru->detail($id_guru);
		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('nama_guru','Nama','required',
			array(	'required' 	=>	'Nama harus diisi'));


		$valid->set_rules('email','Email','required|valid_email',
			array(	'required' 		=>	'Email harus diisi',
					'valid_email' 	=> 'Format email salah'));
		
		if ($valid->run()===FALSE) 
		{
			//end validasi
			$data = array(	'title' => 'Update Data Guru :'.$guru->nama_guru,
							'guru' 	=> $guru,
							'isi' 	=> 'guru/dasbor/profile');
			$this->load->view('guru/layout/wrapper', $data, FALSE);
			//gax ada eror, maka masuk database
		}
		else
		{
			$i= $this->input;

			//jika input password lebih dari 5 karakter
			if (strlen($i->post('password'))> 5) 
			{
				$data = array(	'id_guru' 	=> $id_guru,
								'nama_guru' => $i->post('nama_guru'),
								'email' 	=> $i->post('email'),
								'username' 	=> $i->post('username'),
								'password' 	=> sha1($i->post('password'))
							);
			}
			else
			{
				$data = array(	'id_guru' 	=>$id_guru,
								'nama_guru' =>$i->post('nama_guru'),
								'email' 	=>$i->post('email'),
								'username' 	=>$i->post('username')
							);
			}
			//end if
			$this->model_guru->edit($data);
			$this->session->set_flashdata('sukses', 'profile Berhasil Diupdate');
			redirect(base_url('guru/dasbor/profile'),'refresh');
		}
	}

}

/* End of file dasbor.php */
/* Location: ./application/controllers/guru/dasbor.php */