<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_kelas');
		$this->load->model('model_ajaran');
		$this->load->model('model_guru');
		$this->load->model('model_kompetensi');
		$this->load->model('model_siswa');
		$this->load->model('model_ajaran');
		$this->load->model('model_semester');
		$this->load->helper('my_helper');
	}

	public function index()
	{
		$kelas = $this->model_kelas->listing1();

		$data = array(	'title' => '',
						'kelas' => $kelas,
					 	'isi' 	=> 'guru/kelas/list'
					 );
		$this->load->view('guru/layout/wrapper', $data, FALSE);
		
	}

	// public function tambah()
	// {
	// 	$kompetensi = $this->model_kompetensi->listing();
	// 	$ajaran 	= $this->model_ajaran->listing();
	// 	$guru 		= $this->model_guru->listing1();
	// 	//validasi
	// 	$valid = $this->form_validation;

	// 	$valid->set_rules('id_guru', 'Wali Kelas', 'required',
	// 		array('required' => 'Wali Kelas Harus dipilih'));

	// 	if ($valid->run()===FALSE) 
	// 	{
	// 		//end validasi
	// 		$data = array(	'title' 		=> 'Tambah Data Kelas',
	// 						'kompetensi'	=> $kompetensi,
	// 						'ajaran'		=> $ajaran,
	// 						'guru' 			=> $guru,
	// 				 		'isi' 			=> 'guru/kelas/tambah'
	// 				 );
	// 		$this->load->view('guru/layout/wrapper', $data, FALSE);
	// 		//gax ada eror, maka masuk database
	// 	}
	// 	else
	// 	{
	// 		$i = $this->input;
	// 		$data = array(	'id_kompetensi' => $i->post('id_kompetensi'),
	// 						'id_ajaran' 	=> $i->post('id_ajaran'),
	// 						'id_guru' 		=> $i->post('id_guru'), 
	// 						'nama_kelas' 	=> $i->post('nama_kelas'),
	// 						'nama_jurusan' 	=> $i->post('nama_jurusan')
	// 					);
	// 		$this->model_kelas->tambah($data);
	// 		$this->session->set_flashdata('sukses', 'data telah ditambah');
	// 		redirect(base_url('guru/kelas'),'refresh');
	// 	}
		
	// }

	// //menampilkan daftar anggota kelas
	// public function anggota_kelas($id_kelas)
	// {
	// 	$kelas = $this->model_kelas->walikelas($id_kelas);

	// 	$data = array(	'title' 	=> 'Daftar Anggota Kelas :'.$id_kelas,
	// 					'kelas' 	=> $kelas,
	// 					'isi' 		=> 'guru/kelas/view' 
	// 				);
	// 	$this->load->view('guru/layout/wrapper', $data, FALSE);
	// }

	// public function edit($id_kelas)
	// {
	// 	$kelas 		= $this->model_kelas->detail($id_kelas);
	// 	$kompetensi = $this->model_kompetensi->listing();
	// 	$ajaran 	= $this->model_ajaran->listing();
	// 	$guru 		= $this->model_guru->listing();
	// 	//validasi
	// 	$valid = $this->form_validation;

	// 	$valid->set_rules('id_guru', 'Wali Kelas', 'required',
	// 		array('required' => 'Wali Kelas Harus dipilih'));

	// 	if ($valid->run()===FALSE) 
	// 	{
	// 		//end validasi
	// 		$data = array(	'title' 		=> 'Edit Data Kelas : ' .$kelas->nama_jurusan,
	// 						'kelas' 		=> $kelas,
	// 						'kompetensi' 	=> $kompetensi,
	// 						'ajaran' 		=> $ajaran,
	// 						'guru' 			=> $guru,
	// 				 		'isi' 			=> 'guru/kelas/edit'
	// 				 );
	// 		$this->load->view('guru/layout/wrapper', $data, FALSE);
	// 		//gax ada eror, maka masuk database
	// 	}
	// 	else
	// 	{
	// 		$i = $this->input;
	// 		$data = array(	'id_kelas' 		=> $id_kelas,
	// 						'id_kompetensi' => $i->post('id_kompetensi'),
	// 						'id_ajaran' 	=> $i->post('id_ajaran'),
	// 						'id_guru' 		=> $i->post('id_guru'), 
	// 						'nama_kelas' 	=> $i->post('nama_kelas'),
	// 						'nama_jurusan' 	=> $i->post('nama_jurusan')
	// 					);
	// 		$this->model_kelas->edit($data);
	// 		$this->session->set_flashdata('sukses', 'data telah diupdate');
	// 		redirect(base_url('guru/kelas'),'refresh');
	// 	}	
	// }

	// //delete
	// public function delete($id_kelas)
	// {
	// 	//proteksi hapus disini
	// 	if ($this->session->userdata('username')=="" && $this->session->userdata('nama')=="") {
	// 	$this->session->set_flashdata('sukses','silahkan login terlebih dahulu');
	// 	redirect(base_url('login'),'refresh');
	// 	}
	// 	//end proteksi

	// 	$data = array('id_kelas' => $id_kelas);
	// 	$this->model_kelas->delete($data);
	// 	$this->session->set_flashdata('sukses', 'Data Berhasil Dihapus');
	// 	redirect(base_url('guru/kelas'),'refresh');
	// }

}

/* End of file kelas.php */
/* Location: ./application/controllers/guru/kelas.php */