<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sikap extends CI_Controller
{
    
    private $semester_aktif='';
    
    public function __construct()
	{
		parent::__construct();
		
        $this->load->model('model_siswa');
        $this->load->model('model_sikap');
        $this->load->model('model_ajaran');
        
        $this->load->model('model_semester');
        $this->semester_aktif=$this->model_semester->getsemesteraktif();
        $this->ajaran_aktif=$this->model_ajaran->getajaranaktif();
	}

	public function index(){

		
		$where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);



        $siswa=$this->model_siswa->listing($where);
        

      
        //cek dulu data ada apa tidak
        $where=array(

            'sikap.id_semester'=>$this->semester_aktif,
            'sikap.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );


        $sikap=$this->model_sikap->listing($where);
            
     
        
		$ajaran=$this->model_ajaran->listing();

		$data = array(	'title' => 'Sikap Siswa',
						
                        'siswa'=> $siswa,
                        'sikap'=> $sikap,
						
						'isi' 	=> 'guru/sikap/index');
        $this->load->view('guru/layout/wrapper', $data, FALSE);	
        
    }
    

    public function store(){

        $id_ajaran=$this->input->get('ajaran');
        
        $where=array(

			'kelas.id_guru'=>$this->session->id_guru
		);

        $siswa=$this->model_siswa->listing($where);

        
        $sikap=$this->input->post('sikap');

        

        //cek dulu data ada apa tidak
        $where=array(
            'sikap.id_semester'=>$this->semester_aktif,
            'sikap.id_ajaran'=>$this->ajaran_aktif,
            'kelas.id_guru'=>$this->session->id_guru
        );

        $sikap=$this->model_sikap->listing($where);



        // jika data telah ada maka hanya di update jika tidak ada lakukan input
        if(count($sikap)!=0){

           // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){


                $id_sikap=$this->input->post('id_sikap');
                $sikap=$this->input->post('sikap');

               
                $data=array(
                    
                    'id_semester'=>$this->semester_aktif,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'sikap'=>$sikap[$data_siswa->id_siswa]
                );

                $where=array(

                    'id'=>$id_sikap[$data_siswa->id_siswa]
                );

                $this->model_sikap->edit($data,$where);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Update');
            redirect(base_url('guru/sikap/'),'refresh');

        }else{


            // mengulang sebanyak jumlah siswa

            foreach($siswa as $data_siswa){



                $sikap=$this->input->post('sikap');

                
                $data=array(
                    
                    'id_semester'=>$this->semester_aktif,
                    'id_ajaran'=>$this->ajaran_aktif,
                    'id_siswa'=>$data_siswa->id_siswa,
                    'id_kelas'=>$data_siswa->id_kelas,
                    'sikap'=>$sikap[$data_siswa->id_siswa]
                );

                $this->model_sikap->tambah($data);
            }

            $this->session->set_flashdata('sukses','Data Berhasil Di Simpan');
            redirect(base_url('guru/sikap/'),'refresh');
            
        }



        
       

    }

	
}

/* End of file set_nilai.php */
/* Location: ./application/controllers/guru/set_nilai.php */