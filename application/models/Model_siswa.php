<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_siswa extends CI_Model 
{
	//load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	public function get($where){

		$this->db->select('siswa.*,
							semester.nama_semester,
							kelas.nama_jurusan');
		$this->db->from('siswa');
		$this->db->join('semester', 'semester.id_semester = siswa.id_semester', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas', 'left');
		$this->db->join('guru', 'guru.id_guru = kelas.id_guru', 'left');
		$this->db->order_by('id_siswa', 'desc');
		$this->db->where($where);
		$query = $this->db->get();
	

		return $query;
	}

	//listing
	public function listing($where='')
	{

		if(!empty($where)){

			$this->db->select('siswa.*,
							semester.nama_semester,
							kelas.nama_jurusan');
			$this->db->from('siswa');
			$this->db->join('semester', 'semester.id_semester = siswa.id_semester', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas', 'left');
			$this->db->join('guru', 'guru.id_guru = kelas.id_guru', 'left');
			$this->db->order_by('id_siswa', 'desc');
			$this->db->where($where);
			
		}else{

			$this->db->select('siswa.*,
							semester.nama_semester,
							kelas.nama_jurusan');
			$this->db->from('siswa');
			$this->db->join('semester', 'semester.id_semester = siswa.id_semester', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas', 'left');
			$this->db->order_by('id_siswa', 'desc');

		}
		
		$query = $this->db->get();
		return $query->result();
	}
	//listing
	public function listing_laporan()
	{
		$this->db->select('siswa.*,
							semester.nama_semester,
							kelas.nama_jurusan');
		$this->db->from('siswa');
		$this->db->join('semester', 'semester.id_semester = siswa.id_semester', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas', 'left');
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//perkelas
	public function perkelas()
	{
		$this->db->select('siswa.*,
							kelas.nama_jurusan');
		$this->db->from('siswa');
		$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas', 'left');
		$this->db->where('siswa.id_kelas', $this->session->userdata('id_kelas'));
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function nilai_siswa()
	{
		$this->db->select(	'siswa.*, absensi.sakit, absensi.izin, absensi.alpa, catatan_wk.catatan, nilai.nilai, 
							nilai_ekskul.nilai, nilai_ekskul.keterangan, prakerin.nama_instansi, prakerin.alamat, prakerin.lama, 
							prakerin.keterangan, prestasi.nama_prestasi, prestasi.keterangan'
						);
		$this->db->from('siswa');
		$this->db->join('absensi', 'absensi.id_siswa = siswa.id_siswa', 'left');
		$this->db->join('catatan_wk', 'catatan_wk.id_siswa = siswa.id_siswa', 'left');
		$this->db->join('nilai', 'nilai.id_siswa = siswa.id_siswa', 'left');
		$this->db->join('nilai_ekskul', 'nilai_ekskul.id_siswa = siswa.id_siswa', 'left');
		$this->db->join('prakerin', 'prakerin.id_siswa = siswa.id_siswa', 'left');
		$this->db->join('prestasi', 'prestasi.id_siswa = siswa.id_siswa', 'left');
		// $this->db->where('siswa.id_siswa', $this->session->userdata('id_siswa'));
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();	

	}

	//detail
	public function detail($id_siswa)
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->where('id_siswa', $id_siswa);
		$this->db->order_by('id_siswa', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//login
	public function login($username, $password)
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->where(array(	'username' => $username,
								'password' => sha1($password)));
		$this->db->order_by('id_siswa', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('siswa', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_siswa', $data['id_siswa']);
		$this->db->update('siswa', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_siswa', $data['id_siswa']);
		$this->db->delete('siswa', $data);
	}
	

}

/* End of file model_siswa.php */
/* Location: ./application/models/model_siswa.php */