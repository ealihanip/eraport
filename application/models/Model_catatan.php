<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_catatan extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function get($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'catatan_wk.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('catatan_wk');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = catatan_wk.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = catatan_wk.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = catatan_wk.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->row();
	}

	public function listing($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'catatan_wk.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('catatan_wk');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = catatan_wk.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = catatan_wk.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = catatan_wk.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function listing1($id_kelas)
	{
		$this->db->select(	'catatan_wk.*,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('catatan_wk');
		$this->db->join('kelas', 'kelas.id_kelas = catatan_wk.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = catatan_wk.id_siswa', 'left');
		$this->db->where('kelas.id_kelas', $id_kelas);
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('catatan_wk');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function tambah($data)
	{
		$this->db->insert('catatan_wk', $data);
	}

	public function edit($data,$where)
	{
		$this->db->where($where);
		$this->db->update('catatan_wk', $data);
	}



	

}

/* End of file model_catatan.php */
/* Location: ./application/models/model_catatan.php */