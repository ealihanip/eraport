<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kompetensi extends CI_Model {

	//listing
	public function listing()
	{
		$this->db->select('*');
		$this->db->from('k_keahlian');
		//  SELECT title, content, date FROM mytable
		$this->db->order_by('id_kompetensi', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_kompetensi)
	{
		$this->db->select('*');
		$this->db->from('k_keahlian');
		$this->db->where('id_kompetensi', $id_kompentensi);
		$this->db->order_by('id_kompetensi', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('k_keahlian',$data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_kompetensi', $data['id_kompetensi']);
		$this->db->update('k_keahlian', $data);
	}

	

}

/* End of file model_kompentensi.php */
/* Location: ./application/models/model_kompentensi.php */