<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_mapel extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	

	public function mapelperkelas($where){

		$this->db->select('guru_mapel.*,mapel.*');
		$this->db->where($where);
		$this->db->from('guru_mapel');
		$this->db->join('mapel','mapel.id_mapel=guru_mapel.id_mapel');	
		$query = $this->db->get();
		

		return $query;
	}


	public function siswa_mapel($where){

		$this->db->select('siswa.*,mapel.*,kelas.*');
		$this->db->where($where);
		$this->db->from('guru_mapel');
		$this->db->join('mapel','mapel.id_mapel=guru_mapel.id_mapel');
		$this->db->join('kelas','kelas.id_kelas=guru_mapel.id_kelas');
		$this->db->join('siswa','siswa.id_kelas=kelas.id_kelas');	
		$query = $this->db->get();
		

		return $query;
	}


	//listing
	public function listing($where='')
	{
		
		//jika where
		if(!empty($where)){
			$this->db->select(
				
				'
					guru_mapel.id_gm,
					guru.*,
					mapel.*,
					kelas.*
		
				'
			);
			$this->db->from('guru_mapel');
			$this->db->join('guru', 'guru.id_guru = guru_mapel.id_guru');
			$this->db->join('mapel', 'mapel.id_mapel = guru_mapel.id_mapel');
			$this->db->join('kelas', 'kelas.id_kelas = guru_mapel.id_kelas');

			$this->db->where($where);
		}else{
			$this->db->select('*');
			$this->db->from('mapel');
		}
		//  SELECT title, content, date FROM mytable
		$this->db->order_by('mapel.id_mapel', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_mapel)
	{
		$this->db->select('*');
		$this->db->from('mapel');
		$this->db->where('id_mapel', $id_mapel);
		$this->db->order_by('id_mapel', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('mapel',$data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_mapel', $data['id_mapel']);
		$this->db->update('mapel', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_mapel', $data['id_mapel']);
		$this->db->delete('mapel', $data);
	}

}

/* End of file model_mapel.php */
/* Location: ./application/models/model_mapel.php */