<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_dataekskul extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		$this->db->select(	'ekskul.*,
							tahun_ajaran.tahun,
							guru.nama_guru');
		$this->db->from('ekskul');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = ekskul.id_ajaran', 'left');
		$this->db->join('guru', 'guru.id_guru = ekskul.id_guru', 'left');
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('ekskul');
		$this->db->where('id', $id);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('ekskul', $data);
	}

	public function edit($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('ekskul', $data);
	}

	public function delete($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->delete('ekskul', $data);
	}

	

}

/* End of file model_ekskul.php */
/* Location: ./application/models/model_ekskul.php */