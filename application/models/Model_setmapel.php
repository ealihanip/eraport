<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setmapel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	//listing
	public function listing()
	{
		$this->db->select(	'guru_mapel.*,
							tahun_ajaran.tahun,
							guru.nama_guru,
							kelas.nama_jurusan,
							mapel.nama_mapel');
		$this->db->from('guru_mapel');
		//  join
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = guru_mapel.id_ajaran', 'left');
		$this->db->join('guru', 'guru.id_guru = guru_mapel.id_guru', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = guru_mapel.id_kelas', 'left');
		$this->db->join('mapel', 'mapel.id_mapel = guru_mapel.id_mapel', 'left');
		//end join
		$this->db->order_by('id_gm', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	//listing
	public function perguru()
	{
		$this->db->select(	'guru_mapel.*,
							tahun_ajaran.tahun,
							guru.nama_guru,
							kelas.nama_jurusan,
							mapel.nama_mapel');
		$this->db->from('guru_mapel');
		//  join
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = guru_mapel.id_ajaran', 'left');
		$this->db->join('guru', 'guru.id_guru = guru_mapel.id_guru', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = guru_mapel.id_kelas', 'left');
		$this->db->join('mapel', 'mapel.id_mapel = guru_mapel.id_mapel', 'left');
		//end join
		$this->db->where('guru_mapel.id_guru', $this->session->userdata('id_guru'));
		$this->db->order_by('id_gm', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_gm)
	{
		$this->db->select('*');
		$this->db->from('guru_mapel');
		$this->db->where('id_gm', $id_gm);
		$this->db->order_by('id_gm', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function tambah($data)
	{
		$this->db->insert('guru_mapel',$data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_gm', $data['id_gm']);
		$this->db->update('guru_mapel', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_gm', $data['id_gm']);
		$this->db->delete('guru_mapel', $data);
		
	}

}

/* End of file model_setmapel.php */
/* Location: ./application/models/model_setmapel.php */