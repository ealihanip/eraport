<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kelas extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing($where='')
	{

		
		$this->db->select(	'kelas.*,
							tahun_ajaran.tahun,
							k_keahlian.*,
							b_keahlian.nama as nama_bidang_keahlian,
							b_keahlian.*,
							guru.nama_guru,
							guru.akses_level');


		if(!empty($where)){

			$this->db->from('kelas');
			$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = kelas.id_ajaran', 'left');
			$this->db->join('guru', 'guru.id_guru = kelas.id_guru', 'left');
			
			$this->db->join('k_keahlian', 'k_keahlian.id_kompetensi = kelas.id_kompetensi', 'left');
			$this->db->join('b_keahlian', 'k_keahlian.id_bidang = b_keahlian.id_bidang', 'left');

			$this->db->where($where);

		}else{
			$this->db->from('kelas');
			$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = kelas.id_ajaran', 'left');
			$this->db->join('guru', 'guru.id_guru = kelas.id_guru', 'left');
			$this->db->join('k_keahlian', 'k_keahlian.id_kompetensi = kelas.id_kompetensi', 'left');
			$this->db->join('b_keahlian', 'k_keahlian.id_bidang = b_keahlian.id_bidang', 'left');
		}
		
		//end join
		$this->db->order_by('nama_jurusan', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function listing1()
	{
		$this->db->select(	'kelas.*,
							siswa.*,
							tahun_ajaran.tahun');
		$this->db->from('kelas');
		$this->db->join('siswa', 'siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = kelas.id_ajaran', 'left');
		//end join
		$this->db->where('kelas.id_guru', $this->session->userdata('id_guru'));
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//menampilkan data siswa berdasarkan wali kelas untuk tampilan guru kelas
	function perkelas()
	{
		$this->db->select(	'kelas.*,
							siswa.*,
							guru_mapel.id_guru,
							guru_mapel.id_kelas,
							tahun_ajaran.tahun');
		$this->db->from('kelas');
		$this->db->join('siswa', 'siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('guru_mapel', 'guru_mapel.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = kelas.id_ajaran', 'left');
		
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	// //menampilkan data siswa berdasarkan kelas
	// public function walikelas($id_kelas)
	// {
	// 	$this->db->select(	'kelas.*,
	// 						siswa.*'
	// 					);
	// 	$this->db->from('kelas');
	// 	$this->db->join('siswa', 'siswa.id_kelas = kelas.id_kelas', 'left');
	// 	$this->db->where('siswa.id_kelas', $id_kelas);
	// 	$this->db->order_by('nama_siswa', 'asc');
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	//menampilkan data siswa berdasarkan kelas
	public function walikelas($id_kelas)
	{
		$this->db->select(	'kelas.*,
							siswa.*,
							guru.*'
						);
		$this->db->from('kelas');
		$this->db->join('siswa', 'siswa.id_kelas = kelas.id_kelas', 'left');
		$this->db->join('guru', 'guru.id_guru = kelas.id_guru', 'left');
		$this->db->where('siswa.id_kelas', $id_kelas);
		$this->db->order_by('nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_kelas)
	{
		$this->db->select('*');
		$this->db->from('kelas');
		$this->db->where('id_kelas', $id_kelas);
		$this->db->order_by('id_kelas', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('kelas', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_kelas', $data['id_kelas']);
		$this->db->update('kelas', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_kelas', $data['id_kelas']);
		$this->db->delete('kelas', $data);
	}
	

}

/* End of file model_kelas.php */
/* Location: ./application/models/model_kelas.php */