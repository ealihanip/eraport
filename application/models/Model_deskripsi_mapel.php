<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_deskripsi_mapel extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}



	public function getdata($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'deskripsi_mapel.*');
		$this->db->from('deskripsi_mapel');
		$query = $this->db->get();
		return $query;
	}

	public function listing($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'deskripsi_mapel.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('deskripsi_mapel');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = deskripsi_mapel.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = deskripsi_mapel.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = deskripsi_mapel.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function listing1($id_kelas)
	{
		$this->db->select(	'deskripsi_mapel.*,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('deskripsi_mapel');
		$this->db->join('kelas', 'kelas.id_kelas = deskripsi_mapel.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = deskripsi_mapel.id_siswa', 'left');
		$this->db->where('kelas.id_kelas', $id_kelas);
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('deskripsi_mapel');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function tambah($data)
	{
		$this->db->insert('deskripsi_mapel', $data);
	}

	public function edit($data,$where)
	{
		$this->db->where($where);
		$this->db->update('deskripsi_mapel', $data);
	}



	

}

/* End of file model_catatan.php */
/* Location: ./application/models/model_catatan.php */