<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sn extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	

	public function getdata($where){

		$this->db->select('set_nilai.*');
		$this->db->where($where);
		$this->db->from('set_nilai');
		$query = $this->db->get();
		

		return $query;
	}

	public function getklasifikasibobot($where){

		$this->db->select('set_nilai.bobot_nilai');
		$this->db->where($where);
		$this->db->from('set_nilai');
		$this->db->group_by("bobot_nilai");
		$query = $this->db->get();
		

		return $query;
	}
	
	
	public function listing($where='')
	{
		
		
		if(!empty($where)){

			$this->db->select(	'set_nilai.*,
							tahun_ajaran.tahun,
							mapel.nama_mapel,
							kelas.nama_kelas,
							kelas.nama_jurusan,
							kd.kode_kd,
							aspek.nama_aspek');
			$this->db->from('set_nilai');
			$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = set_nilai.id_ajaran', 'left');
			$this->db->join('mapel', 'mapel.id_mapel = set_nilai.id_mapel', 'left');
			$this->db->join('guru_mapel', 'guru_mapel.id_mapel = set_nilai.id_mapel', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = set_nilai.id_kelas', 'left');
			$this->db->join('kd', 'kd.id_kd = set_nilai.id_kd', 'left');
			$this->db->join('aspek', 'aspek.id_aspek = set_nilai.id_aspek', 'left');
			$this->db->order_by('id', 'asc');
			$this->db->where($where);

		}else{

			
			$this->db->select(	'set_nilai.*,
							tahun_ajaran.tahun,
							mapel.nama_mapel,
							kelas.nama_kelas,
							kelas.nama_jurusan,
							kd.kode_kd,
							aspek.nama_aspek');
			$this->db->from('set_nilai');
			$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = set_nilai.id_ajaran', 'left');
			$this->db->join('mapel', 'mapel.id_mapel = set_nilai.id_mapel', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = set_nilai.id_kelas', 'left');
			$this->db->join('kd', 'kd.id_kd = set_nilai.id_kd', 'left');
			$this->db->join('aspek', 'aspek.id_aspek = set_nilai.id_aspek', 'left');
			$this->db->order_by('id', 'asc');

		}

		$query = $this->db->get();
		return $query->result();
	}

	// public function listing($id_aspek)
	// {
	// 	$this->db->select(	'set_nilai.*,
	// 						tahun_ajaran.tahun,
	// 						mapel.nama_mapel,
	// 						kelas.nama_kelas,
	// 						kelas.nama_jurusan,
	// 						kd.kode_kd,
	// 						aspek.nama_aspek');
	// 	$this->db->from('set_nilai');
	// 	$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = set_nilai.id_ajaran', 'left');
	// 	$this->db->join('mapel', 'mapel.id_mapel = set_nilai.id_mapel', 'left');
	// 	$this->db->join('kelas', 'kelas.id_kelas = set_nilai.id_kelas', 'left');
	// 	$this->db->join('kd', 'kd.id_kd = set_nilai.id_kd', 'left');
	// 	$this->db->join('aspek', 'aspek.id_aspek = set_nilai.id_aspek', 'left');
	// 	$this->db->where('set_nilai.id_aspek', $id_aspek);
	// 	$this->db->order_by('id', 'desc');
	// 	$query = $this->db->get();
	// 	return $query->result();
	// }

	//detail
	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('set_nilai');
		$this->db->where('id', $id);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('set_nilai', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('set_nilai', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->delete('set_nilai', $data);
	}
	
}

/* End of file model_sn.php */
/* Location: ./application/models/model_sn.php */