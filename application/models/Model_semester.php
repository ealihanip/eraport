<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_semester extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		$this->db->select('*');
		$this->db->from('semester');
		$this->db->order_by('id_semester', 'asc');
		$query = $this->db->get();
		return $query->result();
	}


	public function get($where=''){

		if(!empty($where)){

			$this->db->where($where);
		}
		$query = $this->db->get('semester');
		return $query;

	}

	public function update($data,$where){

		$this->db->where($where);
		$this->db->update('semester',$data);

	}

	public function getsemesteraktif(){


		
		$this->db->where('aktif',true);
	
		$query = $this->db->get('semester');
		return $query->row()->id_semester;

	}
	

}

/* End of file model_semester.php */
/* Location: ./application/models/model_semester.php */