<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_Model {
	
	//load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//listing
	public function listing()
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->order_by('id_admin', 'desc');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_admin)
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where('id_admin', $id_admin);
		$this->db->order_by('id_admin', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//login
	public function login($username, $password)
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where(array(	'username' => $username,
								'password' => sha1($password)));
		$this->db->order_by('id_admin', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('admin', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_admin', $data['id_admin']);
		$this->db->update('admin', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_admin', $data['id_admin']);
		$this->db->delete('admin', $data);
	}
	

}

/* End of file model_user.php */
/* Location: ./application/models/model_user.php */