<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_setkelas extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	function listing()
	{
		$this->db->select(	'anggota_kelas.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('anggota_kelas');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = anggota_kelas.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = anggota_kelas.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = anggota_kelas.id_siswa', 'left');
		$this->db->order_by('id', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('anggota_kelas');
		$this->db->where('id', $id);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('anggota_kelas', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('anggota_kelas', $data);
	}

	

}

/* End of file model_setkelas.php */
/* Location: ./application/models/model_setkelas.php */