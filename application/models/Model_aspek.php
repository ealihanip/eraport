<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_aspek extends CI_Model 
{
	public function __construct()
	{
		
	//load database
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		//listing
		$this->db->select('*');
		$this->db->from('aspek');
		$this->db->order_by('id_aspek', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_aspek)
	{
		$this->db->select('*');
		$this->db->from('aspek');
		$this->db->where('id_aspek', $id_aspek);
		$this->db->order_by('id_aspek', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	

}

/* End of file model_ajaran.php */
/* Location: ./application/models/model_ajaran.php */