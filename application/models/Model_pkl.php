<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pkl extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing($where='')
	{
		$this->db->select(	'prakerin.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('prakerin');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = prakerin.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = prakerin.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = prakerin.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		if(!empty($where)){

			$this->db->where($where);

		}
		
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('prakerin');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function tambah($data)
	{
		$this->db->insert('prakerin', $data);
	}

	public function edit($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('prakerin', $data);
	}

	public function get($where=''){

		if(!empty($where)){

			$this->db->where($where);	
		}
		$this->db->select('prakerin.*');
	
		$this->db->from('prakerin');
		
		$query = $this->db->get()->result();
		

		return $query;
	}
}

/* End of file model_catatan.php */
/* Location: ./application/models/model_catatan.php */