<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {
	
	//load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//login admin
	public function login_admin($username, $password)
	{
		$this->db->select('*');
		$this->db->from('admin');
		$this->db->where(array(	'username' => $username,
								'password' => sha1($password)));
		$this->db->order_by('id_admin', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function login_guru($username, $password)
	{
		$this->db->select('guru.*, guru_mapel.id_mapel AS mapel');
		$this->db->from('guru');
		$this->db->join('guru_mapel','guru.id_guru = guru_mapel.id_guru', 'INNER');
		$this->db->where('guru.username', $username);
		$this->db->where('guru.password', sha1($password));
		$this->db->order_by('guru.id_guru', 'desc');
		$query = $this->db->get();
		return $query->row();
		// $query = $this->db->query("SELECT * FROM guru where username='$username' AND password=sha1('$password') ORDER BY id_guru");
		// return $query;
	}

	//login admin
	public function login_siswa($username, $password)
	{
		$this->db->select('*');
		$this->db->from('siswa');
		$this->db->where(array(	'username' => $username,
								'password' => sha1($password)));
		$this->db->order_by('id_siswa', 'desc');
		$query = $this->db->get();
		return $query->row();
	}
	
}

/* End of file model_guru.php */
/* Location: ./application/models/model_guru.php */