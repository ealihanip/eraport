<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_golongan extends CI_Model 
{

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	

	public function get(){

		$this->db->select('golongan_mapel.*');
	
		$this->db->from('golongan_mapel');
		
		$query = $this->db->get()->result();
		

		return $query;
	}


	

}

/* End of file model_mapel.php */
/* Location: ./application/models/model_mapel.php */