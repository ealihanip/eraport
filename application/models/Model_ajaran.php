<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ajaran extends CI_Model 
{
	public function __construct()
	{
		
	//load database
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		//listing
		$this->db->select('*');
		$this->db->from('tahun_ajaran');
		$this->db->order_by('id_ajaran', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_ajaran)
	{
		$this->db->select('*');
		$this->db->from('tahun_ajaran');
		$this->db->where('id_ajaran', $id_ajaran);
		$this->db->order_by('id_ajaran', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	public function get($where='')
	{	


		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select('*');
		$this->db->from('tahun_ajaran');
		$query = $this->db->get();
		return $query;
	}

	public function update($data,$where){

		$this->db->where($where);
		$this->db->update('tahun_ajaran',$data);

	}


	public function getajaranaktif(){


		
		$this->db->where('aktif',true);
	
		$query = $this->db->get('semester');
		return $query->row()->id_semester;

	}

	

}

/* End of file model_ajaran.php */
/* Location: ./application/models/model_ajaran.php */