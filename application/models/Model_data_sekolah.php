<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_data_sekolah extends CI_Model 
{
	public function __construct()
	{
		
	//load database
		parent::__construct();
		$this->load->database();
	}

	public function get(){

		$this->db->order_by('id', 'DESC');

		$query = $this->db->get('data_sekolah');
		
		return $query->row();

	}

	public function update($data,$where){

		$this->db->where($where);
		$this->db->update('data_sekolah',$data);

	}

}

/* End of file model_ajaran.php */
/* Location: ./application/models/model_ajaran.php */