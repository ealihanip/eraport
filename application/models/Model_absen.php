<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_absen extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'absen.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('absen');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = absen.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = absen.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = absen.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function get($where='')
	{

		if(!empty($where)){

			$this->db->where($where);
			
		}
		$this->db->select(	'absen.*,
							tahun_ajaran.tahun,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('absen');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = absen.id_ajaran', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = absen.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = absen.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->row();
	}

	public function listing1($id_kelas)
	{
		$this->db->select(	'absen.*,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('absen');
		$this->db->join('kelas', 'kelas.id_kelas = absen.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = absen.id_siswa', 'left');
		$this->db->where('kelas.id_kelas', $id_kelas);
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('absen');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function tambah($data)
	{
		$this->db->insert('absen', $data);
	}

	public function edit($data,$where)
	{
		$this->db->where($where);
		$this->db->update('absen', $data);
	}



	

}

/* End of file model_catatan.php */
/* Location: ./application/models/model_catatan.php */