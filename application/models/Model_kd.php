<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kd extends CI_Model 
{
	//load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing($where='')
	{
		
		
		if(!empty($where)){

			$this->db->select('kd.*,
			mapel.nama_mapel,
			kelas.nama_kelas,
			kelas.nama_jurusan,
			aspek.nama_aspek');
			$this->db->from('kd');
			//join3
			$this->db->join('mapel', 'mapel.id_mapel = kd.id_mapel', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = kd.id_kelas', 'left');
			$this->db->join('aspek', 'aspek.id_aspek = kd.id_aspek', 'left');
			$this->db->join('guru_mapel', 'guru_mapel.id_kelas = kelas.id_kelas and guru_mapel.id_mapel=mapel.id_mapel', 'left');
			$this->db->where($where);


		}else{

			$this->db->select('kd.*,
			mapel.nama_mapel,
			kelas.nama_kelas,
			kelas.nama_jurusan,
			aspek.nama_aspek');
			$this->db->from('kd');
			//join3
			$this->db->join('mapel', 'mapel.id_mapel = kd.id_mapel', 'left');
			$this->db->join('kelas', 'kelas.id_kelas = kd.id_kelas', 'left');
			$this->db->join('aspek', 'aspek.id_aspek = kd.id_aspek', 'left');
			$this->db->join('guru_mapel', 'guru_mapel.id_kelas = kelas.id_kelas and guru_mapel.id_mapel=mapel.id_mapel', 'left');
		}
		
		$this->db->order_by('kode_kd', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	//menampikan kd per mapel
	public function listing1($id_mapel)
	{
		$this->db->select('kd.*,
							mapel.nama_mapel,
							kelas.nama_kelas,
							kelas.nama_jurusan');
		$this->db->from('kd');
		//join
		$this->db->join('mapel', 'mapel.id_mapel = kd.id_mapel', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = kd.id_kelas', 'left');
		$this->db->where('kd.id_mapel', $id_mapel);
		$this->db->order_by('nama_jurusan', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id_kd)
	{
		$this->db->select('*');
		$this->db->from('kd');
		$this->db->where('id_kd', $id_kd);
		$this->db->order_by('id_kd', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

// (SELECT guru_mapel.id_kelas, kd.id_mapel, kd.aspek, kd.kode_kd, kd.isi_kd FROM kd JOIN guru_mapel ON guru_mapel.id_kelas=kd.id_kelas WHERE kd.id_mapel=1)

	//tambah
	public function tambah($data)
	{
		$this->db->insert('kd', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_kd', $data['id_kd']);
		$this->db->update('kd', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_kd', $data['id_kd']);
		$this->db->delete('kd', $data);
	}


	public function getdata($where){

		$this->db->select('kd.*');
		$this->db->where($where);
		$this->db->from('kd');	
		$query = $this->db->get();
		

		return $query;
	}
	

}

/* End of file model_kd.php */
/* Location: ./application/models/model_kd.php */