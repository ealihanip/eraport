<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_nilai extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}



	public function getdata($where){

		$this->db->select('nilai.*');
		$this->db->where($where);
		$this->db->from('nilai');
		$query = $this->db->get();
		

		return $query;
	}


	public function getdata_ujian($where){

		$this->db->select('nilai_ujian.*');
		$this->db->where($where);
		$this->db->from('nilai_ujian');
		$query = $this->db->get();
		

		return $query;
	}

	public function listing($where='')
	{
		
		$this->db->select(	'nilai.*,
							tahun_ajaran.tahun,
							mapel.nama_mapel,
							kelas.nama_kelas,
							kelas.nama_jurusan,
							kd.kode_kd,
							siswa.nama_siswa');
		$this->db->from('nilai');
		$this->db->join('set_nilai', 'set_nilai.id = nilai.id_sn', 'left');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = set_nilai.id_ajaran', 'left');
		$this->db->join('mapel', 'mapel.id_mapel = set_nilai.id_mapel', 'left');
		$this->db->join('guru_mapel', 'guru_mapel.id_mapel = mapel.id_mapel', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = set_nilai.id_kelas', 'left');
		$this->db->join('kd', 'kd.id_kd = set_nilai.id_kd', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = nilai.id_siswa', 'left');
		$this->db->order_by('id_nilai', 'desc');

		if(!empty($where)){

			$this->db->where($where);

		}

		$query = $this->db->get();
		return $query->result();
	}

	public function listing_ujian($where='')
	{
		
		$this->db->select(	'nilai_ujian.*');
		$this->db->from('nilai_ujian');
		
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = nilai_ujian.id_ajaran', 'left');
		$this->db->join('mapel', 'mapel.id_mapel = nilai_ujian.id_mapel', 'left');
		$this->db->join('guru_mapel', 'guru_mapel.id_mapel = mapel.id_mapel', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = nilai_ujian.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = nilai_ujian.id_siswa', 'left');
		$this->db->order_by('id_nilai', 'desc');

		if(!empty($where)){

			$this->db->where($where);

		}

		$query = $this->db->get();
		return $query->result();
	}


	

	public function getnilaisiswa($where='')
	{
		
		$this->db->select('siswa.*');
		$this->db->from('siswa');
		$this->db->join('kelas', 'kelas.id_kelas = siswa.id_kelas');
		$this->db->join('set_nilai', 'kelas.id_kelas = set_nilai.id_kelas');
		$this->db->join('nilai', 'nilai.id_sn = set_nilai.id','left');

		if(!empty($where)){

			$this->db->where($where);

		}

		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_nilai)
	{
		$this->db->select('*');
		$this->db->from('nilai');
		$this->db->where('id_nilai', $id_nilai);
		$this->db->order_by('id_nilai', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	//tambah
	public function tambah($data)
	{
		$this->db->insert('nilai', $data);
	}

	//tambah
	public function tambah_ujian($data)
	{
		$this->db->insert('nilai_ujian', $data);
	}
	//edit
	public function edit($data,$where)
	{
		$this->db->where($where);
		$this->db->update('nilai', $data);
	}

	//edit
	public function edit_ujian($data,$where)
	{
		$this->db->where($where);
		$this->db->update('nilai_ujian', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_nilai', $data['id_nilai']);
		$this->db->delete('nilai', $data);
	}
	
}

/* End of file model_sn.php */
/* Location: ./application/models/model_sn.php */