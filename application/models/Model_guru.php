<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_guru extends CI_Model 
{
	
	//load database
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	//listing
	public function listing()
	{
		$this->db->select('*');
		$this->db->from('guru');
		//  SELECT title, content, date FROM mytable
		$this->db->order_by('nama_guru', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	public function listing1()
	{
		$this->db->select('*');
		$this->db->from('guru');
		//  SELECT title, content, date FROM mytable
		$this->db->where('akses_level', 2);
		$this->db->order_by('id_guru', 'asc');
		// Produces: ORDER BY `title` DESC
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_guru)
	{
		$this->db->select('*');
		$this->db->from('guru');
		$this->db->where('id_guru', $id_guru);
		$this->db->order_by('id_guru', 'desc');
		$query = $this->db->get();
		return $query->row();
	}

	// //login
	// public function login($username, $password)
	// {
	// 	$this->db->select('*');
	// 	$this->db->from('guru');
	// 	$this->db->where(array(	'username' => $username,
	// 							'password' => sha1($password)));
	// 	$this->db->order_by('id_guru', 'desc');
	// 	$query = $this->db->get();
	// 	return $query->row();
	// }

	//tambah
	public function tambah($data)
	{
		$this->db->insert('guru', $data);
	}

	//edit
	public function edit($data)
	{
		$this->db->where('id_guru', $data['id_guru']);
		$this->db->update('guru', $data);
	}

	//delete
	public function delete($data)
	{
		$this->db->where('id_guru', $data['id_guru']);
		$this->db->delete('guru', $data);
	}
	

}

/* End of file model_guru.php */
/* Location: ./application/models/model_guru.php */