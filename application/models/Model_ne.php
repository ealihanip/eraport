<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_ne extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function listing()
	{
		$this->db->select(	'nilai_ekskul.*,
							tahun_ajaran.tahun,
							ekskul.nama_ekskul,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('nilai_ekskul');
		$this->db->join('tahun_ajaran', 'tahun_ajaran.id_ajaran = nilai_ekskul.id_ajaran', 'left');
		$this->db->join('ekskul', 'ekskul.id = nilai_ekskul.id_ekskul', 'left');
		$this->db->join('kelas', 'kelas.id_kelas = nilai_ekskul.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = nilai_ekskul.id_siswa', 'left');
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function listing1($id_kelas)
	{
		$this->db->select(	'catatan_wk.*,
							kelas.nama_jurusan,
							siswa.nama_siswa');
		$this->db->from('catatan_wk');
		$this->db->join('kelas', 'kelas.id_kelas = catatan_wk.id_kelas', 'left');
		$this->db->join('siswa', 'siswa.id_siswa = catatan_wk.id_siswa', 'left');
		$this->db->where('kelas.id_kelas', $id_kelas);
		$this->db->order_by('siswa.nama_siswa', 'asc');
		$query = $this->db->get();
		return $query->result();
	}

	public function detail($id)
	{
		$this->db->select('*');
		$this->db->from('nilai_ekskul');
		$this->db->where('id', $id);
		$query = $this->db->get();
		return $query->result();
	}

	public function tambah($data)
	{
		$this->db->insert('nilai_ekskul', $data);
	}

	public function edit($data)
	{
		$this->db->where('id', $data['id']);
		$this->db->update('nilai_ekskul', $data);
	}



	

}

/* End of file model_catatan.php */
/* Location: ./application/models/model_catatan.php */