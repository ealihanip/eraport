<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_keahlian extends CI_Model 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function bidang_keahlian()
	{
		$this->db->select('*');
		$this->db->from('b_keahlian');
		$query = $this->db->get();
		return $query->result();
	}

	public function program_keahlian()
	{
		$this->db->select('p_keahlian.*, b_keahlian.nama');
		$this->db->from('p_keahlian');
		$this->db->join('b_keahlian', 'b_keahlian.id_bidang = p_keahlian.id_bidang', 'left');
		// $this->db->where('Field / comparison', $Value);
		$query = $this->db->get();
		return $query->result();
	}

	public function kompetensi_keahlian()
	{
		$this->db->select('k_keahlian.*, b_keahlian.nama, p_keahlian.nama');
		$this->db->from('k_keahlian');
		$this->db->join('b_keahlian', 'b_keahlian.id_bidang = k_keahlian.id_bidang', 'left');
		$this->db->join('p_keahlian', 'p_keahlian.id_program = k_keahlian.id_program', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	

}

/* End of file Model_keahlian.php */
/* Location: ./application/models/Model_keahlian.php */